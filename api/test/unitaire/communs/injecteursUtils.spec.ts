import { lireCelluleFloatOuNull, lireCelluleInt, lireCelluleIntOuNull, lireCelluleString } from '../../../src/communs/injecteursUtils';

describe('Test des fonctions utilitaires de lectures des cellules ', () => {
    let map = new Map<string, string>();
    map.set('colonneA', 'texte');
    map.set('colonneB', '1.2');
    map.set('colonneC', '1');
    map.set('colonneD', '');

    it('Lire une cellule (en string)', () => {
        expect(lireCelluleString(map, 'colonneA')).toEqual('texte');
        expect(lireCelluleString(map, 'colonneB')).toEqual('1.2');
        expect(lireCelluleString(map, 'colonneD')).toEqual('');
    });

    it('Convertir une cellule en entier', () => {
        expect(lireCelluleInt(map, 'colonneA')).toEqual(NaN);
        expect(lireCelluleInt(map, 'colonneB')).toEqual(1);
        expect(lireCelluleInt(map, 'colonneC')).toEqual(1);
        expect(lireCelluleInt(map, 'colonneD')).toEqual(NaN);
    });

    it('Convertir une cellule en entier ou null', () => {
        expect(lireCelluleIntOuNull(map, 'colonneA')).toEqual(NaN);
        expect(lireCelluleIntOuNull(map, 'colonneB')).toEqual(1);
        expect(lireCelluleIntOuNull(map, 'colonneC')).toEqual(1);
        expect(lireCelluleIntOuNull(map, 'colonneD')).toEqual(null);
    });

    it('Convertir une cellule en float ou null', () => {
        expect(lireCelluleFloatOuNull(map, 'colonneA')).toEqual(NaN);
        expect(lireCelluleFloatOuNull(map, 'colonneB')).toEqual(1.2);
        expect(lireCelluleFloatOuNull(map, 'colonneC')).toEqual(1);
        expect(lireCelluleFloatOuNull(map, 'colonneD')).toEqual(null);
    });

    it('Lever une erreur si le nom de colonne est inconnu', () => {
        expect(() => lireCelluleInt(map, 'colonneInconnue')).toThrowError(Error);
    });
});

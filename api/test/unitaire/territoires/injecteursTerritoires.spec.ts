import { ServicesTerritoires } from '../../../src/territoires/domaines/ServicesTerritoires';
import { InjecteursTerritoires } from '../../../src/territoires/InjecteursTerritoires';
import { Commune, Departement, Epci, Pays, Region, RegroupementCommunes } from '../../../src/territoires/domaines/entitesTerritoires';

const CHEMIN_DONNEES_TESTS_UNITAIRES_INPUT = 'test/unitaire/data/input';

describe('Test unitaire injecteurs territoires', () => {
    let servicesTerritoires: ServicesTerritoires;
    let injecteursTerritoires: InjecteursTerritoires;

    beforeEach(() => {
        servicesTerritoires = new ServicesTerritoires();
        injecteursTerritoires = new InjecteursTerritoires(servicesTerritoires);
    });

    it('Charger les territoires', async () => {
        // when
        await injecteursTerritoires.injecterDonneesTerritoires(CHEMIN_DONNEES_TESTS_UNITAIRES_INPUT);
        // then
        expect(servicesTerritoires.rechercherTous().length).toEqual(14);

        let pays = servicesTerritoires.rechercher('france');
        expect(pays).toEqual({ id: 'france', idParcel: '1', nom: 'France' });
        expect(pays).toBeInstanceOf(Pays);

        let region = servicesTerritoires.rechercher('nouvelle-aquitaine');
        expect(region).toEqual({
            id: 'nouvelle-aquitaine',
            idParcel: null,
            nom: 'Nouvelle-Aquitaine',
            idPays: 'france'
        });
        expect(region).toBeInstanceOf(Region);

        let departement = servicesTerritoires.rechercher('landes');
        expect(departement).toEqual({
            id: 'landes',
            idParcel: null,
            nom: 'Landes',
            idPays: 'france',
            idRegion: 'nouvelle-aquitaine'
        });
        expect(departement).toBeInstanceOf(Departement);

        let epci = servicesTerritoires.rechercher('communaute-de-communes-d-aire-sur-l-adour');
        expect(epci).toEqual({
            id: 'communaute-de-communes-d-aire-sur-l-adour',
            idParcel: null,
            nom: "CC d'Aire-sur-l'Adour",
            sousCategorie: 'COMMUNAUTE_COMMUNES',
            idPays: 'france',
            idRegion: 'nouvelle-aquitaine',
            idDepartement: 'landes'
        });
        expect(epci).toBeInstanceOf(Epci);

        let pnr = servicesTerritoires.rechercher('pnr-fictif-pour-les-tests');
        expect(pnr).toEqual({
            id: 'pnr-fictif-pour-les-tests',
            idParcel: null,
            nom: 'PNR Fictif pour les tests',
            sousCategorie: 'PNR',
            idPays: 'france',
            idRegion: 'occitanie',
            idDepartement: 'gers'
        });
        expect(pnr).toBeInstanceOf(RegroupementCommunes);

        let commune = servicesTerritoires.rechercher('paris-commune');
        expect(commune).toEqual({
            id: 'paris-commune',
            idParcel: null,
            nom: 'Paris',
            idEpci: 'metropole-du-grand-paris',
            idDepartement: 'paris-departement',
            idRegion: 'ile-de-france',
            idPays: 'france',
            _codesPostaux: ['75010', '75011']
        });
        expect(commune).toBeInstanceOf(Commune);
    });
});

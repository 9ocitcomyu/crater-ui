import CraterApp from '../../src/CraterApp';
import express = require('express');
import request = require('supertest');

jest.setTimeout(60000);

describe('Tests integration API CRATer pour valider le fonctionnement avec les données réelles', () => {
    let craterApp: CraterApp;
    let expressApp: express.Application;

    beforeAll(async function () {
        expressApp = express();
        craterApp = new CraterApp(expressApp, 'build/crater-data-resultats/data');
        await craterApp.init();
    });

    it('Test API GET /territoires?critere=toulouse', (done) => {
        request(expressApp).get('/crater/api/territoires?critere=toulouse').expect('Content-Type', /json/).expect(200, done);
    });

    it('Test API GET /territoires?critere=saint', (done) => {
        request(expressApp)
            .get('/crater/api/territoires?critere=saint')
            .expect('Content-Type', /json/)
            .expect((res: any) => {
                if (res.body.length != 10) {
                    throw new Error('Erreur : la réponse ne contient pas 10 territoires');
                }
            })
            .expect(200, done);
    });

    it('Test API GET /territoires?paraminconnu=12', (done) => {
        request(expressApp).get('/crater/api/territoires?paraminconnu=12').expect('Content-Type', /json/).expect(200, [], done);
    });

    it('Test API GET /territoires/P-FR', (done) => {
        request(expressApp).get('/crater/api/territoires/france').expect('Content-Type', /json/).expect(
            200,
            {
                idTerritoire: 'france',
                nomTerritoire: 'France',
                categorieTerritoire: 'PAYS',
                idParcel: '1'
            },
            done
        );
    });

    it('Test API GET /territoires/normandie', (done) => {
        request(expressApp)
            .get('/crater/api/territoires/normandie')
            .expect('Content-Type', /json/)
            .expect(
                200,
                {
                    idTerritoire: 'normandie',
                    nomTerritoire: 'Normandie',
                    categorieTerritoire: 'REGION',
                    idParcel: '5',
                    pays: {
                        id: 'france',
                        nom: 'France'
                    }
                },
                done
            );
    });

    it('Test API GET /territoires/calvados', (done) => {
        request(expressApp)
            .get('/crater/api/territoires/calvados')
            .expect('Content-Type', /json/)
            .expect(
                200,
                {
                    idTerritoire: 'calvados',
                    nomTerritoire: 'Calvados',
                    categorieTerritoire: 'DEPARTEMENT',
                    idParcel: '60',
                    pays: {
                        id: 'france',
                        nom: 'France'
                    },
                    region: {
                        id: 'normandie',
                        nom: 'Normandie'
                    }
                },
                done
            );
    });

    it('Test API GET /territoires/communaute-de-communes-yvetot-normandie', (done) => {
        request(expressApp)
            .get('/crater/api/territoires/communaute-de-communes-yvetot-normandie')
            .expect('Content-Type', /json/)
            .expect(
                200,
                {
                    idTerritoire: 'communaute-de-communes-yvetot-normandie',
                    nomTerritoire: 'Communauté de communes Yvetot Normandie',
                    categorieTerritoire: 'EPCI',
                    idParcel: null,
                    sousCategorieTerritoire: 'COMMUNAUTE_COMMUNES',
                    pays: {
                        id: 'france',
                        nom: 'France'
                    },
                    region: {
                        id: 'normandie',
                        nom: 'Normandie'
                    },
                    departement: {
                        id: 'seine-maritime',
                        nom: 'Seine-Maritime'
                    }
                },
                done
            );
    });

    it('Test API GET /territoires/pnr-du-vercors', (done) => {
        request(expressApp)
            .get('/crater/api/territoires/pnr-du-vercors')
            .expect('Content-Type', /json/)
            .expect(
                200,
                {
                    idTerritoire: 'pnr-du-vercors',
                    nomTerritoire: 'PNR du Vercors',
                    categorieTerritoire: 'REGROUPEMENT_COMMUNES',
                    idParcel: null,
                    sousCategorieTerritoire: 'PARC_NATUREL_REGIONAL',
                    pays: {
                        id: 'france',
                        nom: 'France'
                    },
                    region: {
                        id: 'auvergne-rhone-alpes',
                        nom: 'Auvergne-Rhône-Alpes'
                    },
                    departement: {
                        id: 'isere',
                        nom: 'Isère'
                    }
                },
                done
            );
    });

    it('Test API GET /territoires/caen', (done) => {
        request(expressApp)
            .get('/crater/api/territoires/caen')
            .expect('Content-Type', /json/)
            .expect(
                200,
                {
                    idTerritoire: 'caen',
                    nomTerritoire: 'Caen',
                    categorieTerritoire: 'COMMUNE',
                    idParcel: '8949',
                    pays: {
                        id: 'france',
                        nom: 'France'
                    },
                    region: {
                        id: 'normandie',
                        nom: 'Normandie'
                    },
                    departement: {
                        id: 'calvados',
                        nom: 'Calvados'
                    },
                    epci: {
                        id: 'communaute-urbaine-caen-la-mer',
                        nom: 'Communauté urbaine Caen la Mer'
                    }
                },
                done
            );
    });

    it('Test API GET /diagnostics par codeInsee pour France', (done) => {
        request(expressApp)
            .get('/crater/api/diagnostics/france')
            .expect('Content-Type', /json/)
            .expect((res: any) => {
                if (!('idTerritoire' in res.body)) {
                    throw new Error('Erreur : la réponse ne contient pas de champ idTerritoire');
                }
                if (res.body.idTerritoire != 'france') {
                    throw new Error("Erreur : l'ID Territoire dans la réponse du diagnostic n'a pas la bonne valeur");
                }
            })
            .expect(200, done);
    });

    it('Test API GET /diagnostics par codeInsee pour une région', (done) => {
        request(expressApp)
            .get('/crater/api/diagnostics/nouvelle-aquitaine')
            .expect('Content-Type', /json/)
            .expect((res: any) => {
                if (!('idTerritoire' in res.body)) {
                    throw new Error('Erreur : la réponse ne contient pas de champ idTerritoire');
                }
                if (res.body.idTerritoire != 'nouvelle-aquitaine') {
                    throw new Error("Erreur : l'ID Territoire dans la réponse du diagnostic n'a pas la bonne valeur");
                }
            })
            .expect(200, done);
    });

    it('Test API GET /indicateurs/superficieHa pour les EPCIs', (done) => {
        request(expressApp).get('/crater/api/indicateurs/superficieHa?categorieTerritoire=EPCI').expect('Content-Type', /json/).expect(200, done);
    });
});

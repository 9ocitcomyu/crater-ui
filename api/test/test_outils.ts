import * as fs from 'fs';

export function verifierEgaliteAvecFichierTexte(contenuFichierAValider: string, cheminDossierDonneesTest: string, nomFichierResultatAttendu: string) {
    let cheminFichierAttendu = cheminDossierDonneesTest + 'expected/' + nomFichierResultatAttendu;
    let contenuFichierAttendu = '';
    if (fs.existsSync(cheminFichierAttendu)) {
        contenuFichierAttendu = fs.readFileSync(cheminFichierAttendu).toString();
    } else {
        throw new Error(`Erreur : le fichier attendu n'existe pas (pas de fichier ${cheminFichierAttendu}`);
    }

    if (!fs.existsSync(cheminDossierDonneesTest + 'output')) {
        fs.mkdirSync(cheminDossierDonneesTest + 'output');
    }
    // écriture d'un fichier avec le contenu pour faciliter les diff avec le resultat attendu en cas d'erreur
    let cheminFichierResultat = cheminDossierDonneesTest + 'output/' + nomFichierResultatAttendu;
    fs.writeFile(cheminFichierResultat, contenuFichierAValider, (error) => {
        if (error) throw error;
    });

    if (contenuFichierAValider !== contenuFichierAttendu) {
        throw new Error(
            `Erreur : les données ne sont pas identiques entre le fichier resultat (${cheminFichierResultat}) et le fichier attendu ${cheminFichierAttendu}`
        );
    }
}

export function supprimerNPremieresLignes(contenu: string, nbLigne: number | undefined, separateurFinLigne: string) {
    if (nbLigne === undefined) {
        return contenu;
    } else {
        return contenu.split(separateurFinLigne).slice(nbLigne).join(separateurFinLigne);
    }
}

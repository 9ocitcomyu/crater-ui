echo "copy-cdr-gitlab.sh $1 $2 $3"
echo "Copie des données crater-data-resultats, environnement GITLAB"
if [ $# -ne 3 ]; then
  echo 1>&2 "$0: Erreur, merci de renseigner les 3 paramètres obligatoires, dans l'ordre : CI_COMMIT_BRANCH, CRATER_DATA_RESULTATS_GIT_URL et CI_JOB_TOKEN"
  exit 2
fi

CI_COMMIT_BRANCH=$1
CRATER_DATA_RESULTATS_GIT_URL=$2
CI_JOB_TOKEN=$3

if [ $CI_COMMIT_BRANCH != "master" ]; then
  CRATER_BRANCHE_CDR="develop"
else
  CRATER_BRANCHE_CDR="master"
fi

echo "  - dossier courant = $(pwd)"
echo "  - branche courante sur dépot crater-ui=${CI_COMMIT_BRANCH}"
echo "  - url dépot crater-data-resultats=${CRATER_DATA_RESULTATS_GIT_URL}"
echo "  - branche crater-data-resultats utilisée pour le clone=${CRATER_BRANCHE_CDR}"
echo "  - dossier cible dans crater-ui=$(pwd)/prebuild/crater-data-resultats"

rm -rf ./prebuild/crater-data-resultats
mkdir -p ./prebuild
git clone --single-branch -b ${CRATER_BRANCHE_CDR} --depth 1 https://gitlab-ci-token:${CI_JOB_TOKEN}@${CRATER_DATA_RESULTATS_GIT_URL} ./prebuild/crater-data-resultats
cd ./prebuild/crater-data-resultats
ls -A1 | grep -v data | xargs rm -rf



import {
    BesoinsAssiette,
    BesoinsParCulture,
    BesoinsParGroupeCulture,
    Diagnostic,
    IndicateursParTypeCommerce,
    IndicateursPesticidesAnneeN,
    SauParCulture,
    SauParGroupeCulture
} from '../../src_generated/api/model/models';
import { convertirDateEnString, Etat, extraireValeurDepuisSousObjet, jsonToCsv, SAUT_LIGNE, verifierEtatApi } from '../communs/apiUtils';
import { ErreurRequeteIncorrecte, ErreurRessourceNonTrouvee } from '../communs/erreurs';
import {
    CATEGORIE_COMMUNE,
    CATEGORIE_DEPARTEMENT,
    CATEGORIE_EPCI,
    CATEGORIE_PAYS,
    CATEGORIE_REGION
} from '../territoires/domaines/entitesTerritoires';
import {
    BesoinsParAssietteEntite,
    BesoinsParGroupeCultureEntite as BesoinsParGroupeCultureEntite,
    DiagnosticEntite,
    IndicateursPesticidesAnneeNEntite,
    SauParGroupeCultureEntite as SauParGroupeCultureEntite
} from './domaines/entitesDiagnostics';
import { ServicesDiagnostics } from './domaines/ServicesDiagnostics';
import express = require('express');

export class ApiDiagnostics {
    private servicesDiagnostics: ServicesDiagnostics;
    private expressApp: express.Application;
    public etat = Etat.INIT_EN_COURS;

    constructor(expressApp: express.Application, servicesDiagnostics: ServicesDiagnostics) {
        this.expressApp = expressApp;
        this.servicesDiagnostics = servicesDiagnostics;
    }

    configurerRoutes() {
        this.expressApp.get('/crater/api/diagnostics/:idTerritoire', this.construireReponseDiagnosticsJson);
        this.expressApp.get('/crater/api/diagnostics/csv/:idTerritoire', this.construireReponseDiagnosticsCsv);
        this.expressApp.get('/crater/api/indicateurs/:nomIndicateur', this.construireReponseIndicateursJson);
    }

    private construireReponseDiagnosticsJson = (req: express.Request, res: express.Response) => {
        verifierEtatApi(this.etat);
        const diagnostic = this.servicesDiagnostics.rechercher(req.params.idTerritoire);
        if (!diagnostic) {
            throw new ErreurRessourceNonTrouvee('Diagnostic non trouvé (idTerritoire=' + req.params.idTerritoire + ')');
        }
        res.json(ApiDiagnostics.creerModeleApiDepuisEntite(diagnostic));
    };

    private construireReponseDiagnosticsCsv = (req: express.Request, res: express.Response) => {
        verifierEtatApi(this.etat);
        const diagnostic = this.servicesDiagnostics.rechercher(req.params.idTerritoire);
        if (!diagnostic) {
            throw new ErreurRessourceNonTrouvee('Diagnostic non trouvé (idTerritoire=' + req.params.idTerritoire + ')');
        }

        const diagnosticApiModele = ApiDiagnostics.creerModeleApiDepuisEntite(diagnostic);

        res.setHeader('Content-Type', 'text/csv; charset=utf-8');
        res.setHeader('Content-disposition', `attachment; filename= ${diagnosticApiModele.idTerritoire}_${convertirDateEnString(new Date())}.csv`);
        const baseUrl = req.protocol + '://' + req.hostname;
        let csvBody = `Résultat du diagnostic CRATer pour le territoire ${diagnosticApiModele.nomTerritoire}, exporté le ${convertirDateEnString(
            new Date()
        )}${SAUT_LIGNE}`;
        csvBody += `Adresse du rapport CRATer;${baseUrl}/diagnostic/${diagnosticApiModele.idTerritoire}${SAUT_LIGNE}`;
        csvBody += `Sources de données utilisées pour le calcul;${baseUrl}/aide/methodologie/sources-donnees${SAUT_LIGNE}`;
        csvBody += `${SAUT_LIGNE}${SAUT_LIGNE}`;
        csvBody += jsonToCsv(diagnosticApiModele).join(SAUT_LIGNE);
        res.send(csvBody);
    };

    private construireReponseIndicateursJson = (req: express.Request, res: express.Response) => {
        verifierEtatApi(this.etat);
        const categorieTerritoire = this.extraireParametreCategorieTerritoire(req);
        const filtreAnnee = req.query.annee ? req.query.annee.toString() : '';
        const filtreIdsDepartements = req.query.idsDepartements ? req.query.idsDepartements.toString().split('|') : '';

        if (categorieTerritoire === CATEGORIE_COMMUNE && filtreIdsDepartements === '')
            throw new ErreurRequeteIncorrecte('Le paramètre idsDepartements est obligatoire si categorieTerritoire=COMMUNE');

        let diagnosticEntites = this.servicesDiagnostics.rechercherTousParCategorie(categorieTerritoire);

        if (categorieTerritoire == CATEGORIE_COMMUNE) {
            diagnosticEntites = diagnosticEntites.filter((d) => filtreIdsDepartements.includes(d.idDepartement));
        }

        const listeIndicateurs = diagnosticEntites
            .map((d) => ApiDiagnostics.creerModeleApiDepuisEntite(d))
            .map((d) => {
                return {
                    idTerritoire: d.idTerritoire,
                    valeur: extraireValeurDepuisSousObjet(d, req.params.nomIndicateur, filtreAnnee)
                };
            });

        res.json(listeIndicateurs);
    };

    private extraireParametreCategorieTerritoire(req: express.Request) {
        const categorieTerritoire = req.query.categorieTerritoire ? req.query.categorieTerritoire.toString() : '';
        if ([CATEGORIE_COMMUNE, CATEGORIE_EPCI, CATEGORIE_DEPARTEMENT, CATEGORIE_REGION, CATEGORIE_PAYS].includes(categorieTerritoire)) {
            return categorieTerritoire;
        } else {
            return '';
        }
    }

    private static creerModeleApiDepuisEntite(diagnostic: DiagnosticEntite): Diagnostic {
        return {
            idTerritoire: diagnostic.idTerritoire,
            nomTerritoire: diagnostic.nomTerritoire,
            population: diagnostic.population,
            superficieHa: diagnostic.superficieHa,
            idTerritoireParcel: diagnostic.idTerritoireParcel,
            nbCommunes: diagnostic.nbCommunes,
            surfaceAgricoleUtile: {
                sauTotaleHa: diagnostic.surfaceAgricoleUtile.sauTotaleHa,
                sauProductiveHa: diagnostic.surfaceAgricoleUtile.sauProductiveHa,
                sauPeuProductiveHa: diagnostic.surfaceAgricoleUtile.sauPeuProductiveHa,
                sauBioHa: diagnostic.surfaceAgricoleUtile.sauBioHa,
                sauParGroupeCulture: ApiDiagnostics.creerListeSauParGroupeCulture(diagnostic)
            },
            nbCommunesParOtex: {
                grandesCultures: diagnostic.nbCommunesParOtex.grandesCultures,
                maraichageHorticulture: diagnostic.nbCommunesParOtex.maraichageHorticulture,
                viticulture: diagnostic.nbCommunesParOtex.viticulture,
                fruits: diagnostic.nbCommunesParOtex.fruits,
                bovinLait: diagnostic.nbCommunesParOtex.bovinLait,
                bovinViande: diagnostic.nbCommunesParOtex.bovinViande,
                bovinMixte: diagnostic.nbCommunesParOtex.bovinMixte,
                ovinsCaprinsAutresHerbivores: diagnostic.nbCommunesParOtex.ovinsCaprinsAutresHerbivores,
                porcinsVolailles: diagnostic.nbCommunesParOtex.porcinsVolailles,
                polyculturePolyelevage: diagnostic.nbCommunesParOtex.polyculturePolyelevage,
                nonClassees: diagnostic.nbCommunesParOtex.nonClassees,
                sansExploitations: diagnostic.nbCommunesParOtex.sansExploitations,
                nonRenseigne: diagnostic.nbCommunesParOtex.nonRenseigne
            },
            consommation: {
                tauxPauvrete60Pourcent: diagnostic.consommation.tauxPauvrete60Pourcent
            },
            politiqueFonciere: {
                note: diagnostic.politiqueFonciere.note,
                artificialisation5ansHa: diagnostic.politiqueFonciere.artificialisation5ansHa,
                evolutionMenagesEmplois5ans: diagnostic.politiqueFonciere.evolutionMenagesEmplois5ans,
                sauParHabitantM2: diagnostic.politiqueFonciere.sauParHabitantM2,
                rythmeArtificialisationSauPourcent: diagnostic.politiqueFonciere.rythmeArtificialisationSauPourcent,
                evaluationPolitiqueAmenagement: diagnostic.politiqueFonciere.politiqueAmenagement,
                partLogementsVacants2013Pourcent: diagnostic.politiqueFonciere.partLogementsVacants2013Pourcent,
                partLogementsVacants2018Pourcent: diagnostic.politiqueFonciere.partLogementsVacants2018Pourcent
            },
            populationAgricole: {
                note: diagnostic.populationAgricole.note,
                populationAgricole1988: diagnostic.populationAgricole.populationAgricole1988,
                populationAgricole2010: diagnostic.populationAgricole.populationAgricole2010,
                partPopulationAgricole2010Pourcent: diagnostic.populationAgricole.partPopulationAgricole2010Pourcent,
                partPopulationAgricole1988Pourcent: diagnostic.populationAgricole.partPopulationAgricole1988Pourcent,
                nbExploitations1988: diagnostic.populationAgricole.nbExploitations1988,
                nbExploitations2010: diagnostic.populationAgricole.nbExploitations2010,
                sauHa1988: diagnostic.populationAgricole.sauHa1988,
                sauHa2010: diagnostic.populationAgricole.sauHa2010,
                sauMoyenneParExploitationHa1988: diagnostic.populationAgricole.sauMoyenneParExploitationHa1988,
                sauMoyenneParExploitationHa2010: diagnostic.populationAgricole.sauMoyenneParExploitationHa2010,
                nbExploitationsParClassesAgesChefExploitation: {
                    moins_40_ans: diagnostic.populationAgricole.nbExploitationsParClassesAgesChefExploitation.moins_40_ans,
                    de_40_a_49_ans: diagnostic.populationAgricole.nbExploitationsParClassesAgesChefExploitation.de_40_a_49_ans,
                    de_50_a_59_ans: diagnostic.populationAgricole.nbExploitationsParClassesAgesChefExploitation.de_50_a_59_ans,
                    plus_60_ans: diagnostic.populationAgricole.nbExploitationsParClassesAgesChefExploitation.plus_60_ans
                },
                nbExploitationsParClassesSuperficies: {
                    moins_20_ha: diagnostic.populationAgricole.nbExploitationsParClassesSuperficies.moins_20_ha,
                    de_20_a_50_ha: diagnostic.populationAgricole.nbExploitationsParClassesSuperficies.de_20_a_50_ha,
                    de_50_a_100_ha: diagnostic.populationAgricole.nbExploitationsParClassesSuperficies.de_50_a_100_ha,
                    de_100_a_200_ha: diagnostic.populationAgricole.nbExploitationsParClassesSuperficies.de_100_a_200_ha,
                    plus_200_ha: diagnostic.populationAgricole.nbExploitationsParClassesSuperficies.plus_200_ha
                },
                sauHaParClassesSuperficies: {
                    moins_20_ha: diagnostic.populationAgricole.sauHaParClassesSuperficies.moins_20_ha,
                    de_20_a_50_ha: diagnostic.populationAgricole.sauHaParClassesSuperficies.de_20_a_50_ha,
                    de_50_a_100_ha: diagnostic.populationAgricole.sauHaParClassesSuperficies.de_50_a_100_ha,
                    de_100_a_200_ha: diagnostic.populationAgricole.sauHaParClassesSuperficies.de_100_a_200_ha,
                    plus_200_ha: diagnostic.populationAgricole.sauHaParClassesSuperficies.plus_200_ha
                }
            },
            intrants: {
                note: diagnostic.intrants.note,
                noduNormalise: diagnostic.intrants.nodeNormalise,
                pesticides: {
                    indicateursMoyennesTriennales: ApiDiagnostics.creerListeIndicateursPesticidesParAnnee(
                        diagnostic.intrants.pesticides.indicateursMoyennesTriennales.toutesAnnees
                    )
                }
            },
            pratiquesAgricoles: {
                indicateurHvn: {
                    indice1: diagnostic.pratiquesAgricoles.indicateurHvn.indice1,
                    indice2: diagnostic.pratiquesAgricoles.indicateurHvn.indice2,
                    indice3: diagnostic.pratiquesAgricoles.indicateurHvn.indice3,
                    indiceTotal: diagnostic.pratiquesAgricoles.indicateurHvn.indiceTotal
                },
                partSauBioPourcent: diagnostic.pratiquesAgricoles.partSauBioPourcent
            },
            productionsBesoins: {
                besoinsAssietteActuelle: ApiDiagnostics.creerBesoinParAssiette(diagnostic.productionsBesoins.besoinsAssietteActuelle),
                besoinsAssietteDemitarienne: ApiDiagnostics.creerBesoinParAssiette(diagnostic.productionsBesoins.besoinsAssietteDemitarienne)
            },
            production: {
                note: diagnostic.production.note,
                tauxAdequationMoyenPonderePourcent: diagnostic.production.tauxAdequationMoyenPonderePourcent,
                partSauBioPourcent: diagnostic.production.partSauBioPourcent,
                indiceHvn: diagnostic.production.indiceHvn,
                noteTauxAdequationMoyenPondere: diagnostic.production.noteTauxAdequationMoyenPondere,
                notePartSauBio: diagnostic.production.notePartSauBio,
                noteHvn: diagnostic.production.noteHvn
            },
            proximiteCommerces: {
                note: diagnostic.proximiteCommerces.note,
                partPopulationDependanteVoiturePourcent: diagnostic.proximiteCommerces.partPopulationDependanteVoiturePourcent,
                partTerritoireDependantVoiturePourcent: diagnostic.proximiteCommerces.partTerritoireDependantVoiturePourcent,
                indicateursParTypesCommerces: ApiDiagnostics.creerListeIndicateursProximiteCommercesParTypesCommerces(diagnostic)
            }
        };
    }

    private static creerListeSauParGroupeCulture(diagnostic: DiagnosticEntite): SauParGroupeCulture[] {
        const listeSauParGroupeCulture: SauParGroupeCulture[] = [];
        for (const sauGroupeCultureCourante of diagnostic.surfaceAgricoleUtile.sauParGroupeCulture) {
            const sauParCulture = ApiDiagnostics.creerListeSauParCulture(sauGroupeCultureCourante);
            listeSauParGroupeCulture.push({
                codeGroupeCulture: sauGroupeCultureCourante.codeGroupeCulture,
                nomGroupeCulture: sauGroupeCultureCourante.nomGroupeCulture,
                sauHa: sauGroupeCultureCourante.sauHa,
                sauParCulture: sauParCulture
            });
        }
        return listeSauParGroupeCulture;
    }

    private static creerListeSauParCulture(sauParGroupeCulture: SauParGroupeCultureEntite): SauParCulture[] {
        const listeSauParCulture: SauParCulture[] = [];
        for (const sauParCultureCourante of sauParGroupeCulture.sauParCulture) {
            listeSauParCulture.push({
                codeCulture: sauParCultureCourante.codeCulture,
                nomCulture: sauParCultureCourante.nomCulture,
                sauHa: sauParCultureCourante.sauHa
            });
        }
        return listeSauParCulture;
    }

    private static creerListeBesoinsParGroupeCulture(besoinsAssiette: BesoinsParAssietteEntite): BesoinsParGroupeCulture[] {
        const listeBesoinsParGroupeCultures: BesoinsParGroupeCulture[] = [];
        for (const item of besoinsAssiette.besoinsParGroupeCulture) {
            const besoinsParCulture = ApiDiagnostics.creerListeBesoinsParCulture(item);
            listeBesoinsParGroupeCultures.push({
                codeGroupeCulture: item.codeGroupeCulture,
                nomGroupeCulture: item.nomGroupeCulture,
                besoinsHa: item.besoinsHa,
                tauxAdequationBrutPourcent: item.tauxAdequationBrutPourcent,
                besoinsParCulture: besoinsParCulture
            });
        }
        return listeBesoinsParGroupeCultures;
    }

    private static creerListeBesoinsParCulture(besoinsParGroupeCultures: BesoinsParGroupeCultureEntite): BesoinsParCulture[] {
        const listeBesoinsParCultures: BesoinsParCulture[] = [];
        for (const item of besoinsParGroupeCultures.besoinsParCulture) {
            listeBesoinsParCultures.push({
                codeCulture: item.codeCulture,
                nomCulture: item.nomCulture,
                alimentationAnimale: item.alimentationAnimale,
                besoinsHa: item.besoinsHa
            });
        }
        return listeBesoinsParCultures;
    }

    private static creerListeIndicateursPesticidesParAnnee(
        indicateursToutesAnnees: IndicateursPesticidesAnneeNEntite[]
    ): IndicateursPesticidesAnneeN[] {
        const listeIndicateurs: IndicateursPesticidesAnneeN[] = [];
        for (const indicateurs of indicateursToutesAnnees) {
            listeIndicateurs.push({
                annee: indicateurs.annee,
                quantiteSubstanceSansDUKg: indicateurs.quantiteSubstanceSansDUKg,
                quantiteSubstanceAvecDUKg: indicateurs.quantiteSubstanceAvecDUKg,
                noduHa: indicateurs.noduHa,
                noduNormalise: indicateurs.noduNormalise
            });
        }
        return listeIndicateurs;
    }

    private static creerListeIndicateursProximiteCommercesParTypesCommerces(diagnostic: DiagnosticEntite): IndicateursParTypeCommerce[] {
        const listeIndicateurs: IndicateursParTypeCommerce[] = [];
        for (const indicateurs of diagnostic.proximiteCommerces.indicateursProximiteCommercesEntite.tousTypesCommerces) {
            listeIndicateurs.push({
                typeCommerce: indicateurs.typeCommerce,
                distancePlusProcheCommerceMetres: indicateurs.distancePlusProcheCommerceMetres,
                partPopulationAccesAPiedPourcent: indicateurs.partPopulationAccesAPiedPourcent,
                partPopulationAccesAVeloPourcent: indicateurs.partPopulationAccesAVeloPourcent,
                partPopulationDependanteVoiturePourcent: indicateurs.partPopulationDependanteVoiturePourcent
            });
        }
        return listeIndicateurs;
    }

    private static creerBesoinParAssiette(besoinsAssiette: BesoinsParAssietteEntite): BesoinsAssiette {
        const besoinsAssietteObjet: BesoinsAssiette = {
            besoinsHa: besoinsAssiette.besoinsHa,
            tauxAdequationBrutPourcent: besoinsAssiette.tauxAdequationBrutPourcent,
            tauxAdequationMoyenPonderePourcent: besoinsAssiette.tauxAdequationMoyenPonderePourcent,
            besoinsParGroupeCulture: ApiDiagnostics.creerListeBesoinsParGroupeCulture(besoinsAssiette)
        };
        return besoinsAssietteObjet;
    }
}

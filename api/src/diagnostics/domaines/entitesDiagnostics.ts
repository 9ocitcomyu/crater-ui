import { Entite } from '../../communs/servicesUtils';

export class DiagnosticEntite implements Entite {
    public readonly idTerritoire: string;
    public readonly nomTerritoire: string;
    public readonly categorieTerritoire: string;
    public idDepartement = '';
    public nbCommunes: number | null = null;
    public population: number | null = null;
    public superficieHa: number | null = null;
    public idTerritoireParcel = '';
    public consommation: ConsommationEntite = new ConsommationEntite();
    public nbCommunesParOtex: NbCommunesParOtex = new NbCommunesParOtex();
    public surfaceAgricoleUtile: SurfaceAgricoleUtileEntite = new SurfaceAgricoleUtileEntite();
    public politiqueFonciere: PolitiqueFonciereEntite = new PolitiqueFonciereEntite();
    public populationAgricole: PopulationAgricoleEntite = new PopulationAgricoleEntite();
    public intrants: IntrantsEntite = new IntrantsEntite();
    public pratiquesAgricoles: PratiquesAgricolesEntite = new PratiquesAgricolesEntite();
    public productionsBesoins: ProductionsBesoinsEntite = new ProductionsBesoinsEntite();
    public production: ProductionEntite = new ProductionEntite();
    public proximiteCommerces: ProximiteCommercesEntite = new ProximiteCommercesEntite();

    constructor(idTerritoire: string, nomTerritoire: string, categorieTerritoire: string) {
        this.idTerritoire = idTerritoire;
        this.nomTerritoire = nomTerritoire;
        this.categorieTerritoire = categorieTerritoire;
    }

    get id(): string {
        return this.idTerritoire;
    }
}

export class SurfaceAgricoleUtileEntite {
    public sauTotaleHa: number | null = null;
    public sauProductiveHa: number | null = null;
    public sauPeuProductiveHa: number | null = null;
    public sauBioHa: number | null = null;
    public sauParGroupeCulture: SauParGroupeCultureEntite[] = [];
}

export class SauParGroupeCultureEntite {
    public codeGroupeCulture: string;
    public nomGroupeCulture: string;
    public sauHa: number | null;
    public sauParCulture: SauParCultureEntite[] = [];

    constructor(codeGroupeCulture: string, nomGroupeCulture: string, sauHa: number | null) {
        this.codeGroupeCulture = codeGroupeCulture;
        this.nomGroupeCulture = nomGroupeCulture;
        this.sauHa = sauHa;
    }
}

export class SauParCultureEntite {
    public codeCulture: string;
    public nomCulture: string;
    public sauHa: number | null;

    constructor(codeCulture: string, nomCulture: string, sauHa: number | null) {
        this.codeCulture = codeCulture;
        this.nomCulture = nomCulture;
        this.sauHa = sauHa;
    }
}

export class ProductionsBesoinsEntite {
    public besoinsAssietteActuelle: BesoinsParAssietteEntite = new BesoinsParAssietteEntite();
    public besoinsAssietteDemitarienne: BesoinsParAssietteEntite = new BesoinsParAssietteEntite();
}

export class BesoinsParAssietteEntite {
    public besoinsHa: number | null = null;
    public tauxAdequationBrutPourcent: number | null = null;
    public tauxAdequationMoyenPonderePourcent: number | null = null;
    public besoinsParGroupeCulture: BesoinsParGroupeCultureEntite[] = [];
}

export class BesoinsParGroupeCultureEntite {
    public codeGroupeCulture: string;
    public nomGroupeCulture: string;
    public besoinsHa: number | null;
    public tauxAdequationBrutPourcent: number | null;
    public besoinsParCulture: BesoinsParCultureEntite[] = [];

    constructor(codeGroupeCulture: string, nomGroupeCulture: string, besoinsHa: number | null, tauxAdequationBrutPourcent: number | null) {
        this.codeGroupeCulture = codeGroupeCulture;
        this.nomGroupeCulture = nomGroupeCulture;
        this.besoinsHa = besoinsHa;
        this.tauxAdequationBrutPourcent = tauxAdequationBrutPourcent;
    }
}

export class BesoinsParCultureEntite {
    public codeCulture: string;
    public nomCulture: string;
    public alimentationAnimale: boolean;
    public besoinsHa: number | null;

    constructor(codeCulture: string, nomCulture: string, alimentationAnimale: boolean, besoinsHa: number | null) {
        this.codeCulture = codeCulture;
        this.nomCulture = nomCulture;
        this.alimentationAnimale = alimentationAnimale;
        this.besoinsHa = besoinsHa;
    }
}

export class PolitiqueFonciereEntite {
    public note: number | null = null;
    public sauParHabitantM2: number | null = null;
    public artificialisation5ansHa: number | null = null;
    public evolutionMenagesEmplois5ans: number | null = null;
    public rythmeArtificialisationSauPourcent: number | null = null;
    public politiqueAmenagement = '';
    public partLogementsVacants2013Pourcent: number | null = null;
    public partLogementsVacants2018Pourcent: number | null = null;
}
export class ConsommationEntite {
    public tauxPauvrete60Pourcent: number | null;

    constructor(tauxPauvrete60Pourcent: number | null = null) {
        this.tauxPauvrete60Pourcent = tauxPauvrete60Pourcent;
    }
}
export class NbCommunesParOtex {
    public grandesCultures = 0;
    public maraichageHorticulture = 0;
    public viticulture = 0;
    public fruits = 0;
    public bovinLait = 0;
    public bovinViande = 0;
    public bovinMixte = 0;
    public ovinsCaprinsAutresHerbivores = 0;
    public porcinsVolailles = 0;
    public polyculturePolyelevage = 0;
    public nonClassees = 0;
    public sansExploitations = 0;
    public nonRenseigne = 0;
}

export class PopulationAgricoleEntite {
    public note: number | null = null;
    public populationAgricole1988: number | null = null;
    public populationAgricole2010: number | null = null;
    public partPopulationAgricole2010Pourcent: number | null = null;
    public partPopulationAgricole1988Pourcent: number | null = null;
    public nbExploitations1988: number | null = null;
    public nbExploitations2010: number | null = null;
    public sauHa1988: number | null = null;
    public sauHa2010: number | null = null;
    public sauMoyenneParExploitationHa1988: number | null = null;
    public sauMoyenneParExploitationHa2010: number | null = null;
    public nbExploitationsParClassesAgesChefExploitation: ValeursParClassesAgesEntite = new ValeursParClassesAgesEntite();
    public nbExploitationsParClassesSuperficies: ValeursParClassesSuperficiesEntite = new ValeursParClassesSuperficiesEntite();
    public sauHaParClassesSuperficies: ValeursParClassesSuperficiesEntite = new ValeursParClassesSuperficiesEntite();
}

export class ValeursParClassesAgesEntite {
    public moins_40_ans: number | null;
    public de_40_a_49_ans: number | null;
    public de_50_a_59_ans: number | null;
    public plus_60_ans: number | null;

    constructor(
        moins_40_ans: number | null = null,
        de_40_a_49_ans: number | null = null,
        de_50_a_59_ans: number | null = null,
        plus_60_ans: number | null = null
    ) {
        this.moins_40_ans = moins_40_ans;
        this.de_40_a_49_ans = de_40_a_49_ans;
        this.de_50_a_59_ans = de_50_a_59_ans;
        this.plus_60_ans = plus_60_ans;
    }
}

export class ValeursParClassesSuperficiesEntite {
    public moins_20_ha: number | null;
    public de_20_a_50_ha: number | null;
    public de_50_a_100_ha: number | null;
    public de_100_a_200_ha: number | null;
    public plus_200_ha: number | null;

    constructor(
        moins_20_ha: number | null = null,
        de_20_a_50_ha: number | null = null,
        de_50_a_100_ha: number | null = null,
        de_100_a_200_ha: number | null = null,
        plus_200_ha: number | null = null
    ) {
        this.moins_20_ha = moins_20_ha;
        this.de_20_a_50_ha = de_20_a_50_ha;
        this.de_50_a_100_ha = de_50_a_100_ha;
        this.de_100_a_200_ha = de_100_a_200_ha;
        this.plus_200_ha = plus_200_ha;
    }
}

export class IntrantsEntite {
    public note: number | null = null;
    public nodeNormalise: number | null = null;
    public pesticides: PesticidesEntite = new PesticidesEntite();
}

export class PratiquesAgricolesEntite {
    public partSauBioPourcent: number | null = null;
    public indicateurHvn: IndicateurHvnEntite = new IndicateurHvnEntite();
}

export class IndicateurHvnEntite {
    public indice1: number | null = null;
    public indice2: number | null = null;
    public indice3: number | null = null;
    public indiceTotal: number | null = null;
}

export class PesticidesEntite {
    public indicateursMoyennesTriennales = new IndicateursPesticidesEntite();
}

export interface IndicateursPesticidesAnneeNEntite {
    annee: number;
    quantiteSubstanceSansDUKg: number | null;
    quantiteSubstanceAvecDUKg: number | null;
    noduHa: number | null;
    noduNormalise: number | null;
}

export class IndicateursPesticidesEntite {
    private listeIndicateursParAnnees: Array<IndicateursPesticidesAnneeNEntite> = [];

    ajouterValeursAnneeN(
        annee: number,
        quantiteSubstanceSansDUKg: number | null,
        quantiteSubstanceAvecDUKg: number | null,
        noduHa: number | null,
        noduNormalise: number | null
    ) {
        this.listeIndicateursParAnnees.push({
            annee: annee,
            quantiteSubstanceSansDUKg: quantiteSubstanceSansDUKg,
            quantiteSubstanceAvecDUKg: quantiteSubstanceAvecDUKg,
            noduHa: noduHa,
            noduNormalise: noduNormalise
        });
        this.listeIndicateursParAnnees.sort((ia1, ia2) => {
            return ia1.annee - ia2.annee;
        });
    }

    public get toutesAnnees() {
        return this.listeIndicateursParAnnees;
    }
}

export class ProductionEntite {
    public note: number | null = null;
    public tauxAdequationMoyenPonderePourcent: number | null = null;
    public partSauBioPourcent: number | null = null;
    public indiceHvn: number | null = null;
    public noteTauxAdequationMoyenPondere: number | null = null;
    public notePartSauBio: number | null = null;
    public noteHvn: number | null = null;
}

export class ProximiteCommercesEntite {
    public note: number | null = null;
    public partPopulationDependanteVoiturePourcent: number | null = null;
    public partTerritoireDependantVoiturePourcent: number | null = null;
    public indicateursProximiteCommercesEntite = new IndicateursProximiteCommercesEntite();
}

interface IndicateursProximiteCommercesPourUnTypeCommerceEntite {
    typeCommerce: string;
    distancePlusProcheCommerceMetres: number | null;
    partPopulationAccesAPiedPourcent: number | null;
    partPopulationAccesAVeloPourcent: number | null;
    partPopulationDependanteVoiturePourcent: number | null;
    partTerritoireDependantVoiturePourcent: number | null;
}

export class IndicateursProximiteCommercesEntite {
    ajouterValeursPourUnTypeDeCommerce(
        typeCommerce: string,
        distancePlusProcheCommerceMetres: number | null,
        partPopulationAccesAPiedPourcent: number | null,
        partPopulationAccesAVeloPourcent: number | null,
        partPopulationDependanteVoiturePourcent: number | null,
        partTerritoireDependantVoiturePourcent: number | null
    ) {
        this.listeIndicateursParTypesCommerces.push({
            typeCommerce: typeCommerce,
            distancePlusProcheCommerceMetres: distancePlusProcheCommerceMetres,
            partPopulationAccesAPiedPourcent: partPopulationAccesAPiedPourcent,
            partPopulationAccesAVeloPourcent: partPopulationAccesAVeloPourcent,
            partPopulationDependanteVoiturePourcent: partPopulationDependanteVoiturePourcent,
            partTerritoireDependantVoiturePourcent: partTerritoireDependantVoiturePourcent
        });
    }

    private listeIndicateursParTypesCommerces: Array<IndicateursProximiteCommercesPourUnTypeCommerceEntite> = [];

    public get tousTypesCommerces() {
        return this.listeIndicateursParTypesCommerces;
    }
}

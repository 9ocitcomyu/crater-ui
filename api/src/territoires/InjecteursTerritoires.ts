import { InjecteurCsv, lireCelluleString, TraiterCellules } from '../communs/injecteursUtils';
import { logInfo } from '../communs/logUtils';
import { MapIdsTerritoires } from '../communs/MapIdsTerritoires';
import {
    CATEGORIE_COMMUNE,
    CATEGORIE_DEPARTEMENT,
    CATEGORIE_EPCI,
    CATEGORIE_PAYS,
    CATEGORIE_REGION,
    CATEGORIE_REGROUPEMENT_COMMUNES,
    Commune,
    Departement,
    Epci,
    Pays,
    Region,
    RegroupementCommunes,
    Territoire
} from './domaines/entitesTerritoires';
import { ServicesTerritoires } from './domaines/ServicesTerritoires';

function extraireCodesPostaux(string: string) {
    return string.split('|');
}

export class InjecteursTerritoires {
    private readonly idsTerritoires = new MapIdsTerritoires();

    constructor(private readonly servicesTerritoires: ServicesTerritoires) {}

    private traiterCellulesReferentielTerritoires: TraiterCellules = (mapCellules) => {
        let territoire: Territoire;
        const idTerritoire = this.idsTerritoires.lireCelluleIdTerritoire(mapCellules);
        const nomTerritoire = lireCelluleString(mapCellules, 'nom_territoire');

        switch (mapCellules.get('categorie_territoire')) {
            case CATEGORIE_PAYS:
                territoire = new Pays(idTerritoire, nomTerritoire);
                break;
            case CATEGORIE_REGION:
                territoire = new Region(idTerritoire, nomTerritoire, this.idsTerritoires.lireCelluleIdTerritoire(mapCellules, 'id_pays'));
                break;
            case CATEGORIE_DEPARTEMENT:
                territoire = new Departement(
                    idTerritoire,
                    nomTerritoire,
                    this.idsTerritoires.lireCelluleIdTerritoire(mapCellules, 'id_region'),
                    this.idsTerritoires.lireCelluleIdTerritoire(mapCellules, 'id_pays')
                );
                break;
            case CATEGORIE_REGROUPEMENT_COMMUNES:
                territoire = new RegroupementCommunes(
                    idTerritoire,
                    nomTerritoire,
                    mapCellules.get('categorie_regroupement_communes')!,
                    this.idsTerritoires.lireCelluleIdTerritoire(mapCellules, 'id_departement'),
                    this.idsTerritoires.lireCelluleIdTerritoire(mapCellules, 'id_region'),
                    this.idsTerritoires.lireCelluleIdTerritoire(mapCellules, 'id_pays')
                );
                break;
            case CATEGORIE_EPCI:
                territoire = new Epci(
                    idTerritoire,
                    nomTerritoire,
                    lireCelluleString(mapCellules, 'categorie_epci'),
                    this.idsTerritoires.lireCelluleIdTerritoire(mapCellules, 'id_departement'),
                    this.idsTerritoires.lireCelluleIdTerritoire(mapCellules, 'id_region'),
                    this.idsTerritoires.lireCelluleIdTerritoire(mapCellules, 'id_pays')
                );
                break;
            case CATEGORIE_COMMUNE:
                territoire = new Commune(
                    idTerritoire,
                    nomTerritoire,
                    this.idsTerritoires.lireCelluleIdTerritoire(mapCellules, 'id_epci'),
                    this.idsTerritoires.lireCelluleIdTerritoire(mapCellules, 'id_departement'),
                    this.idsTerritoires.lireCelluleIdTerritoire(mapCellules, 'id_region'),
                    this.idsTerritoires.lireCelluleIdTerritoire(mapCellules, 'id_pays'),
                    extraireCodesPostaux(mapCellules.get('codes_postaux_commune')!)
                );
                break;
            default:
                return;
        }

        this.servicesTerritoires.ajouterTerritoire(territoire);
    };

    private traiterCellulesDonneesTerritoires: TraiterCellules = (mapCellules) => {
        const idTerritoire = this.idsTerritoires.lireCelluleIdTerritoire(mapCellules);
        const territoire = this.servicesTerritoires.rechercher(idTerritoire);
        const idParcel = lireCelluleString(mapCellules, 'id_territoire_parcel');
        territoire!.idParcel = idParcel === '' ? null : idParcel;
    };

    async injecterDonneesTerritoires(dossierDonnees = '../crater-data/output-data'): Promise<void> {
        logInfo("CRATER-API : Début de l'initialisation=" + dossierDonnees);
        await new InjecteurCsv().injecter(
            dossierDonnees + '/crater/territoires/referentiel_territoires/referentiel_territoires.csv',
            this.idsTerritoires.traiterCellulesCorrespondanceIdsTerritoires
        );
        await new InjecteurCsv().injecter(
            dossierDonnees + '/crater/territoires/referentiel_territoires/referentiel_territoires.csv',
            this.traiterCellulesReferentielTerritoires
        );
        await new InjecteurCsv().injecter(
            dossierDonnees + '/crater/territoires/donnees_territoires/donnees_territoires.csv',
            this.traiterCellulesDonneesTerritoires
        );
    }
}

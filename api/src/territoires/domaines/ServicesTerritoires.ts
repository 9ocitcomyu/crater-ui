import { StockageMemoireIndexParId } from '../../communs/servicesUtils';
import {
    CATEGORIE_COMMUNE,
    CATEGORIE_DEPARTEMENT,
    CATEGORIE_EPCI,
    CATEGORIE_REGION,
    CATEGORIE_REGROUPEMENT_COMMUNES,
    Commune,
    Territoire
} from './entitesTerritoires';

// Ces codes sont traités comme des mots clés particuliers dans le critère de recherche pour filtrer les catégories
// CATEGORIE_PAYS n'est pas dans la liste car peu utile, et interfère avec les libellés de regroupements de commune de type PAYS
const CODES_CATEGORIES_TERRITOIRES_POUR_FILTRAGE_RECHERCHE = [
    CATEGORIE_COMMUNE,
    CATEGORIE_EPCI,
    CATEGORIE_REGROUPEMENT_COMMUNES,
    CATEGORIE_DEPARTEMENT,
    CATEGORIE_REGION
];

export class ServicesTerritoires {
    private stockageTerritoires: StockageMemoireIndexParId<Territoire> = new StockageMemoireIndexParId<Territoire>();

    ajouterTerritoire(territoire: Territoire): void {
        this.stockageTerritoires.save(territoire);
    }

    rechercherTous(): Array<Territoire> {
        return this.stockageTerritoires.findAll();
    }

    rechercher(idTerritoire: string): Territoire | undefined {
        return this.stockageTerritoires.findById(idTerritoire);
    }

    rechercherTerritoireParCritere(critere: string, nbMaxResultat: number): Array<Territoire> {
        const categoriesTerritoiresAFiltrer = this.extraireListeCategoriesTerritoiresAFiltrer(critere);
        const critereRecherche = this.extraireCritereRechercheNormalise(critere);

        const listeTerritoires = this.recupererListeTerritoiresFiltresParCategorie(categoriesTerritoiresAFiltrer);

        if (critere.length < 3) return this.rechercherParCorrespondanceExacte(listeTerritoires, critereRecherche, nbMaxResultat);
        else return this.rechercherParCodePostalouNom(listeTerritoires, critereRecherche, nbMaxResultat);
    }

    private extraireListeCategoriesTerritoiresAFiltrer(critere: string) {
        const critereNormalise = ServicesTerritoires.normaliser(critere);
        return CODES_CATEGORIES_TERRITOIRES_POUR_FILTRAGE_RECHERCHE.filter((categorie) => {
            const r = new RegExp(`\\b(${categorie})\\b`);
            return r.test(critereNormalise);
        });
    }

    private extraireCritereRechercheNormalise(critere: string) {
        let critereRecherche = ServicesTerritoires.normaliser(critere);
        CODES_CATEGORIES_TERRITOIRES_POUR_FILTRAGE_RECHERCHE.forEach((c) => {
            const r = new RegExp(`\\b${c}\\b`, 'g');
            critereRecherche = critereRecherche.replace(r, '');
        });
        return critereRecherche.replace(/[ ]+/g, ' ').trim();
    }

    private recupererListeTerritoiresFiltresParCategorie(filtreParCategoriesTerritoires: string[]) {
        let listeTerritoires: Territoire[] = this.stockageTerritoires.findAll().map((t) => t as Territoire);
        if (filtreParCategoriesTerritoires.length > 0) {
            listeTerritoires = listeTerritoires.filter((t) => {
                return filtreParCategoriesTerritoires.includes(t.categorie);
            });
        }
        return listeTerritoires;
    }

    private rechercherParCorrespondanceExacte(listeTerritoires: Territoire[], critereNormalise: string, nbMaxResultat: number) {
        return listeTerritoires.filter((t) => ServicesTerritoires.normaliser(t.nom) === critereNormalise).slice(0, nbMaxResultat);
    }

    private rechercherParCodePostalouNom(listeTerritoires: Territoire[], critereRecherche: string, nbMaxResultat: number) {
        const resultat = new Set<Territoire>();

        const resultatMatchingCodePostal = listeTerritoires.filter(this.contientUnCodePostalContenant(critereRecherche));
        const resultatMatchingMotPartielOuEntier = listeTerritoires.filter(this.contientChaine(critereRecherche));
        const resultatMatchingMotEntier = resultatMatchingMotPartielOuEntier.filter(this.contientMotEntier(critereRecherche));

        resultatMatchingMotEntier.sort(this.triLongeurNomPuisOrdreAlphabetique).forEach((t) => resultat.add(t));
        resultatMatchingMotPartielOuEntier.sort(this.triLongeurNomPuisOrdreAlphabetique).forEach((t) => resultat.add(t));
        resultatMatchingCodePostal.sort(this.triLongeurNomPuisOrdreAlphabetique).forEach((t) => resultat.add(t));

        return Array.from(resultat).slice(0, nbMaxResultat);
    }

    private triLongeurNomPuisOrdreAlphabetique = (t1: Territoire, t2: Territoire) => {
        const compareLength = t1.nom.length - t2.nom.length;
        if (compareLength == 0) {
            return t1.nom.localeCompare(t2.nom);
        } else {
            return compareLength;
        }
    };

    private contientUnCodePostalContenant(codePostalPartiel: string) {
        return (territoire: Territoire) => {
            if (territoire instanceof Commune) {
                const commune = territoire as Commune;
                return commune.codesPostaux.startsWith(codePostalPartiel) || commune.codesPostaux.includes(codePostalPartiel);
            }
            return false;
        };
    }

    private contientMotEntier(mot: string) {
        const r = new RegExp(`\\b(${mot})\\b`);
        return (t: Territoire) => r.test(ServicesTerritoires.normaliser(t.nom));
    }

    private contientChaine(chaine: string) {
        return (t: Territoire) => ServicesTerritoires.normaliser(t.nom).includes(chaine);
    }

    static normaliser(chaine: string): string {
        return ServicesTerritoires.normaliserMotsDeLiaison(ServicesTerritoires.normaliserCaracteres(chaine));
    }

    private static normaliserCaracteres(chaine: string): string {
        return chaine
            .toUpperCase()
            .replace(/\u0152/g, 'OE') // cas du Œ
            .replace(/\u00C6/g, 'AE') // cas du Æ
            .replace(/['|-]/g, ' ') // remplacer ' et - par des espaces
            .normalize('NFD') // remplacer les caractères accentués par leur équivalent sans accent
            .replace(/[\u0300-\u036f]/g, ''); // supprimer certains caractère spéciaux
    }

    private static normaliserMotsDeLiaison(chaine: string): string {
        return (
            chaine
                // "D" et "L" permettent de traiter les mots "D'" et "L'" dans le cas ou l'apostrophe a été supprimée (cf normaliserCaracteres) ou omise par l'utilisateur
                .replace(/\bDE\b|\bDU\b|\bDES\b|\bD\b|\bD'\b/g, ' ')
                .replace(/\bLE\b|\bLA\b|\bLES\b|\bL\b|\bL'\b/g, ' ')
                .replace(/[ ]+/g, ' ') // remplacer n espaces par un seul espace
                .trim()
        );
    }
}

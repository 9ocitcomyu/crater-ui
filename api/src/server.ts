import express = require('express');
import cors = require('cors');
import { logInfo } from './communs/logUtils';
import CraterApp from './CraterApp';

const CHEMIN_CRATER_DATA_RESULTATS = '../crater-data-resultats/data';

logInfo('CRATER-API : [' + new Date().toUTCString() + "] CRATer-API Démarrage de l'application");

const app: express.Application = express();

app.use(cors());
app.use('/crater/api/docs', express.static('api_spec/crater-openapi.yml'));

app.use(
    '/crater/api/cartes',
    express.static(CHEMIN_CRATER_DATA_RESULTATS + '/crater/geojson', {
        maxAge: '1d'
    })
);
app.use(
    '/crater/api/sitemaps',
    express.static(CHEMIN_CRATER_DATA_RESULTATS + '/crater/sitemaps', {
        maxAge: '1d'
    })
);

const craterApp = new CraterApp(app, CHEMIN_CRATER_DATA_RESULTATS);
craterApp.init();

const port = process.env.PORT || 3000;

app.listen(port, function () {
    logInfo(`CRATER-API : [${new Date().toUTCString()}] CRATer-API en ecoute sur le port ${port}`);
});

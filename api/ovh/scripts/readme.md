Scripts d'install : 

* installer-serveur.sh : permet de configurer un serveur from scratch (ubuntu 22.04). A faire uniquement la première fois, ou après réinit du serveur
* renouveler-certificat.sh : permet de renouveler les certificats SSL, à faire la 1ere fois, puis éventuellement quand les certificats sont expirés (mais normallement certbot se charge de le faire automatiquement)
* installer-app.sh : build et configure les applications dev et prod, à faire si besoin après une livraison manuelle (normallement pm2 s'en charge arpès un update des fichiers par le CI/CD) 
* restart-dev.sh et restart-prod.sh : pour redémarrer manuellement les applications dev et prod

Commandes utiles :

* certbot certificates : pour voir les certificats SSL installés et leur date d'expiration
* sudo certbot --dry-run renew : pour tester le renouvellement des certificats SSL (sans les renouveler)
* systemctl list-timers | grep certbot : voir le statut du timer et sa prochaine exécution
* systemctl status certbot.timer : pour voir si le timer de renouvellement des certificats est bien configuré
* systemctl status certbot.service : pour voir si le service de renouvellement des certificats est bien configuré, et le résultat de la dernière exécution
* pm2 list : pour voir les applications en cours d'exécution
* pm2 logs : pour voir les logs des applications en cours d'exécution
* pm2 monit : pour voir les stats des applications en cours d'exécution
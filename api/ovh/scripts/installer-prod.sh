#!/bin/bash
# Install d'une nouvelle version de l'application

# pour récuperer l'env node (défini dans bashrc, mais pas lu lors de l'execution du script a distance)
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm

# MAJ conf apache
sudo cp ../apache/crater-api.apache.conf /etc/apache2/sites-available/crater-api.conf
sudo a2ensite crater-api.conf
sudo systemctl reload apache2

# Build des apps (l'arborescence est celle livrée sur le serveur, voir api/build et gitlab-ci.yml)
cd ../../prod/crater-api
npm ci --omit=dev

# Maj conf pm2 et redemarrage
cd ../../ovh/pm2

pm2 stop  ecosystem.config.js --only crater-api-prod
pm2 start ecosystem.config.js --only crater-api-prod
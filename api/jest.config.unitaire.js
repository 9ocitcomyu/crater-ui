module.exports = {
    roots: ['<rootDir>/test'],
    testMatch: ['**/unitaire/**/?(*.)+(spec|test).+(ts|tsx|js)'],
    transform: {
        '^.+\\.(ts|tsx)$': 'ts-jest'
    }
};

Objectif du document : décrire les principaux choix d’architecture technique de l’application web CRATer, ainsi que les principales guidelines techniques

Ce document ne décrit pas encore : l'application transition-alimentaire.fr et le composant crater-api

[[_TOC_]]

# Périmètre de l’application CRATer

L’application propose 3 fonctionnalités principales :

1. Consulter le diagnostic pour un territoire en particulier, un diagnostic étant présenté sous forme de plusieurs pages contenant textes, données chiffrées, et graphiques divers
2. Visualiser les résultats pour un indicateur donné et sur un ensemble de territoire via une carte
3. Comprendre les enjeux liés au sujet de la transition agricole et alimentaire, au travers de différents articles, et de pages expliquant la méthodologie utilisée pour les calculs (principalement du contenu éditorial statique)

L’application est utilisable sur tous types de terminaux : écrans desktop larges, tablettes, écrans mobiles en mode portrait.

Elle est construite sous la forme



* d’une SPA (single page application)
* responsive (la même app s’adapte à tous les écrans)
* qui utilise une api pour s’alimenter en données, api que l’on construit et peut faire évoluer sur mesure pour les besoins de l’app. Voir le repo[ https://framagit.org/lga/crater-api](https://framagit.org/lga/crater-api) Rq : ce repo est un projet autonome, il n’est pas décrit dans le périmètre de ce document

L’approche retenue pour l’architecture de l’app web est la suivante :



* architecture orientée composants, qui permet de construire les écrans par assemblage de briques réutilisables
* avec isolation des règles de gestion dans des modules spécifiques, les modules du « domaine » métier (= en référence à Clean Architecture ou Architecture Hexagonale) , qui permettent de stocker et véhiculer facilement l’état de l’application, tout en centralisant les règles de gestion les plus essentielles, et en les rendant facilement testables


# Web components et lit-element

Les composants graphiques sont construits sous forme de web components (voir[ https://developer.mozilla.org/fr/docs/Web/Web_Components](https://developer.mozilla.org/fr/docs/Web/Web_Components)) en s’appuyant les standards associés : déclaration de custom elements, utilisation du shadow dom pour l’encapsulation, et utilisation des éléments html &lt;template> et &lt;slot>.

La mise en oeuvre s’appuie sur la librairie lit-element qui offre une surchouche très légère sur les standards web components tout en facilitant l’implémentation. Voir [https://lit.dev/docs/](https://lit.dev/docs/)




# Routage des urls

Le routage est réalisé dans le composant CraterApp.ts, il permet d’identifier la page à afficher en fonction de l’url saisie. Il s’appuie sur pagejs.

# Gestion de l’état

La conservation de l’état est faite à plusieurs niveaux :

* CraterApp.ts (le composant racine) est responsable des données de contexte globale de l’application (essentiellement des données liées au routage)
* Chaque page est responsable de son domaine métier, elle est donc amenée à créer et mettre à jour l’objet de son domaine
* Chaque composant peut être responsable de données relatives à son état

# Communication et partage de données entre composants

Le partage de données depuis un composant parent vers un composant enfant se fait :

* Comme « attribute expression » : En passant les informations en tant qu’attribut du composant enfant, lors du render ( voir[ https://lit.dev/docs/templates/expressions/#attribute-expressions](https://lit.dev/docs/templates/expressions/#attribute-expressions) ). Valable pour les types simples
* Comme « property expression » :Pour les types comlexes (e.g. une instance d’objet du domaine), en passant les informations en tant que propriété du composant enfant, lors du render (voir[ https://lit.dev/docs/templates/expressions/#property-expressions](https://lit.dev/docs/templates/expressions/#property-expressions) )
* Les 2 approches précédentes reposent sur les mécanismes offerts par lit-element, elles sont a privilégier car permettent d’avoir un code très simple
* En complément, il est également possible d’appeler une méthode du composant enfant depuis le composant parent, en passant les données en tant que paramètre (voir certaines méthodes « maj() » dans le code, qui sont censées être remplacées progressivement par un passage de données property expression). Certaines parties du code utilisent encore ce fonctionnement, mais il va être progressivement remplacé par les 2 approches précédentes.

Le partage de données depuis un composant enfant vers un composant parent se fait via émission d’un évènement, qui outre son type peut s’accompagner de certaines données.

Seul exception à cette transmission descendante des données, et cette remontée d’évènements : le domaine systemealimentaire, qui est instancié sous forme d’une variable globale. De cette manière il est accessible à l’ensemble des composants en direct. Ces données peuvent en effet être considérées comme des données statiques de configuration, accessibles à tous. Cela évite de transférer de proche en proche l’instance du système alimentaire sur toute la hiérarchie de composants.


# Cycle de vie et mise à jour de l'affichage

L'affichage suit le cycle de vie suivant :
* basé sur le cycle de vie des [composants lit-element](https://lit.dev/docs/components/lifecycle/)
* le render de l'application déclenche le render de chaque composant enfants de manière récursive
* de manière générale, quand l'application change d'état (cad lorsqu'un évènement est capté), on effectue les actions suivantes :
  * on capte l'évènement soit au niveau du composant racine (craterApp.ts) soit au niveau d'une page
  * en fonction de l'évènement, on appelle le traitement correspondant sur le domaine, qui se met à jour et retourne le nouvel état
  * le composant craterApp ou la page déclenche ensuite son ré affichage (et en cascade celui de ses composants enfants)
* cela signifie que sauf exception, les composants intermédiaires ou bas niveau ne se mettent pas à jour directement. Ils publient des évènements, qui remontent au composant racine (ou a une page), pour que l'on puisse mettre à jour le domaine en cohérence, puis demander le réaffichage. Il y a quelques exceptions à cette règle (ex : changer les options d'affichage d'un graphique => on met à jour le graphique sans remonter l'évènement).
* cette approche ne pose pas de problème de performance car lit-element se charge de ne réafficher que ce qui a vraiment changé


# Affichage responsive et media queries

L'application est responsive. Ce sont les composants de plus haut niveau (page ou fragements de pages) qui ont pour responsabilité de gérer la mise en page en fonction de la taille d'écran. Ce sont eux qui portent les media query.

# Les différents types de composants et leur responsabilité

Composant craterApp.ts
* Porte la logique de routage, cad affichage de la page correspondant à chaque route.
* Maintient l’état de niveau application (essentiellement pour mémoriser la route/page courante)
* Ecoute et traite les évènements de niveau application (erreur, nav)
 
Composant de type Page (src/pages/xxx/Page*.ts)
* Afficher une page donnée
* Maintient l’état au travers des données métier (le domaine), délègue si besoin la création/modification du domaine à une classe « Controleur » qui s’appuie sur les API
* Ecoute et traite les évènements de niveau page, et décide des modification à faire sur le domaine en conséquence
* Gère le cycle de vie du domaine et pilote les render sur les sous composants
 
Composant de type template (src/page/commun/template/Template*.ts) :
* Affichage une page pour une mise en page donnée
* S'occupe du render d'une partie seulement des zones de la page, le reste étant des zones "à remplir" par les composants utilisant le template (implémentation sous forme de slot)
* une Page utilise typiquement un template qu'elle vient compléter avec son propre contenu

Sous composants relatifs à une page (ex src/pages/diagnostics/chapitres)
* Chaque page s’appuie sur des composants de premier niveaux (enfants directs de la page)
* Ces composants peuvent accéder aux données métiers (ex objet du domaine Rapport), mais en lecture seule
* Ils réagissent aux modifications du rapport pour propager les modifications à faire dans l’affichage. Ne transfert pas le rapport, mais extrait les données nécessaires à chaque sous composant

Composants intermédiaire (ex src/pages/diagnostic/charts) 
* Ce type de composants pilote l’affichage d’une zone en particulier de la page
* Utilise des composants basiques, ou d’autres composants intermédiaires. A part cela, est indépendant de tout le reste de l’application (en particulier ne connait pas le domaine).
* Peut être réutilisable dans plusieurs écrans de l’application
* Question : est ce qu’on s’autorise à dépendre de  domaine/systemealimentaire ? Ca simplifierait bcp les api de ces composants car évite de déclarer certaines structures de données ad hoc (par ex Indicateur, ou Maillon que l’on retrouve très souvent) ? |

Composant basique (modules/design-system)   
* Composant le plus simple, forte généricité et réutilisabilité (potentiellement en dehors de l’application)
* Ne dépend de personne d’autre (a part d’un autre composant de base)

# Organisation des dossiers

Le projet est organisé sous forme de monorepo. Description des dossiers principaux dans /src : 

* /apps : les app web définies dans le projet
  * /apps/crater : l'application CRATer (objet de ce doc). Voir détail dans le 2eme tableau après celui-ci
  * /apps/transition-alimentaire : l'application transition-alimentaire (non traitée ici)
* /modules : modules (au sens node) partagés par les apps
  * /modules/commun : Module contenant des composants intermédiaires et ressources partagés par plusieurs apps ou pages
    * /modules/commun/.../champ-recherche-territoire : Composant de recherche territoire (composant intermédiaire)
    * /modules/commun/.../outils : Module contenant certaines fonctions utilitaires partagés par les apps & modules (fetchRetry, formatage nombre...)
  * /modules/design-system : Module contenant les composants de base et ressources relatifs au design system de l’application. Module utilisé par tous les autres composants de l’app. Il contient également la définition de certains évènements et des styles faisant partie du design system (sous forme de constantes js contenant des templates css)

Pour l’application CRATer, les principaux fichiers et dossiers dans apps/crater/src sont :

* CraterApp.ts : Point d’entrée dans l’application. Porte la logique de routage, cad affichage de la page correspondant à chaque route. Maintient l’état de niveau application (essentiellement pour mémoriser la route/page courante). Ecoute et traite les évènements de niveau application (erreur, nav)
* /pages : 
  * Un sous dossier par page de l’application qui peut contenir le composant Pagexxx.ts, un fichier css, un fichier Controleurxxx.ts  (le controleur gère l’échange avec l’api, et la création modification des objets du domaine), ainsi que l’ensemble des fichiers et dossiers décrivant les sous composants (sous composants relatifs à la page + composants intermédiaires relatifs à la page)
  * /pages/commun : Dossier contenant les composants intermédiaires utilisés par plusieurs pages
  * /pages/commun/template : Dossier contenant les composants templates utilisés par plusieurs pages
* /domaines : Modules indépendants du reste de l’application qui portent l’état et la logique métier
  * /domaines/territoires : Données sur les territoires
  * /domaines/systeme-alimentaire : Modélisation du système alimentaire
    * /domaines/systeme-alimentaire/configuration : Données qui permettent de créer les objets SystemeAlimentaire (contrairement aux autres dossiers du domaine, cette partie n’est pas alimentée depuis des données externes via une api, on considère que c’est une données coeur qui fait partie de la configuration)


# Stratégie de tests

## Types de test

3 types de tests sont mis en oeuvre :

* le code des domaines est testé unitairement. Les sources des tests sont situés dans le même dossier que les sources testées, et portent l’extension .test.ts
* les composants lit sont testés unitairement via cypress (component test). Voir les dossiers .../cypress/component
* les apps sont testées en condition réelle via des tests bout en bout cypress (e2e tests). Voir les dossiers .../cypress/e2e

## Bonnes pratiques pour chaque type de test

Tests unitaires des domaines : 
* on vise une couverture à 100% du code des domaines par les tests unitaires

Tests de composants avec cypress :
* ces tests sont utiles pour contrôler visuellement l'affichage des composants sans avoir à lancer l'application
* on ne cherche pas à tester toutes les propriétés css, mais uniquement les comportements significatifs
* en conséquence on cherchera à faire peu de tests, qui permettent de visualiser les principales configuration d'affichage du composant, et qui comportent peu d'assertion ciblées par exemple la capacité responsive de certains composants

Tests "end to end" cypress :
* ces tests vérifient le fonctionnement nominal des principaux parcours de l'application
* parcours à tester :
  * recherche territoire depuis l'accueil, consultation diagnostic (synthèse), page maillon
  * consultation carte
  * ... à définir


# Technologiques utilisées

L’ensemble de l’application est codée en typescript, avec les principales lib suivantes :



* Lit-element : mise en oeuvre des web components
* Pagejs : routage
* Leaflet : composant carte
* Apexhcarts : charts de type histogrammes, line charts, radar et treemap
* Vitejs est utilisé pour le build


# Design system

Voir le design system sur figma :  [https://www.figma.com/file/WGjjnOPR6AsGrF9Az7UGQL/Design-System-%E2%80%A2-Crater](https://www.figma.com/file/WGjjnOPR6AsGrF9Az7UGQL/Design-System-%E2%80%A2-Crater)

## Composants

Les composants proposés par le design system sont définis dans design-system/src/composants

## Theme

Le design system suppose qu'un theme a été importé au préalable pour définir :

* les 3 font-family utilisées
* le jeux de couleurs sous forme de variables css
* l'unité de marge en pixel sous forme de variable css

Voir le dossier design-system/public/theme-defaut pour le theme fourni par défaut, en particulier le fichier css qui contient toutes ces définitions. 

Ce fichier de thème peut être surchargée par une application utilisant le design system. Il doit être importé par le composant parent, au niveau de la racine du DOM, ce qui rend ses variables css disponibles partout dans l'application.

## Styles partagés

Le design system mets également a disposition des éléments de style partagé.

Voir design-system/src/styles

On trouve en particulier les styles de textes à utiliser dans l'ensemble de l'application pour mettre en forme les titres et les paragraphes.:
* Corps de texte (body) : texte-petit, texte-moyen, texte-titre, texte-alternatif
* Titre : titre-petit, titre-moyen, titre-large












 

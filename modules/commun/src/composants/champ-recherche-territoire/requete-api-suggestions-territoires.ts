import { fetchRetry } from '../../outils/requetes-utils';
import { SuggestionTerritoire } from './SuggestionTerritoire';

const NB_MAX_RESULTATS_TERRITOIRES = 10;

export function chargerListeSuggestionsTerritoires(apiBaseUrl: string, texteSaisi: string): Promise<SuggestionTerritoire[]> {
    texteSaisi = texteSaisi.toLowerCase();
    const url = new URL(apiBaseUrl + `crater/api/territoires?critere=${texteSaisi}&nbMaxResultats=${NB_MAX_RESULTATS_TERRITOIRES}`);

    return fetchRetry(url.toString())
        .then((response) => response.json())
        .then((territoiresJson: unknown[]) => {
            return traduireJsonEnListeSuggestionTerritoires(territoiresJson);
        });
}

function traduireJsonEnListeSuggestionTerritoires(territoiresJson: unknown[]): SuggestionTerritoire[] {
    const territoires: SuggestionTerritoire[] = [];
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    territoiresJson.forEach((territoireJson: any) => {
        territoires.push({
            id: territoireJson.id,
            idParcel: territoireJson.idParcel,
            nom: territoireJson.nom,
            libellePrincipal: territoireJson.libellePrincipal,
            libelleSecondaire: territoireJson.libelleSecondaire,
            estSuggere: territoireJson.estSuggere || false
        });
    });
    return territoires;
}

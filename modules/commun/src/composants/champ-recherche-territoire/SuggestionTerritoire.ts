export interface SuggestionTerritoire {
    id: string;
    idParcel: string;
    nom: string;
    libellePrincipal: string;
    libelleSecondaire: string;
    estSuggere: boolean;
}

async function attendre(ms: number) {
    return new Promise((resolve) => setTimeout(resolve, ms));
}

const TIMEOUT_TOTAL_FETCH_RETRY_MS = 30000;
const DELAI_ENTRE_RETRY_MS = 1000;

function fetchRetryRecursif(url: string, tentativesRestantes: number): Promise<Response> {
    return fetch(url).then(async (response) => {
        if (response.status === 503) {
            if (tentativesRestantes > 1) {
                console.warn(`FetchRetry de l'url ${url}, erreur 503, nouvelle tentative dans 1 seconde`);
                await attendre(DELAI_ENTRE_RETRY_MS);
                return fetchRetryRecursif(url, tentativesRestantes - 1);
            } else {
                return Promise.reject(new Error(`Délai expiré, pas de réponse de l'api après plusieurs tentative à l'url ${url}`));
            }
        } else return response;
    });
}

export function fetchRetry(url: string, delaiGlobalMs: number = TIMEOUT_TOTAL_FETCH_RETRY_MS): Promise<Response> {
    return fetchRetryRecursif(url, delaiGlobalMs / DELAI_ENTRE_RETRY_MS);
}

export function toJson(objet: Object) {
    return JSON.stringify(
        objet,
        function (k, v) {
            return v === undefined ? 'undefined' : v;
        },
        2
    );
}

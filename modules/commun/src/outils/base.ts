export function arrondirANDecimales(nombre: number, nbDecimales = 1) {
    const facteur = Math.pow(10, nbDecimales);
    return Math.round(nombre * facteur) / facteur;
}

export function calculerValeurEntiereNonNulle(nombre: number | null): number {
    if (nombre === null) {
        return 0;
    }
    return Math.round(nombre);
}

export function clip(valeur: number, borneInf: number, borneSup: number) {
    return Math.max(borneInf, Math.min(valeur, borneSup));
}

export function sommeArray(array: (number | null)[]): number | null {
    if (array.length === 0) return null;

    return array.reduce((a, b) => (a ?? 0) + (b ?? 0));
}

export function formaterNombreEnEntierString(nombre: number | null | undefined, retournerValeurAbsolue = false): string {
    return formaterNombreEnNDecimalesString(nombre, 0, retournerValeurAbsolue);
}

export function formaterNombreEnNDecimalesString(nombre: number | null | undefined, nbDecimales = 1, retournerValeurAbsolue = false) {
    if (nombre === null || nombre === undefined || isNaN(nombre)) {
        return '-';
    }
    retournerValeurAbsolue ? (nombre = Math.abs(nombre)) : '';

    return arrondirANDecimales(nombre, nbDecimales).toLocaleString('fr-FR');
}

export function formaterNombreSelonValeurString(nombre: number | null | undefined, prefixePlusSiPositif = false) {
    if (nombre === null || nombre === undefined || isNaN(nombre)) return '-';

    let prefixe = '';
    if (prefixePlusSiPositif && nombre > 0) prefixe = '+';

    if (Math.abs(nombre) > 5) {
        return prefixe + formaterNombreEnEntierString(nombre);
    } else if (Math.abs(nombre) > 0.1) {
        return prefixe + formaterNombreEnNDecimalesString(nombre, 1);
    } else {
        return prefixe + formaterNombreEnNDecimalesString(nombre, 2);
    }
}

export function formaterEvolutionPourcentString(evolution: number | null | undefined): string {
    if (evolution === null || evolution === undefined || isNaN(evolution)) {
        return '-%';
    } else if (evolution < 100) {
        return (evolution >= 0 ? '+' : '') + formaterNombreSelonValeurString(evolution) + '%';
    } else {
        return 'x' + formaterNombreSelonValeurString(evolution / 100 + 1);
    }
}

export function calculerEtFormaterEvolutionString(nombreInitial: number | null | undefined, nombreFinal: number | null | undefined): string {
    let evolution;
    if (
        nombreInitial === null ||
        nombreInitial === undefined ||
        isNaN(nombreInitial) ||
        nombreFinal === null ||
        nombreFinal === undefined ||
        isNaN(nombreFinal)
    ) {
        evolution = null;
    } else evolution = 100 * (nombreFinal / nombreInitial - 1);

    return formaterEvolutionPourcentString(evolution);
}

export function formaterNombreEnEntierHtml(nombre: number | null | undefined): string {
    if (nombre === null || nombre === undefined || isNaN(nombre)) {
        return `-`;
    }
    return `<span class="chiffre">${Math.round(nombre).toLocaleString('fr-FR')}</span>`;
}

// Fusionne les champs de objet2 dans objet1. Fonctionne sur tous les niveaux (deep merge).
// Si le même champ est présent dans objet1 et objet2, la valeur de objet2 vient écraser celle de objet1
// TODO ajouter des TUs
export function fusionner(objet1: Object, objet2: Object) {
    const resultat = {};
    mergeDeep(resultat, objet1, objet2);
    return resultat;
}

function isObject(item: undefined | Object) {
    return item && typeof item === 'object' && !Array.isArray(item);
}

// TODO ajouter TU, virer les any
// eslint-disable-next-line @typescript-eslint/no-explicit-any
function mergeDeep(target: any, ...sources: any[]): any {
    if (!sources.length) return target;
    const source = sources.shift();

    if (isObject(target) && isObject(source)) {
        for (const key in source) {
            if (isObject(source[key])) {
                if (!target[key]) Object.assign(target, { [key]: {} });
                mergeDeep(target[key], source[key]);
            } else {
                Object.assign(target, { [key]: source[key] });
            }
        }
    }

    return mergeDeep(target, ...sources);
}
export enum Signe {
    NULL = 'NULL',
    ZERO = 'ZERO',
    POSITIF = 'POSITIF',
    NEGATIF = 'NEGATIF'
}

export function calculerSigneNombre(nombre: number | null): Signe {
    if (nombre === null) return Signe.NULL;
    else if (nombre === 0) return Signe.ZERO;
    else if (nombre > 0) return Signe.POSITIF;
    else return Signe.NEGATIF;
}

export enum QualificatifEvolution {
    progres = 'progres',
    declin = 'declin',
    neutre = 'neutre',
    valide = 'validé',
    rate = 'rate'
}

export function calculerQualificatifEvolution(signe: Signe, correlation: 'positive' | 'negative' = 'positive'): QualificatifEvolution {
    if (signe === Signe.POSITIF) {
        if (correlation === 'positive') return QualificatifEvolution.progres;
        else return QualificatifEvolution.declin;
    } else if (signe === Signe.NEGATIF) {
        if (correlation === 'positive') return QualificatifEvolution.declin;
        else return QualificatifEvolution.progres;
    }
    return QualificatifEvolution.neutre;
}

export function calculerPourcentageString(numerateur: number | null, denominateur: number | null): string {
    if (numerateur === null || denominateur === null || denominateur === 0) {
        return '-';
    } else {
        return formaterNombreEnEntierString((numerateur / denominateur) * 100);
    }
}

export function supprimerEspacesRedondantsEtRetoursLignes(texte: string) {
    //remplace espaces et retours chariots multiple en espaces simples
    return texte.replace(/(\s|\n)+/g, ' ').trim();
}

export function calculerEvolutionParPasDeTemps(evolutionAnnuelle: number | null): { nombre: number | null; periode: string | null } {
    if (evolutionAnnuelle === null) {
        return {
            nombre: null,
            periode: null
        };
    }
    let pasDetemps = 'tous les dix ans';
    let evolution = Math.abs(evolutionAnnuelle * 10);
    if (evolution / 2 > 5) {
        evolution = evolution / 2;
        pasDetemps = 'tous les cinq ans';
        if (evolution / 5 > 5) {
            evolution = evolution / 5;
            pasDetemps = 'par an';
            if (evolution / 12 > 5) {
                evolution = evolution / 12;
                pasDetemps = 'par mois';
                if ((evolution * 12) / 52 > 5) {
                    evolution = (evolution * 12) / 52;
                    pasDetemps = 'par semaine';
                    if (evolution / 7 > 5) {
                        evolution = evolution / 7;
                        pasDetemps = 'par jour';
                        if (evolution / 24 > 5) {
                            evolution = evolution / 24;
                            pasDetemps = 'par heure';
                            if (evolution / 60 > 5) {
                                evolution = evolution / 60;
                                pasDetemps = 'par minute';
                                if (evolution / 60 > 5) {
                                    evolution = evolution / 60;
                                    pasDetemps = 'par seconde';
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    return {
        nombre: evolutionAnnuelle < 0 ? arrondirANDecimales(evolution, 0) * -1 : arrondirANDecimales(evolution, 0),
        periode: pasDetemps
    };
}

import '../../public/theme-defaut/theme-defaut.css';
import '../../src/composants/RadioBouton.js';

import { html } from 'lit';

import { OptionsRadioBouton, RadioBouton } from '../../src/composants/RadioBouton';
RadioBouton;
describe('Test Radio Boutons', () => {
    it('Test affichage nominal', () => {
        // given
        cy.viewport(500, 500);
        const optionsBoutons: OptionsRadioBouton = {
            boutons: [
                {
                    id: 'item-libelle-secondaire',
                    libelle: 'Item avec libellé secondaire',
                    sousLibelle: 'Libellé secondaire'
                },
                {
                    id: 'item-simple',
                    libelle: 'Item simple'
                }
            ],
            idBoutonActif: 'item-libelle-secondaire'
        };

        cy.document()
            .then((doc) => {
                doc.addEventListener('selectionner', cy.stub().as('selectionner'));
            })
            .mount<'c-radio-bouton'>(html` <c-radio-bouton .options="${optionsBoutons}"> </c-radio-bouton> `);
        // then
        cy.get('c-radio-bouton').find('label').eq(1).click();
        cy.get('@selectionner').should('have.been.calledOnce');
    });
});

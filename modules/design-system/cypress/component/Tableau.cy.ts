import '../../public/theme-defaut/theme-defaut.css';
import '../../src/composants/Tableau.js';

import { html } from 'lit';

import { OptionsTableau } from '../../build/composants/Tableau';

describe('Test Tableau', () => {
    it('Test affichage nominal', () => {
        // given
        cy.viewport(500, 500);

        const options: OptionsTableau = {
            entetes: [`Entête 1`, `Entête 2`],
            contenuHtml: [
                [`Contenu 1`, `Contenu 2`],
                [`Contenu 3`, `Contenu 4`],
                [`Contenu 5`, `Contenu 6`]
            ],
            ligneFin: [`Fin 1`, `Fin 2`]
        };

        cy.mount<'c-tableau'>(html` <c-tableau .options="${options}"> </c-tableau> `);
        // then
        cy.get('c-tableau').find('thead tr').should('have.length', 1);
        cy.get('c-tableau').find('tbody tr').should('have.length', 3);
        cy.get('c-tableau').find('tfoot tr').should('have.length', 1);
    });
});

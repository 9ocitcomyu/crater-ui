import '../../public/theme-defaut/theme-defaut.css';
import '../../src/composants/MenuSousItem.js';

import chaiColors from 'chai-colors';
import { html } from 'lit';

import { VALEURS_COULEURS } from './outils_tests';

// permet de comparer des couleurs hex rgb, etc...
chai.use(chaiColors);

describe('Test composant MenuSousItem', () => {
    it('Sous-item non selectionné', () => {
        // given
        cy.mount<'c-menu-sous-item'>(html`<c-menu-sous-item libelle="Sous-item non sélectionné"></c-menu-sous-item>`);
        //then
        cy.get('c-menu-sous-item').find('a').should('have.css', 'color').and('be.colored', VALEURS_COULEURS.couleur_primaire);
    });

    it('Sous-item selectionné', () => {
        // given
        cy.mount<'c-menu-sous-item'>(html`<c-menu-sous-item libelle="Sous-item sélectionné" selectionne></c-menu-sous-item>`);
        //then
        cy.get('c-menu-sous-item').find('a').should('have.css', 'background-color').and('be.colored', VALEURS_COULEURS.couleur_neutre_clair);
    });
});

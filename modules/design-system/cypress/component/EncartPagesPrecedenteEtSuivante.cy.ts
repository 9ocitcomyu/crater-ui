import '../../src/composants/EncartPagesPrecedenteEtSuivante.js';
import '../../public/theme-defaut/theme-defaut.css';

import { html } from 'lit';

describe('Test EncartPagesPrecedenteEtSuivante', () => {
    describe('Desktop', () => {
        it("Créer des boutons gauche, droite et vérifie qu'ils existent", () => {
            //Given
            cy.viewport(900, 300);
            cy.mount<'c-encart-pages-precedente-suivante'>(
                html`<c-encart-pages-precedente-suivante
                    libellePagePrecedente="le système alimentaire"
                    hrefPagePrecedente="/page-precedente"
                    libellePageSuivante="les menaces"
                    hrefPageSuivante="/page-suivante"
                ></c-encart-pages-precedente-suivante>`
            );
            //Then
            cy.get('c-encart-pages-precedente-suivante').find('.bouton-gauche').should('exist');
            cy.get('c-encart-pages-precedente-suivante').find('.bouton-gauche').contains('le système alimentaire');
            cy.get('c-encart-pages-precedente-suivante').find('.bouton-gauche').should('have.attr', 'href', '/page-precedente');
            cy.get('c-encart-pages-precedente-suivante').find('.bouton-gauche').find('svg').should('be.visible');

            cy.get('c-encart-pages-precedente-suivante').find('.bouton-droit').should('exist');
            cy.get('c-encart-pages-precedente-suivante').find('.bouton-droit').contains('les menaces');
            cy.get('c-encart-pages-precedente-suivante').find('.bouton-droit').should('have.attr', 'href', '/page-suivante');
            cy.get('c-encart-pages-precedente-suivante').find('.bouton-droit').find('svg').should('be.visible');
        });

        it("Vérifier qu'il y a absence de bouton gauche si il n'y a pas de 'hrefPagePrecedente'", () => {
            //Given
            cy.viewport(900, 300);
            cy.mount<'c-encart-pages-precedente-suivante'>(
                html`<c-encart-pages-precedente-suivante
                    libellePageSuivante="les menaces"
                    hrefPageSuivante="/page-suivante"
                ></c-encart-pages-precedente-suivante>`
            );
            //Then
            cy.get('c-encart-pages-precedente-suivante').find('.bouton-gauche').should('not.exist');
        });

        it("Vérifier qu'il y a absence de bouton droit si il n'y a pas de 'hrefPageSuivante'", () => {
            //Given
            cy.viewport(900, 300);
            cy.mount<'c-encart-pages-precedente-suivante'>(
                html`<c-encart-pages-precedente-suivante
                    libellePagePrecedente="le système alimentaire"
                    hrefPagePrecedente="/page-precedente"
                ></c-encart-pages-precedente-suivante>`
            );
            //Then
            cy.get('c-encart-pages-precedente-suivante').find('.bouton-droit').should('not.exist');
        });
    });
    describe('Mobile', () => {
        it("Le composant doit s'afficher correctement dans une largeur de 320px", () => {
            //Given
            cy.viewport(320, 300);
            cy.mount<'c-encart-pages-precedente-suivante'>(
                html`<c-encart-pages-precedente-suivante
                    libellePagePrecedente="un libelle super long à lire sur mobile"
                    hrefPagePrecedente="/page-precedente"
                    libellePageSuivante="un autre libelle super long à lire sur mobile"
                    hrefPageSuivante="/page-suivante"
                ></c-encart-pages-precedente-suivante>`
            );
            //Then
            cy.get('c-encart-pages-precedente-suivante').find('.bouton-gauche').should('exist');
            cy.get('c-encart-pages-precedente-suivante').find('.bouton-droit').should('exist');
            cy.window().scrollTo('right', { ensureScrollable: false });
            cy.window().its('scrollX').should('be.equal', 0);
        });
    });
});

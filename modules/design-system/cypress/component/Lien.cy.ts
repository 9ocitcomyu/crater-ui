import '../../public/theme-defaut/theme-defaut.css';

import chaiColors from 'chai-colors';
import { html } from 'lit';

import { Lien } from '../../src/composants/Lien';

Lien;

// permet de comparer des couleursnpm run cypress:app -w modules/design-system hex rgb, etc...
chai.use(chaiColors);

describe('Test composant Lien', () => {
    it('Lien externe', () => {
        // given
        cy.viewport(320, 400);
        cy.mount<'c-lien'>(
            html`<p>
                Exemple de lien externe au milieu d'un paragraphe (<c-lien href="https://crater" libelle="lien externe"></c-lien>) pour vérifier qu'il
                n'impacte pas la hauteur de ligne .
            </p>`
        );
        //then
        cy.get('c-lien').find('a').should('have.css', 'color');
    });

    it('Lien interne', () => {
        // given
        cy.mount<'c-lien'>(html`<p>Exemple de <c-lien href="#cible" libelle="lien interne"></c-lien>, sans icone</p>`);
        //then
        cy.get('c-lien').find('svg').should('not.exist');
    });
});

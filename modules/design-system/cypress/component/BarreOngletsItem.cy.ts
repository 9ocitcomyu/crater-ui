import '../../src/composants/BarreOngletsItem.js';
import '../../public/theme-defaut/theme-defaut.css';

import { html } from 'lit';
import { BarreOngletsItem } from '../../src/composants/BarreOngletsItem.js';

describe('Test composant BarreOngletsItem', () => {
    it('Onglet sélectionné ou non sélectionné', () => {
        // given
        cy.mount<'c-barre-onglets-item'>(
            html`<c-barre-onglets-item
                id="item-selectionne"
                libelle="Onglet sélectionné"
                sousLibelle="Sous-libellé"
                selectionne
            ></c-barre-onglets-item>`
        );
        cy.mount('<br>');
        cy.mount<'c-barre-onglets-item'>(
            html`<c-barre-onglets-item id="item-non-selectionne" libelle="Onglet non sélectionné" sousLibelle="Sous-libellé"></c-barre-onglets-item>`
        );
        //then
        cy.get('#item-selectionne').find('div').should('have.class', 'selectionne');
        cy.get('#item-non-selectionne').find('div').should('not.have.class', 'selectionne');
    });

    it('Onglet avec theme simple', () => {
        // given
        cy.mount<'c-barre-onglets-item'>(
            html`<c-barre-onglets-item
                libelle="Onglet avec un style simple"
                href="./test"
                selectionne
                theme=${BarreOngletsItem.THEME.simple}
            ></c-barre-onglets-item>`
        );
        //then
        cy.get('c-barre-onglets-item').find('div').should('have.class', 'theme-simple');
    });

    it('Onglet avec href et sans sous libellé', () => {
        // given
        cy.mount<'c-barre-onglets-item'>(
            html`<c-barre-onglets-item libelle="Onglet avec href sans sous libellé" href="./test" selectionne></c-barre-onglets-item>`
        );
        //then
        cy.get('c-barre-onglets-item').find('div').should('have.class', 'selectionne');
    });

    it('Onglet avec libellé html', () => {
        // given
        cy.mount<'c-barre-onglets-item'>(
            // eslint-disable-next-line lit/attribute-value-entities
            html`<c-barre-onglets-item libelle="Libellé <abbr title='HyperText Markup Language'>HTML</abbr>"></c-barre-onglets-item>`
        );
        //then
        cy.get('c-barre-onglets-item').find('a').should('contain', 'Libellé HTML');
    });
});

import { css, html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { ifDefined } from 'lit/directives/if-defined.js';
import { unsafeHTML } from 'lit/directives/unsafe-html.js';

import { STYLES_DESIGN_SYSTEM } from '../styles/styles-design-system.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-barre-onglets-item': BarreOngletsItem;
    }
}

@customElement('c-barre-onglets-item')
export class BarreOngletsItem extends LitElement {
    static readonly THEME = {
        classique: 'classique',
        simple: 'simple'
    };
    static styles = [
        STYLES_DESIGN_SYSTEM,
        css`
            .theme-classique {
                --couleur-selection: var(--couleur-primaire);
                --hauteur-selection: 4px;
            }
            .theme-simple {
                --couleur-selection: var(--couleur-neutre);
                --hauteur-selection: 3px;
            }

            #div-conteneur {
                max-width: 210px;
                width: fit-content;
                position: relative;
            }

            #div-conteneur.theme-classique {
                padding: 0 calc(2 * var(--dsem));
            }

            #div-conteneur.theme-simple {
                margin: 0 calc(2 * var(--dsem));
            }

            #div-selection:hover::after,
            #div-selection.selectionne::after {
                display: block;
                position: absolute;
                left: 0px;
                bottom: 0px;
                content: '';
                height: var(--hauteur-selection);
                width: 100%;
                padding: 0;
                border-radius: var(--hauteur-selection);
                background-color: var(--couleur-neutre-20);
            }
            #div-selection.selectionne::after,
            #div-selection.selectionne:hover::after {
                background-color: var(--couleur-selection);
            }

            a {
                display: flex;
                flex-direction: column;
                align-items: start;
                justify-content: center;
                padding: calc(2 * var(--dsem)) 0;
                text-decoration: none;
            }

            .theme-simple a {
                padding: 0;
            }

            a:hover,
            a:active {
                cursor: pointer;
                text-decoration: none;
                text-decoration-color: var(--couleur-selection);
                color: var(--couleur-selection);
            }

            h2,
            h3 {
                margin: 0;
                padding: 0;
                max-width: 100%;
                white-space: nowrap;
                overflow: hidden;
                text-overflow: ellipsis;
            }

            h2 {
                color: var(--couleur-neutre-80);
            }
            h3 {
                color: var(--couleur-neutre-60);
            }

            .selectionne h2,
            .selectionne h3 {
                color: var(--couleur-selection);
            }
        `
    ];

    @property()
    libelle = '';

    @property()
    sousLibelle?: string;

    @property()
    href?: string;

    @property()
    theme: string = BarreOngletsItem.THEME.classique;

    @property({ type: Boolean })
    selectionne = false;

    render() {
        return html`
            <div id="div-conteneur" class="theme-${this.theme}">
                <div id="div-selection" class="${this.selectionne ? `selectionne` : ``}">
                    <a href="${ifDefined(this.href)}">
                        <h2 class="texte-alternatif">${unsafeHTML(this.libelle)}</h2>
                        <h3 class="texte-petit-majuscule">${unsafeHTML(this.sousLibelle)}</h3>
                    </a>
                </div>
            </div>
        `;
    }
}

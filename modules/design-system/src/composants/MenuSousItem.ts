import { css, html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { ifDefined } from 'lit/directives/if-defined.js';

import { STYLES_DESIGN_SYSTEM } from '../styles/styles-design-system.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-menu-sous-item': MenuSousItem;
    }
}

@customElement('c-menu-sous-item')
export class MenuSousItem extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        css`
            a {
                display: grid;
                cursor: pointer;
                text-decoration: none;
                color: var(--couleur-primaire);
                padding: calc(var(--dsem));
                padding-left: calc(4 * var(--dsem));
                border-left: 5px solid transparent;
            }

            a:hover {
                text-decoration: underline;
            }

            .selectionne {
                background-color: var(--couleur-neutre-clair);
                border-color: var(--couleur-primaire);
            }
        `
    ];

    @property()
    libelle?: string;

    @property()
    href?: string;

    @property({ type: Boolean })
    selectionne = false;

    render() {
        return html` <a class="texte-petit ${this.selectionne ? `selectionne` : ``}" href="${ifDefined(this.href)}">${this.libelle}</a> `;
    }
}

import { css, html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { unsafeHTML } from 'lit/directives/unsafe-html.js';

import { STYLES_DESIGN_SYSTEM } from '../styles/styles-design-system.js';

export interface OptionsTableau {
    entetes: string[];
    contenuHtml: string[][];
    ligneFin?: string[];
}

declare global {
    interface HTMLElementTagNameMap {
        'c-tableau': Tableau;
    }
}

@customElement('c-tableau')
export class Tableau extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        css`
            :host {
                overflow-x: auto;
                display: block;
            }

            table {
                border-collapse: collapse;
                margin-left: auto;
                margin-right: auto;
            }

            td,
            th {
                padding: 0.5rem;
                min-width: 100px;
                text-align: right;
            }

            tbody td:first-child {
                text-align: left;
            }
            tbody tr:nth-child(even) {
                background-color: var(--couleur-neutre-20);
            }

            th {
                padding: 0.5rem;
                text-align: center;
                border-bottom: 3px solid var(--couleur-accent);
            }

            tfoot {
                border-top: 3px solid var(--couleur-neutre-40);
            }
        `
    ];

    @property({ attribute: false })
    options?: OptionsTableau;

    render() {
        return html`
            <table>
                <thead>
                    <tr class="texte-moyen">
                        ${this.options?.entetes.map((e) => html`<th>${unsafeHTML(e)}</th>`)}
                    </tr>
                </thead>
                <tbody>
                    ${this.options?.contenuHtml.map(
                        (ligne) =>
                            html`<tr class="texte-moyen">
                                ${ligne.map((contenuCellule) => html`<td>${unsafeHTML(contenuCellule)}</td>`)}
                            </tr>`
                    )}
                </tbody>
                <tfoot>
                    ${this.options?.ligneFin
                        ? html`<tr class="texte-moyen">
                              ${this.options?.ligneFin.map((e) => html`<td>${unsafeHTML(e)}</td>`)}
                          </tr>`
                        : ''}
                </tfoot>
            </table>
        `;
    }
}

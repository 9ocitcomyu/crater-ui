import { css, html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { ifDefined } from 'lit/directives/if-defined.js';
import { unsafeSVG } from 'lit/directives/unsafe-svg.js';

import { STYLES_DESIGN_SYSTEM } from '../styles/styles-design-system.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-menu-item': MenuItem;
    }
}

const chevron = `
<svg width="32" height="32" viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg">
    <g clip-path="url(#clip0_530_6353)">
    <path d="M13.3335 21.3332L18.6668 15.9998L13.3335 10.6665" stroke-linecap="round" stroke-linejoin="round"/>
    </g>
</svg>
`;

@customElement('c-menu-item')
export class MenuItem extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        css`
            a {
                cursor: pointer;
                display: flex;
                flex-wrap: nowrap;
                align-items: center;
                justify-content: space-between;
                padding: calc(1 * var(--dsem)) calc(1 * var(--dsem));
                text-decoration: none;
                min-height: 40px;
                border-radius: calc(2 * var(--dsem));
            }

            a:hover .libelle {
                text-decoration: underline;
            }

            a:hover .chevron {
                stroke-opacity: 1;
                stroke-width: 3;
            }

            .lien-inactif,
            .lien-inactif:hover {
                text-decoration: none;
                cursor: default;
                background-color: initial;
            }

            .contenu {
                display: flex;
                flex-wrap: nowrap;
                align-items: center;
                padding-right: calc(3 * var(--dsem));
            }

            .libelle {
                color: var(--couleur-primaire-sombre);
            }

            .sous-libelle {
                color: var(--couleur-neutre-40);
            }

            ::slotted(div) {
                padding-right: calc(1 * var(--dsem));
                display: grid;
                z-index: 2;
            }

            .chevron {
                display: grid;
                stroke: var(--couleur-neutre);
                stroke-opacity: 0.6;
                stroke-width: 2;
                fill: none;
            }

            .selectionne {
                background-color: var(--couleur-neutre-clair);
            }

            .tourne {
                transform: rotate(90deg);
            }
        `
    ];

    @property()
    href?: string;

    @property()
    libelle?: string;

    @property()
    sousLibelle?: string;

    @property({ type: Boolean })
    selectionne = false;

    @property({ type: Boolean })
    chevronVersBas = false;

    render() {
        return html`
            <a class="${this.selectionne ? `selectionne` : ``} ${this.href ? `` : `lien-inactif`}" href="${ifDefined(this.href)}">
                <div class="contenu">
                    <slot name="icone"></slot>
                    <div>
                        <div class="libelle titre-petit">${this.libelle}</div>
                        <div class="sous-libelle texte-petit-majuscule">${this.sousLibelle}</div>
                    </div>
                </div>
                <div class="chevron ${this.chevronVersBas ? `tourne` : ``}">${unsafeSVG(chevron)}</div>
            </a>
        `;
    }
}

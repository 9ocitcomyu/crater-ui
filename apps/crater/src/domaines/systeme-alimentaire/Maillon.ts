import { LienIndicateur } from '../../pages/diagnostic/chapitres/composants/VoirEgalement';
import { OutilExterne } from './OutilExterne';
import { VoieResilience } from './VoieResilience';

export interface Citation {
    texte: string;
    source: string;
}

export class Maillon {
    constructor(
        public readonly id: string,
        public readonly nom: string,
        public readonly icone: string,
        public readonly description: string,
        public readonly citations: Citation[],
        public readonly voiesResilience: VoieResilience[],
        public readonly outilsExternes: OutilExterne[],
        public readonly indicateursAutresMaillons: LienIndicateur[],
        public readonly lienVersCarteDesactive = false,
        public readonly informationComplementaire?: string
    ) {}
}

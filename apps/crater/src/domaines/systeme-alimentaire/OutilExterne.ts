export class OutilExterne {
    constructor(
        public readonly id: string,
        public readonly titre: string,
        public readonly description: string,
        public question: string,
        public lien: string
    ) {}
}

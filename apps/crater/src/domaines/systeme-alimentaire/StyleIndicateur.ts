import { COULEUR_VALEUR_NULL, GradientCouleurs } from './GradientCouleurs';

interface SeuilLegende {
    borneInf: number;
    libelleBorneInf: string;
    couleur: string;
}

export type TypeJauge = 'NOTE' | 'VALEUR';

interface ValeurLabel {
    valeur: number;
    label: string;
}

export interface AttributsStyleIndicateur {
    readonly nbDecimales: number;
    readonly gradientCouleurs: GradientCouleurs;
    readonly couleurTexte: string;
    readonly labels: ValeurLabel[];
    readonly typeJauge: TypeJauge;
}

export class StyleIndicateur {
    public nbDecimales: number;
    public couleurTexte: string;
    private gradientCouleurs: GradientCouleurs;
    private labels: ValeurLabel[];
    public typeJauge: TypeJauge;

    constructor(attributs: AttributsStyleIndicateur) {
        this.nbDecimales = attributs.nbDecimales;
        this.gradientCouleurs = attributs.gradientCouleurs;
        this.couleurTexte = attributs.couleurTexte;
        this.labels = attributs.labels;
        this.typeJauge = attributs.typeJauge;
    }

    public calculerCouleur(valeur: number | null) {
        return this.gradientCouleurs.calculerCouleur(valeur);
    }

    get seuilsLegende(): SeuilLegende[] {
        return this.labels.map((l, indice) => {
            let couleurIntervalle = COULEUR_VALEUR_NULL;
            if (indice === 0) {
                couleurIntervalle = this.gradientCouleurs.couleurBasse;
            } else if (indice >= this.labels.length - 1) {
                couleurIntervalle = this.gradientCouleurs.couleurHaute;
            } else {
                couleurIntervalle = this.calculerCouleur(this.labels[indice].valeur);
            }
            return { borneInf: l.valeur, libelleBorneInf: l.label, couleur: couleurIntervalle };
        });
    }
}

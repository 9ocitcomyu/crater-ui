import { describe, expect, it } from 'vitest';

import { COULEUR_VALEUR_NULL, GradientCouleurs } from './GradientCouleurs';

describe('Test de la classe GradientCouleurs', () => {
    const couleurNote10 = '#219653';
    const couleurNote5 = '#f2994a';
    const couleurNote0 = '#9b0000';
    const seuilsCouleursNotes = [
        { valeur: 0, couleur: couleurNote0 },
        { valeur: 5, couleur: couleurNote5 },
        { valeur: 10, couleur: couleurNote10 }
    ];
    const couleurNote2 = '#be3e1e';
    const couleurNote5_5 = '#de994b';

    const gradientCouleurs = new GradientCouleurs(seuilsCouleursNotes);

    it('Calculer couleur pour une valeur entre bornes inf et sup', () => {
        expect(gradientCouleurs.calculerCouleur(0)).toEqual(couleurNote0);
        expect(gradientCouleurs.calculerCouleur(5)).toEqual(couleurNote5);
        expect(gradientCouleurs.calculerCouleur(5.5)).toEqual(couleurNote5_5);
        expect(gradientCouleurs.calculerCouleur(10)).toEqual(couleurNote10);
        expect(gradientCouleurs.calculerCouleur(2)).toEqual(couleurNote2);
    });

    it('Calculer couleur pour cas null ou hors bornes', () => {
        expect(gradientCouleurs.calculerCouleur(null)).toEqual(COULEUR_VALEUR_NULL);
        expect(gradientCouleurs.calculerCouleur(-1)).toEqual(couleurNote0);
        expect(gradientCouleurs.calculerCouleur(11)).toEqual(couleurNote10);
    });

    it('Calculer couleur basse et haute', () => {
        expect(gradientCouleurs.couleurBasse).toEqual(couleurNote0);
        expect(gradientCouleurs.couleurHaute).toEqual(couleurNote10);
    });
});

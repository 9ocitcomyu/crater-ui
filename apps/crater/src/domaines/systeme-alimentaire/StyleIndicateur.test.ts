import { describe, expect, it } from 'vitest';

import { GradientCouleurs } from './GradientCouleurs';
import { StyleIndicateur } from './StyleIndicateur';

const couleurNote0 = '#9b0000';
const couleurNote2 = '#be3e1e';
const couleurNote5 = '#f2994a';
const couleurNote5_5 = '#de994b';
const couleurNote10 = '#219653';
const seuilsCouleurs = [
    { valeur: 0, couleur: couleurNote0 },
    { valeur: 5, couleur: couleurNote5 },
    { valeur: 10, couleur: couleurNote10 }
];

describe('Test de la classe StyleIndicateur', () => {
    const style = new StyleIndicateur({
        nbDecimales: 0,
        gradientCouleurs: new GradientCouleurs(seuilsCouleurs),
        couleurTexte: 'red',
        labels: [
            { valeur: 0, label: '0' },
            { valeur: 5.5, label: '5.5' },
            { valeur: 10, label: '10' }
        ],
        typeJauge: 'VALEUR'
    });

    it('Tester du calcul de la couleur ', () => {
        expect(style.calculerCouleur(2)).toEqual(couleurNote2);
    });

    it('Calculer les plages de valeur de la legende ', () => {
        expect(style.seuilsLegende).toEqual([
            {
                borneInf: 0,
                couleur: couleurNote0,
                libelleBorneInf: '0'
            },
            {
                borneInf: 5.5,
                couleur: couleurNote5_5,
                libelleBorneInf: '5.5'
            },
            {
                borneInf: 10,
                couleur: couleurNote10,
                libelleBorneInf: '10'
            }
        ]);
    });
});

import { SystemeAlimentaire } from '../SystemeAlimentaire';
import { creerIndicateurs } from './indicateurs';
import { creerMaillons } from './maillons';
import { creerOutilsExternes } from './outils_externes';
import { creerVoiesResilience } from './voies_resilience';

export const SA = creerSystemeAlimentaire();

export function creerSystemeAlimentaire(): SystemeAlimentaire {
    const systemeAlimentaire = new SystemeAlimentaire();
    systemeAlimentaire.voiesResilience = creerVoiesResilience();
    systemeAlimentaire.outilsExternes = creerOutilsExternes();
    systemeAlimentaire.maillons = creerMaillons(systemeAlimentaire);
    systemeAlimentaire.indicateurs = creerIndicateurs(systemeAlimentaire);

    return systemeAlimentaire;
}

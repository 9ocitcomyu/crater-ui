import { IDS_MAILLONS } from '../../systeme-alimentaire/configuration/maillons';
import { DonneesPageCarte, DonneesPageDiagnostic, DonneesPageDiagnosticIndicateur, DonneesPageDiagnosticMaillon } from '../donnees-pages';
import { ModelePage } from '../ModelePage';
import { ModelePageStatique } from '../ModelePageStatique';

export const PAGES_PRINCIPALES = {
    accueil: new ModelePageStatique({
        url: '/',
        titreCourt: 'Accueil',
        metaDescription:
            'CRATer, un outil de diagnostic au service de la transition agro-alimentaire. Évaluez en un clic la résilience et la durabilité du système alimentaire de votre territoire'
    }),
    diagnostic: new ModelePageStatique({
        url: '/diagnostic',
        titreCourt: 'Diagnostic'
    }),
    carte: new ModelePageStatique({
        url: '/carte',
        titreCourt: 'Carte',
        metaDescription:
            "Visualisez les indicateurs sous forme de cartes : utilisation de pesticides, part de surface agricole en bio, rythme d'artificialisation des sols et bien d'autres"
    }),
    aide: new ModelePageStatique({
        url: '/aide',
        titreCourt: 'Aide',
        metaDescription: 'Comprendre les enjeux de la transition agro-alimentaire et explorer la méthodologie de calcul des indicateurs de CRATer'
    }),
    projet: new ModelePageStatique({
        url: '/projet',
        titreCourt: 'Projet',
        metaDescription: "Une application libre et gratuite developpée par l'association Les Greniers d'Abondance"
    }),
    api: new ModelePageStatique({ url: '/api-doc', titreCourt: 'Api' }),
    contact: new ModelePageStatique({ url: '/contact', titreCourt: 'Contact' }),
    demandeAjoutTerritoire: new ModelePageStatique({ url: '/demande-ajout-territoire', titreCourt: "Demande d'ajout de territoire" }),
    erreur: new ModelePageStatique({ url: '/erreur', titreCourt: 'Erreur' })
};

const urlCanoniquePagesDiagnostic = (d: DonneesPageDiagnostic) => `${PAGES_PRINCIPALES.diagnostic.getUrl()}/${d.idTerritoireActif}`;
export const PAGES_DIAGNOSTIC = {
    synthese: new ModelePage<DonneesPageDiagnostic>({
        url: (d) => `${PAGES_PRINCIPALES.diagnostic.getUrl()}/${d.idTerritoirePrincipal}?echelleterritoriale=${d.idEchelleTerritoriale}`,
        titreCourt: 'Synthèse du diagnostic',
        urlCanonique: urlCanoniquePagesDiagnostic,
        titreLong: (d) => `Diagnostic du système alimentaire pour le territoire ${d.nomTerritoireActif}`,
        metaDescription: (d) => `${d.metaDescription}`
    }),
    territoire: new ModelePage<DonneesPageDiagnostic>({
        url: (d) => `${PAGES_PRINCIPALES.diagnostic.getUrl()}/${d.idTerritoirePrincipal}/territoire?echelleterritoriale=${d.idEchelleTerritoriale}`,
        titreCourt: `Présentation du territoire`,
        urlCanonique: urlCanoniquePagesDiagnostic,
        titreLong: (d) => `Présentation du territoire ${d.nomTerritoireActif}`
    }),
    maillons: new ModelePage<DonneesPageDiagnosticMaillon>({
        url: (d) =>
            `${PAGES_PRINCIPALES.diagnostic.getUrl()}/${d.idTerritoirePrincipal}/maillons/${d.idMaillon}?echelleterritoriale=${
                d.idEchelleTerritoriale
            }`,
        titreCourt: `Diagnostic du maillon`,
        urlCanonique: urlCanoniquePagesDiagnostic,
        titreLong: (d) => `Diagnostic du maillon ${d.nomMaillon} pour le territoire ${d.nomTerritoireActif}`
    }),
    indicateurs: new ModelePage<DonneesPageDiagnosticIndicateur>({
        url: (d) =>
            `${PAGES_PRINCIPALES.diagnostic.getUrl()}/${d.idTerritoirePrincipal}/indicateurs/${d.idIndicateur}?echelleterritoriale=${
                d.idEchelleTerritoriale
            }`,
        titreCourt: `Indicateurs`,
        urlCanonique: urlCanoniquePagesDiagnostic,
        titreLong: (d) => `Indicateur ${d.nomIndicateur} pour le territoire ${d.nomTerritoireActif}`
    }),
    pdf: new ModelePage<DonneesPageDiagnostic>({
        url: (d) => `${PAGES_PRINCIPALES.diagnostic.getUrl()}/${d.idTerritoirePrincipal}/pdf`,
        titreCourt: `PDF`,
        urlCanonique: urlCanoniquePagesDiagnostic,
        titreLong: (d) => `Diagnostic du territoire ${d.nomTerritoirePrincipal} (Version imprimable)`
    })
};

export const PAGES_CARTE = {
    indicateurs: new ModelePage<DonneesPageCarte>({
        url: (d) => `${PAGES_PRINCIPALES.carte.getUrl()}/${d.idIndicateur}/${d.idEchelleTerritoriale}?territoire=${d.idTerritoirePrincipal}`,
        titreCourt: 'Carte',
        urlCanonique: (d) => `${PAGES_PRINCIPALES.carte.getUrl()}/${d.idIndicateur}/${d.idEchelleTerritoriale}`,
        titreLong: (d) => `${d.nomIndicateur} - Carte des ${d.libelleEchelleTerritoriale}`,
        metaDescription: (d) => `Carte des ${d.libelleEchelleTerritoriale} pour l'indicateur ${d.nomIndicateur}`
    })
};

export const PAGES_AIDE = {
    comprendreLeSystemeAlimentaire: new ModelePageStatique({
        url: `${PAGES_PRINCIPALES.aide.getUrl()}/comprendre-le-systeme-alimentaire`,
        titreCourt: 'Comprendre le système alimentaire',
        metaDescription: 'Défaillances et vulnérabilités du système alimentaire actuel, et enjeux de la transition agro-alimentaire à mener'
    }),
    methodologie: new ModelePageStatique({
        url: `${PAGES_PRINCIPALES.aide.getUrl()}/methodologie`,
        titreCourt: 'Méthodologie',
        metaDescription: "Méthodologie de calcul de l'ensemble des indicateurs de CRATer et sources de données utilisées"
    }),
    faq: new ModelePageStatique({ url: `${PAGES_PRINCIPALES.aide.getUrl()}/faq`, titreCourt: 'Foire aux questions' }),
    glossaire: new ModelePageStatique({ url: `${PAGES_PRINCIPALES.aide.getUrl()}/glossaire`, titreCourt: 'Glossaire' })
};

export const PAGES_COMPRENDRE_LE_SA = {
    systemeActuel: new ModelePageStatique({
        url: `${PAGES_AIDE.comprendreLeSystemeAlimentaire.getUrl()}/le-systeme-actuel`,
        titreCourt: 'Un système fait pour produire, pas pour nourrir',
        urlCanonique: PAGES_AIDE.comprendreLeSystemeAlimentaire.getUrlCanonique()
    }),
    defaillancesVulnerabilites: new ModelePageStatique({
        url: `${PAGES_AIDE.comprendreLeSystemeAlimentaire.getUrl()}/defaillances-et-vulnerabilites`,
        titreCourt: 'Nuages à l’horizon'
    }),
    transitionAgricoleAlimentaire: new ModelePageStatique({
        url: `${PAGES_AIDE.comprendreLeSystemeAlimentaire.getUrl()}/la-transition-agricole-et-alimentaire`,
        titreCourt: 'Changement de cap !'
    })
};

export const PAGES_METHODOLOGIE = {
    presentationGenerale: new ModelePageStatique({
        url: `${PAGES_AIDE.methodologie.getUrl()}/presentation-generale`,
        titreCourt: 'Présentation générale',
        urlCanonique: PAGES_AIDE.methodologie.getUrlCanonique()
    }),
    terresAgricoles: new ModelePageStatique({
        url: `${PAGES_AIDE.methodologie.getUrl()}/${IDS_MAILLONS.terresAgricoles}`,
        titreCourt: 'Terres agricoles'
    }),

    agriculteursExploitations: new ModelePageStatique({
        url: `${PAGES_AIDE.methodologie.getUrl()}/${IDS_MAILLONS.agriculteursExploitations}`,
        titreCourt: 'Agriculteurs & Exploitations'
    }),
    intrants: new ModelePageStatique({ url: `${PAGES_AIDE.methodologie.getUrl()}/${IDS_MAILLONS.intrants}`, titreCourt: 'Intrants' }),
    production: new ModelePageStatique({ url: `${PAGES_AIDE.methodologie.getUrl()}/${IDS_MAILLONS.production}`, titreCourt: 'Production' }),
    transformationDistribution: new ModelePageStatique({
        url: `${PAGES_AIDE.methodologie.getUrl()}/${IDS_MAILLONS.transformationDistribution}`,
        titreCourt: 'Transformation & Distribution'
    }),
    consommation: new ModelePageStatique({ url: `${PAGES_AIDE.methodologie.getUrl()}/${IDS_MAILLONS.consommation}`, titreCourt: 'Consommation' }),
    sourcesDonnees: new ModelePageStatique({ url: `${PAGES_AIDE.methodologie.getUrl()}/sources-donnees`, titreCourt: 'Sources de données' }),
    licenceUtilisation: new ModelePageStatique({
        url: `${PAGES_AIDE.methodologie.getUrl()}/licence-utilisation`,
        titreCourt: "Licence d'utilisation"
    })
};

export const LIENS_EXTERNES = {
    lienFaireUnDon: 'https://www.helloasso.com/associations/les-greniers-d-abondance/formulaires/2'
};

import { IDS_MAILLONS } from '../systeme-alimentaire/configuration/maillons';
import { PAGES_AIDE, PAGES_CARTE, PAGES_DIAGNOSTIC, PAGES_METHODOLOGIE } from './configuration/declaration-pages';

export function construireUrlPageDiagnosticSynthese(idTerritoire: string, idEchelleTerritoriale?: string) {
    return PAGES_DIAGNOSTIC.synthese.getUrl({ idTerritoirePrincipal: idTerritoire, idEchelleTerritoriale: idEchelleTerritoriale });
}

export function construireUrlPageDiagnosticTerritoire(idTerritoire: string, idEchelleTerritoriale?: string) {
    return PAGES_DIAGNOSTIC.territoire.getUrl({ idTerritoirePrincipal: idTerritoire, idEchelleTerritoriale: idEchelleTerritoriale });
}

export function construireUrlPageDiagnosticMaillon(
    idMaillon: string,
    idTerritoire: string,
    idEchelleTerritoriale?: string,
    idElementCibleScroll?: string
) {
    return PAGES_DIAGNOSTIC.maillons.getUrl({
        idMaillon: idMaillon,
        idTerritoirePrincipal: idTerritoire,
        idEchelleTerritoriale: idEchelleTerritoriale,
        idElementCibleScroll: idElementCibleScroll
    });
}
export function construireUrlPageDiagnosticIndicateur(idIndicateur: string, idTerritoire: string, idEchelleTerritoriale?: string) {
    return PAGES_DIAGNOSTIC.indicateurs.getUrl({
        idIndicateur: idIndicateur,
        idTerritoirePrincipal: idTerritoire,
        idEchelleTerritoriale: idEchelleTerritoriale
    });
}

export function construireUrlPageMethodologie(idMaillon: string | undefined, idElementCibleScroll?: string): string {
    let pageMethodologie = PAGES_AIDE.methodologie;
    switch (idMaillon) {
        case IDS_MAILLONS.terresAgricoles:
            pageMethodologie = PAGES_METHODOLOGIE.terresAgricoles;
            break;
        case IDS_MAILLONS.agriculteursExploitations:
            pageMethodologie = PAGES_METHODOLOGIE.agriculteursExploitations;
            break;
        case IDS_MAILLONS.intrants:
            pageMethodologie = PAGES_METHODOLOGIE.intrants;
            break;
        case IDS_MAILLONS.production:
            pageMethodologie = PAGES_METHODOLOGIE.production;
            break;
        case IDS_MAILLONS.transformationDistribution:
            pageMethodologie = PAGES_METHODOLOGIE.transformationDistribution;
            break;
        case IDS_MAILLONS.consommation:
            pageMethodologie = PAGES_METHODOLOGIE.consommation;
            break;
        default:
    }
    return pageMethodologie.getUrl({ idElementCibleScroll: idElementCibleScroll });
}

export function construireUrlPageCarte(idIndicateur: string, idEchelleTerritoriale?: string): string {
    return PAGES_CARTE.indicateurs.getUrl({
        idIndicateur: idIndicateur,
        idEchelleTerritoriale: idEchelleTerritoriale,
        idTerritoirePrincipal: undefined
    });
}
export function construireUrlPageCarteTerritoire(idIndicateur: string, idTerritoire: string): string {
    return PAGES_CARTE.indicateurs.getUrl({
        idIndicateur: idIndicateur,
        idEchelleTerritoriale: undefined,
        idTerritoirePrincipal: idTerritoire
    });
}

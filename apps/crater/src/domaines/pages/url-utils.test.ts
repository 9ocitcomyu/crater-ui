import { describe, expect, it } from 'vitest';

import { nettoyerParamSansValeur } from './url-utils';

describe('Test de url utils', () => {
    it('Nettoyer URL', () => {
        expect(nettoyerParamSansValeur('/carte/:param1?param2=valeur2&param3=')).toEqual('/carte/:param1?param2=valeur2');
        expect(nettoyerParamSansValeur('/carte/:param1?param2=&param3=valeur3')).toEqual('/carte/:param1?param3=valeur3');
        expect(nettoyerParamSansValeur('/carte/:param1?param2=&param3=')).toEqual('/carte/:param1');
        expect(nettoyerParamSansValeur('/carte/:param1?param2=undefined&param3=')).toEqual('/carte/:param1');
        expect(nettoyerParamSansValeur('/carte/:param1?param2=undefined&param3=valeur')).toEqual('/carte/:param1?param3=valeur');
        expect(nettoyerParamSansValeur('/carte/undefined')).toEqual('/carte');
        expect(nettoyerParamSansValeur('/carte/undefined?param1=valeur')).toEqual('/carte?param1=valeur');
        expect(nettoyerParamSansValeur('/carte/undefined/indicateur?param1=valeur')).toEqual('/carte/indicateur?param1=valeur');
    });
});

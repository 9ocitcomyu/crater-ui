const REGEX_QUERY_PARAM_SANS_VALEUR = /[^?&]+=(?=&|$)/g;
const REGEX_QUERY_PARAM_VALEUR_UNDEFINED = /[^?&]+=undefined(?=&|$)/g;
const REGEX_PATH_PARAM_SANS_VALEUR = /\/\/|\/\?/g;
const REGEX_PATH_PARAM_VALEUR_UNDEFINED = /\/undefined(?=\/|\?|$)/g;

export function nettoyerParamSansValeur(url: string): string {
    return url
        .replace(REGEX_PATH_PARAM_SANS_VALEUR, '')
        .replace(REGEX_PATH_PARAM_VALEUR_UNDEFINED, '')
        .replace(REGEX_QUERY_PARAM_SANS_VALEUR, '')
        .replace(REGEX_QUERY_PARAM_VALEUR_UNDEFINED, '')
        .replace(/\?&/g, '?')
        .replace(/\?$/g, '')
        .replace(/&$/g, '');
}

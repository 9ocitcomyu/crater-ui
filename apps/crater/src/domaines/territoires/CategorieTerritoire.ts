const CODE_REGROUPEMENT_COMMUNES = 'REGROUPEMENT_COMMUNES';

export class CategorieTerritoire {
    static readonly Commune = new CategorieTerritoire('COMMUNE', null, 'Commune', 'Commune', 1);
    static readonly Epci = new CategorieTerritoire('EPCI', null, 'Intercommunalité', 'Intercommunalité', 2);
    static readonly RegroupementCommunes = {
        AutreTerritoire: new CategorieTerritoire(CODE_REGROUPEMENT_COMMUNES, 'AUTRE_TERRITOIRE', 'Territoire', 'Territoire', 3),
        BassinDeVie: new CategorieTerritoire(CODE_REGROUPEMENT_COMMUNES, 'BASSIN_DE_VIE_2012', 'Bassin de vie', 'Bassin de vie', 4),
        PaysPetr: new CategorieTerritoire(CODE_REGROUPEMENT_COMMUNES, 'PAYS_PETR', 'Pays / PETR', 'Pays / PETR', 5),
        Pn: new CategorieTerritoire(CODE_REGROUPEMENT_COMMUNES, 'PARC_NATIONAL', 'Parc National', '<abbr title="Parc National">PN</abbr>', 6),
        Pnr: new CategorieTerritoire(
            CODE_REGROUPEMENT_COMMUNES,
            'PARC_NATUREL_REGIONAL',
            'Parc Naturel Régional',
            '<abbr title="Parc Naturel Régional">PNR</abbr>',
            7
        ),
        Scot: new CategorieTerritoire(
            CODE_REGROUPEMENT_COMMUNES,
            'SCHEMA_COHERENCE_TERRITORIAL',
            'Schéma de Cohérence Territoriale',
            '<abbr title="Schéma de Cohérence Territoriale">SCoT</abbr>',
            8
        ),
        Pat: new CategorieTerritoire(
            CODE_REGROUPEMENT_COMMUNES,
            'PROJET_ALIMENTAIRE_TERRITORIAL',
            'Projet alimentaire territorial',
            '<abbr title="Projet alimentaire territorial">PAT</abbr>',
            9
        ),
        Nd: new CategorieTerritoire(CODE_REGROUPEMENT_COMMUNES, 'NOUVEAU_DEPARTEMENT', 'Nouveau Département', 'Nouveau département', 10)
    };
    static readonly Departement = new CategorieTerritoire('DEPARTEMENT', null, 'Département', 'Département', 11);
    static readonly Region = new CategorieTerritoire('REGION', null, 'Région', 'Région', 12);
    static readonly Pays = new CategorieTerritoire('PAYS', null, 'Pays', 'Pays', 13);

    private constructor(
        public readonly codeCategorie: string,
        public readonly codeSousCategorie: string | null,
        public readonly libelleCategorie: string,
        public readonly libelleCategorieHtml: string,
        private ordre: number
    ) {}

    static get listerToutes(): CategorieTerritoire[] {
        const listeCategories = Object.values(CategorieTerritoire).filter((c) => c instanceof CategorieTerritoire);
        const listeRegroupementsCommunes = Object.values(this.RegroupementCommunes);
        return listeCategories.concat(listeRegroupementsCommunes).sort((c1, c2) => c1.comparer(c2));
    }

    static fromString(categorie: string, sousCategorie?: string | null): CategorieTerritoire {
        const categorieTerritoire = this.listerToutes.find((e) => this.verifierEgalite(e, categorie, sousCategorie));
        if (categorieTerritoire) return categorieTerritoire;

        throw new RangeError(
            `Valeur incorrecte : le code catégorie "${categorie}" et/ou le code sous catégorie "${sousCategorie}" ne correspondent à aucun des codes de l'énumération`
        );
    }

    private static creerId(codeCategorie: string, codeSousCategorie?: string | null) {
        if (codeSousCategorie && codeCategorie === CODE_REGROUPEMENT_COMMUNES) {
            return codeCategorie.toUpperCase() + '_' + codeSousCategorie.toUpperCase();
        } else {
            return codeCategorie.toUpperCase();
        }
    }

    private static verifierEgalite(e: CategorieTerritoire, categorie: string, sousCategorie?: string | null) {
        return this.creerId(e.codeCategorie, e.codeSousCategorie) === this.creerId(categorie, sousCategorie);
    }

    comparer(categorie: CategorieTerritoire) {
        return this.ordre - categorie.ordre;
    }
}

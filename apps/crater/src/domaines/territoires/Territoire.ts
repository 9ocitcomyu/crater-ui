import { CategorieTerritoire } from './CategorieTerritoire';

export class Territoire {
    constructor(public readonly id: string, public readonly nom: string, public readonly categorie: CategorieTerritoire) {}
}

import { EchelleTerritoriale } from '../carte/EchelleTerritoriale';
import { HierarchieTerritoires, Territoire } from '../territoires';
import { Diagnostic } from './diagnostics';

export enum EtatRapport {
    VIDE = 'Rapport vide',
    CHARGEMENT_EN_COURS = 'Chargement',
    PRET = 'Pret'
}

export class Rapport {
    private constructor(
        private etat: EtatRapport,
        private hierarchieTerritoires: HierarchieTerritoires | undefined,
        private idTerritoireActif: string | undefined,
        private diagnostics: Map<string, Diagnostic>
    ) {}

    public static creerVide(): Rapport {
        return new Rapport(EtatRapport.VIDE, undefined, undefined, new Map<string, Diagnostic>());
    }

    public static creerEnChargement(): Rapport {
        const rapport = Rapport.creerVide();
        rapport.etat = EtatRapport.CHARGEMENT_EN_COURS;
        return rapport;
    }
    private clone(): Rapport {
        return new Rapport(this.etat, this.hierarchieTerritoires, this.idTerritoireActif, this.diagnostics);
    }

    getEtat() {
        return this.etat;
    }

    getHierarchieTerritoires(): HierarchieTerritoires | undefined {
        return this.hierarchieTerritoires;
    }

    setHierarchieTerritoires(hierarchieTerritoires: HierarchieTerritoires): Rapport {
        const rapport = Rapport.creerEnChargement();
        rapport.hierarchieTerritoires = hierarchieTerritoires;
        rapport.idTerritoireActif = hierarchieTerritoires.territoirePrincipal.id;
        return rapport;
    }

    getTerritoireActif(): Territoire | undefined {
        return this.hierarchieTerritoires?.getListeTerritoires().find((t) => t.id === this.idTerritoireActif);
    }

    setTerritoireActif(idTerritoireActif: string): Rapport {
        const rapport = this.clone();
        rapport.idTerritoireActif = idTerritoireActif;
        return rapport;
    }

    setTerritoireActifDepuisEchelleTerritoriale(idEchelleTerritoriale: string | undefined): Rapport {
        if (idEchelleTerritoriale) {
            const idTerritoireActif = this.hierarchieTerritoires
                ?.getListeTerritoires()
                .find((t) => t.categorie.codeCategorie === idEchelleTerritoriale)?.id;
            if (idTerritoireActif) {
                return this.setTerritoireActif(idTerritoireActif);
            }
        }
        return this;
    }

    getDiagnosticActif(): Diagnostic | undefined {
        if (this.idTerritoireActif === undefined) return undefined;
        return this.diagnostics.get(this.idTerritoireActif);
    }

    getDiagnosticPays(): Diagnostic | undefined {
        if (this.hierarchieTerritoires?.pays) {
            return this.diagnostics.get(this.hierarchieTerritoires?.pays.id);
        } else return undefined;
    }

    ajouterDiagnostic(diagnostic: Diagnostic): Rapport {
        if (
            !this.hierarchieTerritoires
                ?.getListeTerritoires()
                .map((t) => t.id)
                .includes(diagnostic.territoire.id)
        ) {
            throw new Error(
                "Erreur lors de l'ajout d'un diagnostic pour le territoire " +
                    diagnostic.territoire.id +
                    ' dans le rapport. Ce territoire ne fait pas partie de la hierarchie de territoires.'
            );
        }
        const rapport = this.clone();
        rapport.diagnostics.set(diagnostic.territoire.id, diagnostic);
        if (rapport.tousDiagnosticsCharges()) {
            rapport.getListeDiagnostics().forEach((d) => d?.setDiagnosticPays(this.getDiagnosticPays()));
            rapport.etat = EtatRapport.PRET;
        }
        return rapport;
    }

    private tousDiagnosticsCharges() {
        return this.hierarchieTerritoires?.getListeTerritoires().every((t) => this.diagnostics.has(t.id));
    }

    getListeDiagnostics() {
        if (this.hierarchieTerritoires) {
            return this.hierarchieTerritoires
                .getListeTerritoires()
                .map((t) => this.diagnostics.get(t.id))
                .filter((d) => d !== undefined);
        } else return [];
    }

    getIdEchelleTerritoriale(): string | undefined {
        return this.hierarchieTerritoires?.territoirePrincipal.id === this.idTerritoireActif
            ? undefined
            : this.getTerritoireActif()?.categorie.codeCategorie;
    }

    echelleEstDisponibleDansCarte(): boolean {
        return EchelleTerritoriale.listerTousLesCodeCategories.includes(this.getTerritoireActif()!.categorie.codeCategorie);
    }
}

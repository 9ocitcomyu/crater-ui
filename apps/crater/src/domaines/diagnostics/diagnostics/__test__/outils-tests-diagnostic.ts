import { Territoire } from '../../../territoires';
import { AgriculteursExploitations } from '../agriculteurs-exploitations/AgriculteursExploitations';
import { ClassesAges } from '../agriculteurs-exploitations/ClassesAges';
import { ClassesSuperficies } from '../agriculteurs-exploitations/ClassesSuperficies';
import { Consommation } from '../consommation/Consommation';
import { Diagnostic } from '../Diagnostic';
import { IndicateursPesticides, IndicateursPesticidesAnneeN, IndicateursPesticidesParAnnees, Intrants } from '../intrants/Intrants';
import { Otex } from '../Otex';
import { IndicateurHvn, Production } from '../production/Production';
import { SurfaceAgricoleUtile } from '../SurfaceAgricoleUtile';
import { PolitiqueAmenagement } from '../terres-agricoles/PolitiqueAmenagement';
import { TerresAgricoles } from '../terres-agricoles/TerresAgricoles';
import { CategorieCommerce } from '../transformation-distribution/CategorieCommerce';
import { IndicateursProximiteCommerces, TransformationDistribution } from '../transformation-distribution/TransformationDistribution';

export function creerDiagnosticVide(territoire: Territoire) {
    const surfaceAgricoleUtile = new SurfaceAgricoleUtile(0, 0, 0, 0, []);
    return new Diagnostic(
        territoire,
        0,
        0,
        'A',
        1,
        new Otex(),
        surfaceAgricoleUtile,
        new TerresAgricoles(territoire, 0, 0, 0, 0, 0, 0, PolitiqueAmenagement.DONNEES_NON_DISPONIBLES, 0, 0),
        new AgriculteursExploitations(
            territoire,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            new ClassesAges(0, 0, 0, 0),
            new ClassesSuperficies(0, 0, 0, 0, 0),
            new ClassesSuperficies(0, 0, 0, 0, 0)
        ),
        new Intrants(territoire, 0, new IndicateursPesticides(new IndicateursPesticidesParAnnees([new IndicateursPesticidesAnneeN(0, 0, 0, 0, 0)]))),
        new Production(territoire, 0, 0, 0, 0, 0, 0, new IndicateurHvn(0, 0, 0, 0), 0, 0),
        new TransformationDistribution(territoire, 0, 0, 0, [new IndicateursProximiteCommerces(CategorieCommerce.TroisTypesCommerces, 0, 0, 0)]),
        new Consommation(territoire, 0, 0, null)
    );
}

import { describe, expect, it } from 'vitest';

import { calculerMessageDetaillePartAlimentationAnimaleDansConsommation, calculerMessageDetailleTauxPauvrete } from './messages-consommation';

describe('Tests des fonctions de calcul des messages de Consommation', () => {
    it(`Calculer le message détaillé pour l'indicateur Taux de pauvreté`, () => {
        expect(calculerMessageDetailleTauxPauvrete('NomTerritoire', null, null)).toEqual(
            `Données indisponibles pour le territoire <em>NomTerritoire</em>.`
        );
        expect(calculerMessageDetailleTauxPauvrete('NomTerritoire', 15.2, 10)).toEqual(
            `La pauvreté a un lien direct avec la précarité alimentaire. Lors de l’enquête sur la pauvreté en France menée en 2021 par l’Ipsos et le Secours Populaire, 27 % des sondés déclaraient se restreindre sur la quantité de ce qu’ils mangeaient pour des raisons financières, et 37 % sur la qualité des produits (<c-lien href="https://publications.resiliencealimentaire.org/partie1_un-systeme-defaillant/#_une_malnutrition_omnipr%C3%A9sente_au_nord_comme_au_sud">en savoir plus</c-lien>).<br><br>Sur le territoire <em>NomTerritoire</em>, le taux de pauvreté est de 15% soit 1,5 fois celui de la France métropolitaine.`
        );
    });

    it(`Calculer le message détaillé pour l'indicateur Empreinte Surface Alimentation`, () => {
        expect(calculerMessageDetaillePartAlimentationAnimaleDansConsommation('NomTerritoire', null, null, null)).toEqual(
            `Données indisponibles pour le territoire <em>NomTerritoire</em>.`
        );
        expect(calculerMessageDetaillePartAlimentationAnimaleDansConsommation('NomTerritoire', 200, 160, 80)).toEqual(
            `Il faut 200 ha pour produire les aliments consommés par les habitants du territoire <em>NomTerritoire</em>.<br>Sur ce total, 160 ha sont utilisés pour produire les aliments d'origine animale (soit 80 %).`
        );
    });
});

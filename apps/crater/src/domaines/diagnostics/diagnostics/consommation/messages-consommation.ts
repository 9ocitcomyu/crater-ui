import { formaterNombreSelonValeurString } from '@lga/commun/build/outils/base';

export function calculerMessageDetailleTauxPauvrete(nomTerritoire: string, tauxPauvrete: number | null, tauxPauvreteFrance?: number | null) {
    if (tauxPauvrete === null || tauxPauvreteFrance === null || tauxPauvreteFrance === undefined)
        return `Données indisponibles pour le territoire <em>${nomTerritoire}</em>.`;
    else
        return `La pauvreté a un lien direct avec la précarité alimentaire. Lors de l’enquête sur la pauvreté en France menée en 2021 par l’Ipsos et le Secours Populaire, 27 % des sondés déclaraient se restreindre sur la quantité de ce qu’ils mangeaient pour des raisons financières, et 37 % sur la qualité des produits (<c-lien href="https://publications.resiliencealimentaire.org/partie1_un-systeme-defaillant/#_une_malnutrition_omnipr%C3%A9sente_au_nord_comme_au_sud">en savoir plus</c-lien>).<br><br>Sur le territoire <em>${nomTerritoire}</em>, le taux de pauvreté est de ${formaterNombreSelonValeurString(
            tauxPauvrete
        )}% soit ${formaterNombreSelonValeurString(tauxPauvrete / tauxPauvreteFrance)} fois celui de la France métropolitaine.`;
}

export function calculerMessageDetaillePartAlimentationAnimaleDansConsommation(
    nomTerritoire: string,
    consommationHa: number | null,
    consommationAnimauxHa: number | null,
    pourcentageConsommationAnimauxSurConsommationTotale: number | null
) {
    if (consommationHa === null || consommationAnimauxHa === null || pourcentageConsommationAnimauxSurConsommationTotale === null)
        return `Données indisponibles pour le territoire <em>${nomTerritoire}</em>.`;
    else
        return `Il faut ${formaterNombreSelonValeurString(
            consommationHa
        )} ha pour produire les aliments consommés par les habitants du territoire <em>${nomTerritoire}</em>.<br>Sur ce total, ${formaterNombreSelonValeurString(
            consommationAnimauxHa
        )} ha sont utilisés pour produire les aliments d'origine animale (soit ${formaterNombreSelonValeurString(
            pourcentageConsommationAnimauxSurConsommationTotale
        )} %).`;
}

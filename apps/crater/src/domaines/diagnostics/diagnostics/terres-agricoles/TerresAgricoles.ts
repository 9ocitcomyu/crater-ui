import { arrondirANDecimales, calculerEvolutionParPasDeTemps } from '@lga/commun/build/outils/base';

import { Territoire } from '../../../territoires';
import { IndicateurSynthese, Note } from '../IndicateurSynthese';
import {
    calculerMessageDetaillePolitiqueAmenagement,
    calculerMessagePartLogementsVacants,
    calculerMessageSyntheseTerresAgricoles,
    MessageRythmeArtificialisation,
    MessageSauParHabitant
} from './messages-terres-agricoles';
import { PolitiqueAmenagement } from './PolitiqueAmenagement';

export const SEUIL_SAU_PAR_HAB_M2_REGIME_TRES_VEGETAL = 1700;
export const SEUIL_SAU_PAR_HAB_M2_REGIME_MOINS_CARNE = 2500;
export const SEUIL_SAU_PAR_HAB_M2_REGIME_ACTUEL = 4000;
export const SEUIL_ARTIFICIALISATION_HA_QUASI_NULLE_OU_NEGATIVE = 0.1; // Seuil en hectare à partir duquel on considère qu'il n'y a pas d'artificialisation

export class TerresAgricoles implements IndicateurSynthese {
    public terresAgricolesPays?: TerresAgricoles;

    constructor(
        private territoire: Territoire,
        public readonly note: Note,
        public readonly superficieTotaleHa: number | null,
        public readonly artificialisation5ansHa: number | null,
        public readonly evolutionMenagesEmplois5ans: number | null,
        public readonly sauParHabitantM2: number | null,
        public readonly rythmeArtificialisationSauPourcent: number | null,
        public readonly politiqueAmenagement: PolitiqueAmenagement,
        public readonly partLogementsVacants2013Pourcent: number | null,
        public readonly partLogementsVacants2018Pourcent: number | null
    ) {}

    setTerresAgricolesPays(terresAgricolesPays: TerresAgricoles) {
        this.terresAgricolesPays = terresAgricolesPays;
    }

    get messageSynthese() {
        return calculerMessageSyntheseTerresAgricoles(this.note, this.sauParHabitantM2, this.artificialisation5ansHa);
    }

    get messageSauParHabitant() {
        return MessageSauParHabitant.construireMessage(this.territoire.nom, this.sauParHabitantM2);
    }

    get messageRythmeArtificialisation() {
        return MessageRythmeArtificialisation.construireMessage(
            this.territoire.nom,
            this.rythmeArtificialisationSauPourcent,
            this.terresAgricolesPays!.rythmeArtificialisationSauPourcent!
        );
    }

    get chiffreRythmeArtificialisation() {
        const SUPERFICIE_TERRAIN_FOOTBALL_HECTARE = 7000 / 10000;
        let artificialisationAnnuelleEnNombreDeTerrainsDeFootball;
        this.artificialisation5ansHa === null
            ? (artificialisationAnnuelleEnNombreDeTerrainsDeFootball = null)
            : (artificialisationAnnuelleEnNombreDeTerrainsDeFootball = this.artificialisation5ansHa / 5 / SUPERFICIE_TERRAIN_FOOTBALL_HECTARE);
        return calculerEvolutionParPasDeTemps(artificialisationAnnuelleEnNombreDeTerrainsDeFootball);
    }

    get ratioArtificialisation5ansSurSuperficieTerritoirePourcent(): number | null {
        if (this.artificialisation5ansHa === null || this.superficieTotaleHa === null) {
            return null;
        }
        return arrondirANDecimales((this.artificialisation5ansHa / this.superficieTotaleHa) * 100, 1);
    }

    get messagePolitiqueAmenagement() {
        return calculerMessageDetaillePolitiqueAmenagement(
            this.territoire.nom,
            this.politiqueAmenagement,
            this.artificialisation5ansHa,
            this.evolutionMenagesEmplois5ans,
            this.ratioArtificialisation5ansSurSuperficieTerritoirePourcent
        );
    }

    get messagePartLogementsVacants() {
        return calculerMessagePartLogementsVacants(this.territoire.nom, this.partLogementsVacants2013Pourcent, this.partLogementsVacants2018Pourcent);
    }

    get evolutionPartLogementsVacants2013a2018Points() {
        if (this.partLogementsVacants2013Pourcent === null || this.partLogementsVacants2018Pourcent === null) return null;
        return this.partLogementsVacants2018Pourcent - this.partLogementsVacants2013Pourcent;
    }
}

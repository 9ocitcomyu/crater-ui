import { describe, expect, it } from 'vitest';

import { CategorieTerritoire } from '../../../territoires';
import { PolitiqueAmenagement } from './PolitiqueAmenagement';
import { TerresAgricoles } from './TerresAgricoles';

describe('Tests de la classe TerresAgricoles', () => {
    it('Les champs calculés retournent null quand les valeurs utilisées dans les calculs sont null', () => {
        const terresAgricoles = new TerresAgricoles(
            { id: '', nom: '', categorie: CategorieTerritoire.Commune },
            null,
            null,
            null,
            null,
            null,
            null,
            PolitiqueAmenagement.DONNEES_NON_DISPONIBLES,
            null,
            null
        );
        expect(terresAgricoles.ratioArtificialisation5ansSurSuperficieTerritoirePourcent).toEqual(null);
        expect(terresAgricoles.chiffreRythmeArtificialisation).toEqual({ nombre: null, periode: null });
        expect(terresAgricoles.evolutionPartLogementsVacants2013a2018Points).toEqual(null);
    });
    it('Les champs calculés retournent une valeur', () => {
        const terresAgricoles = new TerresAgricoles(
            { id: '', nom: '', categorie: CategorieTerritoire.Commune },
            null,
            100,
            7,
            null,
            null,
            null,
            PolitiqueAmenagement.DONNEES_NON_DISPONIBLES,
            5,
            8
        );
        expect(terresAgricoles.ratioArtificialisation5ansSurSuperficieTerritoirePourcent).toEqual(7);
        expect(terresAgricoles.chiffreRythmeArtificialisation).toEqual({ nombre: 10, periode: 'tous les cinq ans' });
        expect(terresAgricoles.evolutionPartLogementsVacants2013a2018Points).toEqual(3);
    });
});

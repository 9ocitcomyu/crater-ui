export const NOTE_VALEUR_WARNING = '!';
export type Note = number | null | typeof NOTE_VALEUR_WARNING;

export interface IndicateurSynthese {
    note: Note;
    messageSynthese: string;
}

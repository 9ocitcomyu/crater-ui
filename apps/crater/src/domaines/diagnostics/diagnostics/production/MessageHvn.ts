import '@lga/design-system/build/composants/Lien.js';
export const SEUIL_LABELLISATION_HVN = 14.78;

export class MessageHvn {
    static readonly messageHVNGlobalSuperieurSeuil =
        "présente une <strong>Haute Valeur Naturelle</strong> au regard de l'expertise agroécologique et naturaliste des exploitations " +
        "menée par <c-lien href='https://solagro.org/nos-domaines-d-intervention/agroecologie/haute-valeur-naturelle' target='_blank'>Solagro</c-lien>. " +
        "Le territoire bénéficie d'exploitations agricoles mettant en œuvre une diversité d'assolement, des pratiques agricoles extensives " +
        'et présentant des infrastructures agroécologiques semi-naturelles témoignant de la qualité des services environnementaux.';
    static readonly messageHVNGlobalInferieurSeuil =
        'obtient une note <strong>insuffisante</strong> pour être reconnu <strong>Haute Valeur Naturelle</strong> ' +
        "au regard de l'expertise agroécologique et naturaliste des exploitations menée par " +
        "<c-lien href='https://solagro.org/nos-domaines-d-intervention/agroecologie/haute-valeur-naturelle' target='_blank'>Solagro</c-lien>. " +
        "La réduction de l'intensité des cheptels, la réduction des intrants chimiques et une meilleure gestion des infrastructures agroécologiques " +
        "(haies, lisières, prairies humides...) sont les pistes d'amélioration à explorer.";

    static construireMessage(nomTerritoire: string, scoreHVNGlobal: number | null): string {
        if (scoreHVNGlobal === null) {
            return `Le territoire <em>${nomTerritoire}</em> n'a pas de score HVN disponible.`;
        } else if (scoreHVNGlobal >= SEUIL_LABELLISATION_HVN) {
            return `Le territoire <em>${nomTerritoire}</em> ` + MessageHvn.messageHVNGlobalSuperieurSeuil;
        } else {
            return `Le territoire <em>${nomTerritoire}</em> ` + MessageHvn.messageHVNGlobalInferieurSeuil;
        }
    }
}

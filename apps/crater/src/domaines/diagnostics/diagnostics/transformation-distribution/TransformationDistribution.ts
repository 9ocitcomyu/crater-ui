import { Territoire } from '../../../territoires';
import { IndicateurSynthese, Note } from '../IndicateurSynthese';
import { CategorieCommerce } from './CategorieCommerce';
import {
    calculerMessagePartPopulationDependanteVoiture,
    calculerMessagePartTerritoireDependantVoiture,
    calculerMessageSynthese
} from './messages-transformation-distribution';

export class TransformationDistribution implements IndicateurSynthese {
    private transformationDistributionPays?: TransformationDistribution;
    private indicateursProximiteCommerces: IndicateursProximiteCommerces[];

    constructor(
        private territoire: Territoire,
        public readonly note: Note,
        public readonly partPopulationDependanteVoiturePourcent: number | null,
        public readonly partTerritoireDependantVoiturePourcent: number | null,
        indicateursProximiteCommerces: IndicateursProximiteCommerces[]
    ) {
        this.indicateursProximiteCommerces = indicateursProximiteCommerces;
    }

    setTransformationDistributionPays(transformationDistributionPays: TransformationDistribution) {
        this.transformationDistributionPays = transformationDistributionPays;
    }

    get messageSynthese() {
        return calculerMessageSynthese(this.note, this.partPopulationDependanteVoiturePourcent, this.partTerritoireDependantVoiturePourcent);
    }

    get messagePartPopulationDependanteVoiture() {
        return calculerMessagePartPopulationDependanteVoiture(
            this.territoire.nom,
            this.partPopulationDependanteVoiturePourcent,
            this.transformationDistributionPays!.partPopulationDependanteVoiturePourcent!
        );
    }

    get messagePartTerritoireDependantVoiture() {
        return calculerMessagePartTerritoireDependantVoiture(
            this.territoire.nom,
            this.territoire.categorie.codeCategorie,
            this.partTerritoireDependantVoiturePourcent,
            this.transformationDistributionPays!.partTerritoireDependantVoiturePourcent!
        );
    }

    get indicateursProximiteCommercesParTypesCommerces(): IndicateursProximiteCommerces[] {
        return this.indicateursProximiteCommerces
            .filter((i) => i.categorieCommerce.categorieAgregee === false)
            .sort((a, b) => a.categorieCommerce.comparer(b.categorieCommerce));
    }

    get indicateursProximiteCommerceGeneraliste(): IndicateursProximiteCommerces {
        return this.indicateursProximiteCommerces.find((i) => i.categorieCommerce === CategorieCommerce.CommerceGeneraliste)!;
    }
}

export class IndicateursProximiteCommerces {
    constructor(
        public readonly categorieCommerce: CategorieCommerce,
        public readonly distancePlusProcheCommerceMetres: number,
        public readonly partPopulationAccesAVeloPourcent: number,
        public readonly partPopulationDependanteVoiturePourcent: number
    ) {}
}

import { CategorieTerritoire } from '../../../territoires';
import { Note, NOTE_VALEUR_WARNING } from '../IndicateurSynthese';

export function calculerMessageSynthese(
    note: Note,
    partPopulationDependanteVoiturePourcent: number | null,
    partTerritoireDependantVoiturePourcent: number | null
): string {
    if (
        note === NOTE_VALEUR_WARNING ||
        note === null ||
        partPopulationDependanteVoiturePourcent === null ||
        partTerritoireDependantVoiturePourcent === null
    ) {
        return `Données non disponibles (voir les échelles supérieures telle que l'EPCI)`;
    }
    return `
    <strong>${partPopulationDependanteVoiturePourcent}&nbsp;% de la population</strong> 
    est théoriquement dépendante de la voiture pour ses achats alimentaires.`;
}

export function calculerMessagePartPopulationDependanteVoiture(
    nomTerrtoire: string,
    partPopulationDependanteVoiturePourcent: number | null,
    partPopulationDependanteVoitureFrance: number | null
): string {
    if (partPopulationDependanteVoiturePourcent === null || partPopulationDependanteVoitureFrance === null) {
        return 'Les données sont indisponibles pour ce territoire.';
    }
    return `
    Sur le territoire <em>${nomTerrtoire}</em>, 
    <strong>${partPopulationDependanteVoiturePourcent}&nbsp;% de la population</strong> est théoriquement dépendante de la voiture pour ses achats alimentaires.
    <br/>En France 🇫🇷, c'est ${partPopulationDependanteVoitureFrance}&nbsp;% de la population.`;
}

export function calculerMessagePartTerritoireDependantVoiture(
    nomTerrtoire: string,
    codeCategorieTerritoire: string,
    partTerritoireDependantVoiturePourcent: number | null,
    partTerritoireDependantVoitureFrance: number | null
): string {
    if (partTerritoireDependantVoiturePourcent === null || partTerritoireDependantVoitureFrance === null) {
        return 'Les données sont indisponibles pour ce territoire.';
    }
    return `
    Dans <strong>${partTerritoireDependantVoiturePourcent}&nbsp;% 
    ${codeCategorieTerritoire == CategorieTerritoire.Commune.codeCategorie ? 'du territoire' : 'des communes du territoire'}</strong>, 
    plus de la moitié de la population est théoriquement dépendante de la voiture pour ses achats alimentaires.
    <br/>En France 🇫🇷, c'est ${partTerritoireDependantVoitureFrance}&nbsp;% des communes qui sont dans cette situation.`;
}

import { describe, expect, it } from 'vitest';

import { CategorieCommerce } from './CategorieCommerce';

describe("Test de l'énumération CategorieCommerce", () => {
    it('Vérification de la comparaison entre catégories', () => {
        expect(CategorieCommerce.BoucheriePoissonnerie.comparer(CategorieCommerce.CommerceGeneraliste)).toBeGreaterThan(0);
        expect(CategorieCommerce.CommerceGeneraliste.comparer(CategorieCommerce.CommerceGeneraliste)).toEqual(0);
    });

    it("Vérification de la récupération d'une catégorie à partir d'une string", () => {
        expect(CategorieCommerce.fromString('COMMERCE_SPECIALISE')).toEqual(CategorieCommerce.CommerceSpecialise);
    });
});

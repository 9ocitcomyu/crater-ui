import { fetchRetry } from '@lga/commun/build/outils/requetes-utils';
import { GeoJsonObject } from 'geojson';

import { EchelleTerritoriale } from '../domaines/carte/EchelleTerritoriale';
import { ValeurIndicateurParIdTerritoire } from '../pages/carte/composants/widget-carte/TerritoiresVisiblesCarte';
import geojsonHotelsDeDepartementsUrl from '../ressources/geojson/hotels-de-departements.geojson';
import geojsonHotelsDeRegionsUrl from '../ressources/geojson/hotels-de-regions.geojson';

export function chargerIndicateurs(
    apiBaseUrl: string,
    nomIndicateurRequeteApi: string,
    categorieTerritoire: string,
    idDepartements?: string
): Promise<ValeurIndicateurParIdTerritoire[]> {
    let urlApi = '';
    if (categorieTerritoire === EchelleTerritoriale.Communes.id) {
        urlApi = `${apiBaseUrl}crater/api/indicateurs/${nomIndicateurRequeteApi}${
            nomIndicateurRequeteApi.includes('?') ? '&' : '?'
        }categorieTerritoire=${categorieTerritoire}&idsDepartements=${idDepartements}`;
    } else {
        urlApi = `${apiBaseUrl}crater/api/indicateurs/${nomIndicateurRequeteApi}${
            nomIndicateurRequeteApi.includes('?') ? '&' : '?'
        }categorieTerritoire=${categorieTerritoire}`;
    }

    return fetchRetry(urlApi).then((response) => response.json());
}

export function chargerContoursTerritoires(apiBaseUrl: string, nomGeojson: string, idDepartement?: string) {
    if (nomGeojson === EchelleTerritoriale.Communes.nomGeojson) {
        return fetchRetry(apiBaseUrl + `crater/api/cartes/${nomGeojson}-crater/communes_du_departement_${idDepartement}.geojson`).then((response) =>
            response.json()
        );
    }
    return fetchRetry(apiBaseUrl + `crater/api/cartes/${nomGeojson}-crater.geojson`).then((response) => response.json());
}

export function chargerCoucheHotelsDepartements(): Promise<GeoJsonObject> {
    return fetchRetry(geojsonHotelsDeDepartementsUrl).then((response) => response.json());
}

export function chargerCoucheHotelsRegions(): Promise<GeoJsonObject> {
    return fetchRetry(geojsonHotelsDeRegionsUrl).then((response) => response.json());
}

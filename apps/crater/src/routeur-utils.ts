import { Context as ContextPageJS } from 'page';

import {
    DonneesPage,
    DonneesPageCarte,
    DonneesPageDiagnostic,
    DonneesPageDiagnosticIndicateur,
    DonneesPageDiagnosticMaillon
} from './domaines/pages/donnees-pages';
import { ModelePage } from './domaines/pages/ModelePage';
import { ModelePageStatique } from './domaines/pages/ModelePageStatique';
import { SA } from './domaines/systeme-alimentaire/configuration/systeme-alimentaire';

export function routePageStatique(page: ModelePageStatique): string {
    return page.getUrl();
}

export function creerDonneesInitialesPageBase(contextPageJS: ContextPageJS): DonneesPage {
    return {
        idElementCibleScroll: contextPageJS.hash
    };
}

export function routePageDiagnostic(page: ModelePage<DonneesPageDiagnostic>): string {
    return page.getUrl({
        idTerritoirePrincipal: ':territoire'
    });
}

export function creerDonneesInitialesPageDiagnostic(contextPageJS: ContextPageJS, chapitre: string): DonneesPageDiagnostic {
    return {
        ...creerDonneesInitialesPageBase(contextPageJS),
        idElementCibleScroll: contextPageJS.hash,
        idTerritoirePrincipal: contextPageJS.params.territoire,
        idEchelleTerritoriale: getRequestParamValue(contextPageJS.querystring, 'echelleterritoriale')?.toUpperCase(),
        chapitre: chapitre
    };
}

export function routePageDiagnosticMaillon(page: ModelePage<DonneesPageDiagnosticMaillon>): string {
    return page.getUrl({
        idTerritoirePrincipal: ':territoire',
        idMaillon: ':maillon'
    });
}
export function creerDonneesInitialesPageDiagnosticMaillon(contextPageJS: ContextPageJS): DonneesPageDiagnosticMaillon {
    return {
        ...creerDonneesInitialesPageBase(contextPageJS),
        ...creerDonneesInitialesPageDiagnostic(contextPageJS, contextPageJS.params.maillon),
        idMaillon: contextPageJS.params.maillon,
        nomMaillon: SA.getMaillon(contextPageJS.params.maillon)?.nom ?? ''
    };
}

export function routePageDiagnosticIndicateur(page: ModelePage<DonneesPageDiagnosticIndicateur>): string {
    return page.getUrl({
        idTerritoirePrincipal: ':territoire',
        idIndicateur: ':indicateur'
    });
}
export function creerDonneesInitialesPageDiagnosticIndicateur(contextPageJS: ContextPageJS): DonneesPageDiagnosticIndicateur {
    return {
        ...creerDonneesInitialesPageBase(contextPageJS),
        ...creerDonneesInitialesPageDiagnostic(contextPageJS, contextPageJS.params.indicateur),
        idIndicateur: contextPageJS.params.indicateur,
        nomIndicateur: SA.getIndicateur(contextPageJS.params.indicateur)?.libelle ?? ''
    };
}

export function routePageCarteIndicateur(page: ModelePage<DonneesPageCarte>): string {
    return page.getUrl({
        idIndicateur: ':indicateur',
        idEchelleTerritoriale: ':echelleTerritoriale?'
    });
}
export function creerDonneesInitialesPageCarte(contextPageJS: ContextPageJS): DonneesPageCarte {
    return {
        ...creerDonneesInitialesPageBase(contextPageJS),
        idIndicateur: contextPageJS.params.indicateur,
        nomIndicateur: SA.getIndicateur(contextPageJS.params.indicateur)?.libelle ?? '',
        idEchelleTerritoriale: contextPageJS.params.echelleterritoriale ? contextPageJS.params.echelleterritoriale.toUpperCase() : undefined,
        idTerritoirePrincipal: getRequestParamValue(contextPageJS.querystring, 'territoire')
    };
}

function getRequestParamValue(queryString: string, requestParamName: string): string | undefined {
    const params = new URLSearchParams(queryString);
    return params.has(requestParamName) ? params.get(requestParamName)! : undefined;
}

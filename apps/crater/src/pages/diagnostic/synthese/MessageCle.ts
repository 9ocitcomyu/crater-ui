import '../../commun/JaugeNoteSur10.js';
import '@lga/design-system/build/composants/Bouton.js';

import { Bouton } from '@lga/design-system/build/composants/Bouton.js';
import { ICONES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/icones-design-system.js';
import { CSS_BREAKPOINT_MAX_WIDTH_TABLETTE } from '@lga/design-system/build/styles/styles-breakpoints';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system';
import { css, html, LitElement, TemplateResult, unsafeCSS } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { unsafeHTML } from 'lit/directives/unsafe-html.js';

import { Note, NOTE_VALEUR_WARNING } from '../../../domaines/diagnostics';
import { stylesIndicateurNotes } from '../../../domaines/systeme-alimentaire/configuration/indicateurs';

declare global {
    interface HTMLElementTagNameMap {
        'c-message-cle': MessageCle;
    }
}

export interface OptionsMessageCle {
    icone: TemplateResult;
    titre: string;
    sousTitre?: string;
    note: Note;
    message: TemplateResult;
    href: string;
}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const iconeCecilia = html` <svg
    viewBox="0 0 16 16"
    xmlns="http://www.w3.org/2000/svg"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xml:space="preserve"
    version="1.1"
>
    <defs>
        <rect height="14.9" width="13.4" y="134.7" x="135.1" id="SVGID_1_" />
    </defs>
    <clipPath id="SVGID_00000063617146874458524370000006723059376206369724_">
        <use x="-133.84306" y="-134.16562" id="svg_2" xlink:href="#SVGID_1_" />
    </clipPath>
    <g>
        <g id="svg_1">
            <path
                id="svg_3"
                fill="var(--couleur-primaire)"
                clip-path="url(#SVGID_00000063617146874458524370000006723059376206369724_)"
                d="m13.15694,11.43438c2.3,-1.7 2.2,-6.7 -1.8,-8.5c-2.9,-1.3 -3.4,-1.8 -5.8,-2.3c0,0 -4.4,-0.8 -4.2,3.3c0,0 0.5,4.7 0,7.2c0,0 -0.6,3.6 3.5,4.2c2,0.3 3.1,0.1 8.3,-3.9"
            />
            <polyline
                id="svg_4"
                stroke-linejoin="round"
                stroke-linecap="round"
                stroke-width="1.539"
                stroke="#FFFFFF"
                fill="none"
                points="6.2569451332092285,11.3343825340271 9.75694465637207,8.034379482269287 6.2569451332092285,4.634385585784912 "
                class="st1"
            />
        </g>
    </g>
</svg>`;

const iconeBenj = html` <svg width="50" height="50" xmlns="http://www.w3.org/2000/svg" fill="none">
    <g>
        <path
            id="svg_1"
            fill="var(--couleur-primaire)"
            d="m40.78159,15.3431c7.6435,4.1692 7.6435,15.1446 0,19.3138l-21.0284,11.47c-7.33014,3.9982 -16.2674,-1.3072 -16.2674,-9.6569l0,-22.94c0,-8.3497 8.93725,-13.65513 16.2674,-9.65689l21.0284,11.46999z"
        />
        <path
            id="svg_2"
            stroke-linecap="round"
            stroke-width="5"
            stroke="var(--couleur-blanc)"
            d="m14.72781,34.38786c0,0 17.6719,-9.9148 17.6757,-10.0809c0.0038,-0.1661 -18,-9.9208 -18,-9.9208"
        />
    </g>
</svg>`;

@customElement('c-message-cle')
export class MessageCle extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        css`
            :host {
                display: block;
                width: 100%;
                max-width: 800px;
                margin: 0 auto;
            }

            a {
                display: grid;
                grid-template-columns: 80px 24px auto 1px;
                grid-template-rows: auto;
                grid-template-areas:
                    'jauge icone titre bouton-icone'
                    'jauge message message bouton-icone';
                grid-column-gap: calc(2 * var(--dsem));
                align-items: center;
                justify-items: center;
                padding: calc(2 * var(--dsem));
                background-color: var(--couleur-blanc);
                border-radius: calc(6 * var(--dsem));
                --couleur-icone: var(--couleur-primaire);
                cursor: pointer;
                text-decoration: none;
                color: var(--couleur-neutre);
            }

            c-jauge-note-sur-10 {
                grid-area: jauge;
                width: 80px;
                height: 80px;
            }
            a > svg {
                grid-area: icone;
                width: 24px;
                height: 24px;
            }
            #titre {
                grid-area: titre;
                justify-self: start;
                display: flex;
                flex-wrap: wrap;
                align-items: baseline;
                gap: calc(var(--dsem));
            }
            #message {
                grid-area: message;
                justify-self: start;
            }

            c-bouton {
                grid-area: bouton-texte;
                display: var(--displayPDF, none);
            }

            #bouton-icone {
                grid-area: bouton-icone;
            }
            #bouton-icone > svg {
                position: relative;
                right: -15px;
                width: 48px;
                height: 48px;
            }

            a:hover #bouton-icone path {
                fill: var(--couleur-accent);
            }

            @media screen and (max-width: ${unsafeCSS(CSS_BREAKPOINT_MAX_WIDTH_TABLETTE + 'px')}) {
                a {
                    grid-template-columns: 24px auto;
                    grid-template-rows: auto;
                    grid-template-areas:
                        'icone titre'
                        'jauge jauge'
                        'message message'
                        'bouton-texte bouton-texte';
                    grid-row-gap: calc(2 * var(--dsem));
                    border-radius: var(--dsem);
                }
                a > svg {
                    align-self: baseline;
                }
                c-bouton {
                    display: var(--displayPDF, initial);
                }
                #bouton-icone {
                    display: none;
                }
            }
        `
    ];

    @property({ attribute: false })
    options!: OptionsMessageCle;

    render() {
        if (!this.options) return;

        return html` <a href="${this.options.href}">
            <c-jauge-note-sur-10
                class="note"
                note="${this.options.note!}"
                couleur="${this.definirCouleurSelonNote(this.options.note!)}"
            ></c-jauge-note-sur-10>
            ${this.options.icone}
            <div id="titre">
                <div class="texte-alternatif">${unsafeHTML(this.options.titre)}</div>
                <div class="texte-petit">${unsafeHTML(this.options.sousTitre)}</div>
            </div>
            <div id="message" class="texte-moyen">${this.options.message}</div>
            <c-bouton
                href="${this.options.href}"
                libelle="Voir le détail"
                libelleTooltip="Voir le détail"
                type="${Bouton.TYPE.plat}"
                positionIcone="${Bouton.POSITION_ICONE.droite}"
                tailleReduite
            >
                ${ICONES_DESIGN_SYSTEM.flecheDroite}
            </c-bouton>
            <div id="bouton-icone">${iconeBenj}</div>
        </a>`;
    }

    private definirCouleurSelonNote(note: Note) {
        return note === NOTE_VALEUR_WARNING ? stylesIndicateurNotes.calculerCouleur(5) : stylesIndicateurNotes.calculerCouleur(note);
    }
}

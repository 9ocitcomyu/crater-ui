import './BoiteInformationsTerritoire.js';
import './BoiteMessagesCle.js';
import '../charts/ChartRadar.js';
import './MenuExportDiagnostic.js';

import { CSS_BREAKPOINT_MAX_WIDTH_MOBILE_PAYSAGE, CSS_BREAKPOINT_MAX_WIDTH_TABLETTE } from '@lga/design-system/build/styles/styles-breakpoints';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { css, html, LitElement, unsafeCSS } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { unsafeHTML } from 'lit/directives/unsafe-html.js';

import { Rapport } from '../../../domaines/diagnostics';
import { PAGES_DIAGNOSTIC } from '../../../domaines/pages/configuration/declaration-pages';
import { DonneesPageDiagnostic } from '../../../domaines/pages/donnees-pages.js';
import { STYLES_CRATER } from '../../commun/pages-styles';
import { getVariableCss, Serie, SerieMultiple } from '../charts/chart-outils';
import { OptionsChartRadar } from '../charts/ChartRadar.js';
import { STYLES_CHAPITRES_DIAGNOSTIC } from '../diagnostic-utils.js';
import { OptionsBoiteInformationsTerritoire } from './BoiteInformationsTerritoire';

declare global {
    interface HTMLElementTagNameMap {
        'c-chapitre-synthese': ChapitreSynthese;
    }
}

@customElement('c-chapitre-synthese')
export class ChapitreSynthese extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        STYLES_CRATER,
        STYLES_CHAPITRES_DIAGNOSTIC,
        css`
            section {
                display: grid;
                grid-template-columns: 2fr 1fr;
                grid-template-rows: min-content 1fr 1fr;
                grid-template-areas:
                    'bloc-titre bloc-titre'
                    'bloc-messages-cles bloc-infos-territoire'
                    'bloc-messages-cles bloc-chart-radar';
                gap: 1rem;
                align-items: stretch;
                justify-items: stretch;
                align-content: space-around;
                padding: calc(4 * var(--dsem)) 0;
            }

            section.PDF {
                grid-template-columns: 1fr;
                grid-template-rows: auto;
                grid-template-areas:
                    'bloc-titre'
                    'bloc-infos-territoire'
                    'bloc-messages-cles'
                    'bloc-chart-radar';
            }

            section.PDF c-menu-exporter {
                display: none;
            }

            section.PDF h1 {
                margin: auto;
            }

            #bloc-titre {
                grid-area: bloc-titre;
                display: flex;
                flex-direction: row;
                align-items: center;
                justify-content: space-between;
            }

            c-boite-informations-territoire {
                grid-area: bloc-infos-territoire;
                height: 100%;
            }

            #bloc-infos-territoire {
                grid-area: bloc-infos-territoire;
            }

            c-boite-messages-cle {
                grid-area: bloc-messages-cles;
            }

            #bloc-chart-radar {
                grid-area: bloc-chart-radar;
            }

            h1 {
                color: var(--couleur-primaire);
                margin: 0;
            }

            @media screen and (max-width: ${unsafeCSS(CSS_BREAKPOINT_MAX_WIDTH_TABLETTE + 'px')}) {
                section {
                    grid-template-columns: 1fr;
                    grid-template-rows: auto;
                    grid-template-areas: 'bloc-titre' 'bloc-infos-territoire' 'bloc-messages-cles' 'bloc-chart-radar';
                    padding-top: var(--dsem);
                }

                #bloc-titre {
                    flex-direction: column;
                    align-items: start;
                }

                c-menu-exporter {
                    display: none;
                }
            }
            @media screen and (max-width: ${unsafeCSS(CSS_BREAKPOINT_MAX_WIDTH_MOBILE_PAYSAGE + 'px')}) {
                section {
                    padding-top: 0;
                }
            }
        `
    ];

    @property({ attribute: false })
    donneesPage?: DonneesPageDiagnostic;

    @property({ attribute: false })
    rapport?: Rapport;

    @property({ type: Boolean })
    affichagePDF? = false;

    render() {
        if (!this.rapport) return;

        return html`
            <section class="${this.affichagePDF ? 'PDF' : ''}">
                <div id="bloc-titre">
                    <h1 class="titre-moyen">Diagnostic du système alimentaire | ${this.rapport.getTerritoireActif()?.nom}</h1>
                    <c-menu-exporter></c-menu-exporter>
                </div>
                <c-boite-informations-territoire
                    .options=${<OptionsBoiteInformationsTerritoire>{
                        nomTerritoire: this.rapport?.getDiagnosticActif()?.territoire?.nom ?? '',
                        message: unsafeHTML(this.rapport?.getDiagnosticActif()?.messageTerritoire) ?? '',
                        population: this.rapport?.getDiagnosticActif()?.population ?? 0,
                        superficieHa: this.rapport?.getDiagnosticActif()?.superficieHa ?? 0,
                        sauProductiveHa: this.rapport?.getDiagnosticActif()?.surfaceAgricoleUtile.sauProductiveHa ?? 0,
                        lienPageTerritoire: PAGES_DIAGNOSTIC.territoire.getUrl({
                            idTerritoirePrincipal: this.donneesPage?.idTerritoirePrincipal ?? '',
                            idEchelleTerritoriale: this.donneesPage?.idEchelleTerritoriale
                        })
                    }}
                ></c-boite-informations-territoire>
                <c-boite-messages-cle .rapport=${this.rapport} .donneesPage=${this.donneesPage}></c-boite-messages-cle>
                <div id="bloc-chart-radar">
                    <c-chart-radar .options=${this.calculerOptionsChartRadar(this.rapport)}></c-chart-radar>
                </div>
            </section>
        `;
    }

    private calculerOptionsChartRadar(rapport: Rapport): OptionsChartRadar {
        // TODO : voir avec apex chart comment mieux gérer le cas ou la note est absente : ici on passe 0 au chart, qui affiche donc 0 dans le tooltip (plutot que "-")
        return {
            series: new SerieMultiple(
                [
                    new Serie(
                        [
                            <number | null>rapport.getDiagnosticActif()!.intrants.note!,
                            <number | null>rapport.getDiagnosticActif()!.production.note!,
                            <number | null>rapport.getDiagnosticActif()!.transformationDistribution.note!,
                            <number | null>rapport.getDiagnosticActif()!.terresAgricoles.note!,
                            <number | null>rapport.getDiagnosticActif()!.agriculteursExploitations.note!
                        ],
                        rapport.getDiagnosticActif()!.territoire.nom
                    ),
                    new Serie(
                        [
                            <number | null>rapport.getDiagnosticPays()!.intrants.note!,
                            <number | null>rapport.getDiagnosticPays()!.production.note!,
                            <number | null>rapport.getDiagnosticPays()!.transformationDistribution.note!,
                            <number | null>rapport.getDiagnosticPays()!.terresAgricoles.note!,
                            <number | null>rapport.getDiagnosticPays()!.agriculteursExploitations.note!
                        ],
                        rapport.getDiagnosticPays()!.territoire.nom
                    )
                ],
                [['Intrants'], ['Production'], ['Transformation', 'Distribution'], ['Terres', 'agricoles'], ['Agriculteurs', '&', 'Exploitations']]
            ),

            couleurRemplissageSeries: [getVariableCss('--c-chart-radar'), getVariableCss('--c-chart-radar-france')],
            couleurBordureSeries: [getVariableCss('--c-chart-radar-bordure'), getVariableCss('--c-chart-radar-france-bordure')]
        };
    }
}

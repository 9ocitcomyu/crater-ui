import '../chapitres/MenuNavigationChapitres.js';
import './MenuMobileDiagnosticModifierTerritoire.js';
import '@lga/design-system/build/composants/PanneauGlissant.js';
import '@lga/design-system/build/composants/Bouton.js';

import { Bouton } from '@lga/design-system/build/composants/Bouton.js';
import { OptionsRadioBouton } from '@lga/design-system/build/composants/RadioBouton';
import { EvenementFermer } from '@lga/design-system/build/evenements/EvenementFermer.js';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { css, html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import { Rapport } from '../../../domaines/diagnostics/index.js';
import { DonneesPageDiagnostic } from '../../../domaines/pages/donnees-pages.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-menu-mobile-sommaire-diagnostic': MenuMobileSommaireDiagnostic;
    }
}

@customElement('c-menu-mobile-sommaire-diagnostic')
export class MenuMobileSommaireDiagnostic extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        css`
            :host {
                display: block;
                height: 100%;
                width: 100%;
            }

            #bouton-changer-territoire {
                display: flex;
                flex-direction: column;
                gap: var(--dsem);
                margin: auto 1rem;
                padding: 1.5em;
                cursor: pointer;
                border: solid 1px var(--couleur-neutre-20);
                border-radius: var(--dsem);
            }

            #votre-recherche {
                color: var(--couleur-neutre-80);
            }

            #territoire-modifier {
                display: flex;
                gap: calc(2 * var(--dsem));
                align-items: center;
            }

            #nom-territoire {
                color: var(--couleur-neutre);
            }

            #bouton-changer-territoire:hover #modifier {
                color: var(--couleur-accent);
            }

            c-menu-navigation-chapitres {
                margin-top: calc(2 * var(--dsem));
            }
        `
    ];

    @property({ attribute: false })
    donneesPage?: DonneesPageDiagnostic;

    @property({ attribute: false })
    rapport?: Rapport;

    render() {
        return html`
            <c-panneau-glissant>
                <div id="bouton-changer-territoire" slot="bouton-ouvrir">
                    <div id="votre-recherche" class="texte-titre">Votre territoire actuel :</div>
                    <div id="territoire-modifier">
                        <div id="nom-territoire" class="texte-moyen">${this.rapport?.getTerritoireActif()?.nom}</div>
                        <c-bouton type=${Bouton.TYPE.plat} libelle="Modifier" libelleTooltip="Modifier le territoire"> </c-bouton>
                    </div>
                </div>
                <c-menu-mobile-diagnostic-modifier-territoire
                    slot="panneau"
                    .optionsEchellesTerritoriales=${<OptionsRadioBouton>{
                        boutons: this.rapport
                            ?.getHierarchieTerritoires()
                            ?.getListeTerritoires()
                            .map((t) => {
                                return {
                                    id: t.categorie.codeCategorie,
                                    libelle: t.categorie.libelleCategorieHtml,
                                    sousLibelle: t.nom
                                };
                            }),
                        idBoutonActif: this.rapport?.getTerritoireActif()?.categorie.codeCategorie
                    }}
                ></c-menu-mobile-diagnostic-modifier-territoire>
            </c-panneau-glissant>
            <c-menu-navigation-chapitres
                @selectionner=${this.fermerParents}
                .donneesPage=${this.donneesPage}
                .diagnostic=${this.rapport?.getDiagnosticActif()}
            >
            </c-menu-navigation-chapitres>
        `;
    }

    private fermerParents() {
        this.dispatchEvent(new EvenementFermer(true));
    }
}

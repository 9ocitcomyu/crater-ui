import { CSS_BREAKPOINT_MAX_WIDTH_TABLETTE } from '@lga/design-system/build/styles/styles-breakpoints';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system';
import { css, html, LitElement, unsafeCSS } from 'lit';
import { customElement } from 'lit/decorators.js';
import { unsafeSVG } from 'lit/directives/unsafe-svg.js';

import { PAGES_PRINCIPALES } from '../../../domaines/pages/configuration/declaration-pages';
import { ICONES_SVG } from '../../../domaines/systeme-alimentaire/configuration/icones_svg';
import DESCRIPTION_CRATER_VISUEL_2 from '../../../ressources/illustrations/diagnostic/diagnostic.png';

declare global {
    interface HTMLElementTagNameMap {
        'c-description-diagnostic': DescriptionDiagnostic;
    }
}
@customElement('c-description-diagnostic')
export class DescriptionDiagnostic extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        css`
            :host {
                display: block;
                width: 100%;
                height: 100%;
            }
            main {
                display: flex;
                gap: calc(20 * var(--dsem));
            }
            section {
                width: 100%;
            }
            figure {
                margin: 0;
            }
            figure > img {
                width: 300px;
                height: auto;
            }

            h1 {
                display: flex;
                flex-direction: column;
                gap: calc(var(--dsem) * 2);
                color: var(--couleur-primaire-sombre);
            }
            h1 > svg {
                width: 130px;
            }
            ul {
                list-style: none;
                padding: 0;
            }
            li {
                display: flex;
                align-items: center;
                gap: var(--dsem);
                margin-bottom: var(--dsem);
            }
            span svg {
                fill: var(--couleur-secondaire);
            }
            p {
                margin: 0;
            }

            @media screen and (max-width: ${unsafeCSS(CSS_BREAKPOINT_MAX_WIDTH_TABLETTE)}px) {
                main {
                    flex-direction: column;
                    align-items: center;
                }
                figure {
                    width: fit-content;
                }
                figure > img {
                    width: 250px;
                    height: 210px;
                }
            }
        `
    ];

    private readonly items = [
        html`Les communes, intercommunalités, départements et régions.`,
        html`Les regroupements de communes type PAT, SCOT, etc.`
    ];

    render() {
        return html`
            <main>
                <section>
                    <h1 class="titre-moyen">
                        L’outil CRATer propose un diagnostic ciblé de résilience et de durabilité du système alimentaire à l’échelon local.
                        ${unsafeSVG(ICONES_SVG.soulignerTexte)}
                    </h1>
                    <p class="texte-titre">Le diagnostic est disponible pour :</p>
                    <ul class="texte-moyen">
                        ${this.items.map(
                            (item) =>
                                html`<li>
                                    <span>${unsafeSVG(ICONES_SVG.puceEnumerationListe)}</span>
                                    <p>${item}</p>
                                </li>`
                        )}
                    </ul>
                    <p>
                        Vous ne trouvez pas votre regroupement de communes? Demandez-en l'ajout
                        <c-lien href=${PAGES_PRINCIPALES.demandeAjoutTerritoire.getUrl()}>ici</c-lien> !
                    </p>
                </section>
                <figure><img src="${DESCRIPTION_CRATER_VISUEL_2}" width="300" height="282" /></figure>
            </main>
        `;
    }
}

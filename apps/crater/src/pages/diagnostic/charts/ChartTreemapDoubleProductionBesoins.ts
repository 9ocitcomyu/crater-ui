import './ChartTreemapCultures.js';
import './LegendeChart.js';

import { formaterNombreEnEntierString } from '@lga/commun/build/outils/base';
import { CSS_BREAKPOINT_MAX_WIDTH_MOBILE_PAYSAGE } from '@lga/design-system/build/styles/styles-breakpoints.js';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { css, html, LitElement, unsafeCSS } from 'lit';
import { customElement, property, state } from 'lit/decorators.js';

import { GroupeCulture } from '../../../domaines/systeme-alimentaire';
import { DonneesTreemapCultures, OptionsDonnesTreemapCultures } from './ChartTreemapCultures';
import { ItemLegende } from './LegendeChart';

const HAUTEUR_MAX_PIXELS = 350;

export interface OptionsChartTreemapDoubleProductionBesoins {
    productionHa: number;
    besoinsHa: number;
    donneesTreemapProduction: DonneesTreemapCultures[];
    donneesTreemapBesoins: DonneesTreemapCultures[];
}

declare global {
    interface HTMLElementTagNameMap {
        'c-chart-treemap-production-besoins': ChartTreemapDoubleProductionBesoins;
    }
}

const ITEMS_LEGENDE_TOUS_GROUPES_CULTURES: ItemLegende[] = GroupeCulture.tous.map((g) => {
    return { libelle: `${g.nom} (${g.nomCourt})`, couleur: g.couleur, motif: '' };
});

@customElement('c-chart-treemap-production-besoins')
export class ChartTreemapDoubleProductionBesoins extends LitElement {
    @state()
    private optionsTreemapProduction!: OptionsDonnesTreemapCultures;

    @state()
    private optionsTreemapBesoins!: OptionsDonnesTreemapCultures;

    @property({ attribute: false })
    options?: OptionsChartTreemapDoubleProductionBesoins;

    @state()
    private conserverProportions = true;

    static styles = [
        STYLES_DESIGN_SYSTEM,
        css`
            figure {
                margin: auto;
                max-width: 600px;
                text-align: center;
            }

            #charts {
                display: grid;
                grid-auto-flow: column;
                align-items: end;
            }

            .texte {
                margin: 0;
            }

            @media screen and (max-width: ${unsafeCSS(CSS_BREAKPOINT_MAX_WIDTH_MOBILE_PAYSAGE + 'px')}) {
                #charts {
                    display: block;
                }
            }
        `
    ];

    render() {
        return html`
            <figure>
                <div id="charts">
                    <div class="chart-treemap">
                        <c-chart-treemap-cultures .options=${this.optionsTreemapProduction} id="chart-treemap-production"></c-chart-treemap-cultures>
                        <p class="texte">Production : ${formaterNombreEnEntierString(this.options?.productionHa ?? null)} ha</p>
                    </div>
                    <div class="chart-treemap">
                        <c-chart-treemap-cultures .options=${this.optionsTreemapBesoins} id="chart-treemap-besoins"></c-chart-treemap-cultures>
                        <p class="texte">Consommation : ${formaterNombreEnEntierString(this.options?.besoinsHa ?? null)} ha</p>
                    </div>
                </div>
                <c-legende-chart .itemsLegende=${ITEMS_LEGENDE_TOUS_GROUPES_CULTURES}></c-legende-chart>
                <div>
                    <label class="texte-moyen">Basculer l'affichage: </label>
                    <select id="selecteur" @change=${this.changerOptionSelecteur}>
                        <option value="conserver" ?selected=${this.conserverProportions}>Conserver proportions</option>
                        <option value="etirer" ?selected=${!this.conserverProportions}>Étirer</option>
                    </select>
                </div>
            </figure>
        `;
    }

    willUpdate(): void {
        if (this.options?.donneesTreemapProduction !== undefined && this.options?.donneesTreemapBesoins !== undefined) {
            let hauteurTreemapProduction = HAUTEUR_MAX_PIXELS;
            let hauteurTreemapBesoins = HAUTEUR_MAX_PIXELS;
            if (this.conserverProportions) {
                if (this.options.productionHa >= this.options.besoinsHa) {
                    hauteurTreemapBesoins = HAUTEUR_MAX_PIXELS * (this.options.besoinsHa / this.options.productionHa);
                } else {
                    hauteurTreemapProduction = HAUTEUR_MAX_PIXELS * (this.options.productionHa / this.options.besoinsHa);
                }
            }
            this.optionsTreemapProduction = { donnees: this.options.donneesTreemapProduction, hauteur: hauteurTreemapProduction };
            this.optionsTreemapBesoins = { donnees: this.options.donneesTreemapBesoins, hauteur: hauteurTreemapBesoins };
        }
    }

    private changerOptionSelecteur() {
        this.conserverProportions = !this.conserverProportions;
    }
}

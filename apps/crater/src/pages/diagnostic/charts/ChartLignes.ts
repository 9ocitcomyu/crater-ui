import { fusionner } from '@lga/commun/build/outils/base';
import { ApexOptions } from 'apexcharts';
import ApexCharts from 'apexcharts';
import { css, html, LitElement } from 'lit';
import { customElement, property, query } from 'lit/decorators.js';

import { APEX_OPTIONS_GLOBALES } from './chart-config';
import { formaterLabelNombre, ModeFiltrageSeries, SerieMultiple } from './chart-outils';

export interface OptionsChartLignes {
    series: SerieMultiple;
    couleurs: string[];
    yaxisTitre: string;
    xaxisCategories: (string | number)[];
}

declare global {
    interface HTMLElementTagNameMap {
        'c-chart-lignes': ChartLignes;
    }
}

@customElement('c-chart-lignes')
export class ChartLignes extends LitElement {
    private chart?: ApexCharts;

    @property({ attribute: false })
    options?: OptionsChartLignes;

    @query('figure')
    private figure?: HTMLElement;

    static styles = css`
        figure {
            margin: auto;
            max-width: 600px;
        }
    `;

    render() {
        return html`
            <div>
                <figure></figure>
            </div>
        `;
    }

    async updated() {
        if (this.chart) {
            this.chart?.updateOptions(this.calculerApexOptions(this.options), true, true);
        } else {
            this.chart = new ApexCharts(this.figure, this.calculerApexOptions(this.options));
            await this.chart?.render();
            // Le premier render d'apex ne fonctionne pas dans lit (diagramme vide). Contournement : on attend la fin du render, et on fait un update
            this.chart?.updateOptions(this.calculerApexOptions(this.options), true, true);
        }
    }

    private calculerApexOptions(options: OptionsChartLignes | undefined): ApexOptions {
        if (!options) return APEX_OPTIONS_GLOBALES;

        const apexOptionsLocales = {
            series: options.series.filtrerValeursNull(ModeFiltrageSeries.SUPPRIMER_SERIES_AVEC_AU_MOINS_UNE_VALEUR_NULL).versSerieApexSingleValue(),
            colors: options.couleurs,
            stroke: {
                width: 3
            },
            chart: {
                type: 'line',
                height: 400,
                stacked: false
            },
            xaxis: {
                categories: options.xaxisCategories
            },
            yaxis: {
                min: 0,
                axisTicks: {
                    show: true
                },
                labels: {
                    formatter: (labelInitial: string) => formaterLabelNombre(labelInitial)
                },
                title: {
                    text: options.yaxisTitre
                }
            },
            tooltip: {
                y: {
                    formatter: (labelInitial: string) => formaterLabelNombre(labelInitial)
                }
            }
        };

        return fusionner(APEX_OPTIONS_GLOBALES, apexOptionsLocales);
    }
}

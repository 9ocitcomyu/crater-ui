import { formaterNombreEnEntierString, formaterNombreEnNDecimalesString, formaterNombreSelonValeurString } from '@lga/commun/build/outils/base';

export type LabelMultilignes = string | string[];

export function decouperLabelEnPlusieursLignes(label: string | string[], longueurMaxiLigne: number): string[] {
    let resultat = [''];
    if (Array.isArray(label)) {
        for (let i = 0; i < label.length; i++) {
            resultat = resultat.concat(transformerEnLabelMultiligne(label[i], longueurMaxiLigne));
        }
        return resultat.filter((l) => l != '' && l != ' ');
    } else {
        return transformerEnLabelMultiligne(label, longueurMaxiLigne);
    }
}

export function getVariableCss(nomVariable: string) {
    return getComputedStyle(document.documentElement).getPropertyValue(nomVariable);
}

//TODO A rapprocher de LabelMultilignes (en faire une classe ?)
export function transformerEnLabelMultiligne(label: string, longueurMaxiLigne: number): string[] {
    // on transforme un string en tableau de string de longueur maximale donnée, en les séparant selon les caratères " " ou "-"
    const l = label.split(/(?= |-)/);
    const resultat = [];
    let r = l[0];
    for (let i = 1; i < l.length; i++) {
        if ((r + l[i]).trim().length <= longueurMaxiLigne) {
            r += l[i];
        } else {
            resultat.push(r);
            r = l[i];
        }
    }
    resultat.push(r);

    return resultat.map((e) => e.trim());
}

export function formaterLabelNombre(labelInitial: string) {
    return formaterNombreSelonValeurString(Number(labelInitial));
}

export function formaterLabelNombreEleve(labelInitial: string | number) {
    const valeur = typeof labelInitial === 'string' ? Number(labelInitial.replace(',', '.')) : labelInitial;
    if (!isNaN(valeur) && valeur < 100) {
        return formaterNombreEnNDecimalesString(valeur, 2);
    } else if (!isNaN(valeur) && valeur < 100000) {
        return formaterNombreEnEntierString(valeur);
    } else if (!isNaN(valeur) && valeur < 100000000) {
        return formaterNombreEnEntierString(valeur / 1000) + 'k';
    } else if (!isNaN(valeur)) {
        return formaterNombreEnEntierString(valeur / 1000000) + 'M';
    } else {
        return labelInitial;
    }
}

export enum ModeFiltrageSeries {
    SUPPRIMER_VALEURS_EN_POSITION_X_SI_AU_MOINS_UN_NULL = 'SUPPRIMER_VALEURS_EN_POSITION_X_SI_AU_MOINS_UN_NULL', // Garde les series, mais supprime toutes les valeurs à un rang donné quand il existe une valeur null
    SUPPRIMER_SERIES_AVEC_AU_MOINS_UNE_VALEUR_NULL = 'SUPPRIMER_SERIES_AVEC_AU_MOINS_UNE_VALEUR_NULL' // Si une série contient une valeur null, elle est supprimée
}

export class Serie {
    constructor(public readonly valeurs: (number | null)[] = [], public readonly nom: string = '') {}

    public aucunNull() {
        return this.valeurs.every((v) => v !== null);
    }
}

export class SerieMultiple {
    constructor(public readonly series: Serie[] = [], public readonly labels: LabelMultilignes[] = []) {}

    public filtrerValeursNull(modeFiltrage: ModeFiltrageSeries) {
        switch (modeFiltrage) {
            case ModeFiltrageSeries.SUPPRIMER_SERIES_AVEC_AU_MOINS_UNE_VALEUR_NULL:
                return new SerieMultiple(
                    this.series.filter((s) => s.aucunNull()),
                    this.labels
                );
            case ModeFiltrageSeries.SUPPRIMER_VALEURS_EN_POSITION_X_SI_AU_MOINS_UN_NULL: {
                const seriesFiltres = this.viderDonnees();
                for (let x = 0; x < this.series[0].valeurs.length; x++) {
                    if (this.aucunNullEnPositionX(x)) {
                        seriesFiltres.labels.push(this.labels[x]);
                        this.series.forEach((s, indiceSerie) => {
                            seriesFiltres.series[indiceSerie].valeurs.push(s.valeurs[x]);
                        });
                    }
                }
                return seriesFiltres;
            }
        }
    }

    public positionnerNomSiVide(nomSerie: string) {
        return new SerieMultiple(
            this.series.map((s) => {
                return new Serie(s.valeurs, s.nom ?? nomSerie);
            }),
            this.labels
        );
    }

    public formaterLabels(longueurMaxi: number) {
        return new SerieMultiple(
            this.series,
            this.labels.map((l) => {
                return decouperLabelEnPlusieursLignes(l, longueurMaxi);
            })
        );
    }

    public versSerieApexXY(): ApexAxisChartSeries {
        return this.series.map((s) => {
            const apexData = s.valeurs.map((v, x) => {
                return { x: this.labels[x], y: v };
            });
            return { name: s.nom, data: apexData };
        });
    }

    public versSerieApexSingleValue(): ApexAxisChartSeries {
        return this.series.map((s) => {
            return { name: s.nom, data: s.valeurs };
        });
    }

    private auMoinsUnNullEnPositionX(x: number) {
        return this.series.some((s) => s.valeurs[x] === null);
    }
    private aucunNullEnPositionX(x: number) {
        return !this.auMoinsUnNullEnPositionX(x);
    }

    private viderDonnees() {
        return new SerieMultiple(
            this.series.map((s) => {
                return new Serie([], s.nom);
            }),
            []
        );
    }
}

import { CSS_BREAKPOINT_MAX_WIDTH_DESKTOP_PETIT, CSS_BREAKPOINT_MAX_WIDTH_TABLETTE } from '@lga/design-system/build/styles/styles-breakpoints';
import { css, unsafeCSS } from 'lit';

export const STYLES_CHAPITRES_DIAGNOSTIC = css`
    :host {
        display: block;
    }

    c-fil-ariane {
        padding-bottom: var(--dsem);
    }

    .indicateurs-resumes {
        margin-top: calc(2 * var(--dsem));
        margin-bottom: calc(2 * var(--dsem));
    }

    .indicateurs-resumes {
        display: grid;
        grid-template-columns: 20% 5% 75%;
    }

    .indicateurs-resumes p {
        grid-column: 1;
        text-align: center;
        padding-right: calc(1 * var(--dsem));
        margin-top: auto;
        margin-bottom: auto;
    }

    .indicateurs-resumes c-timeline {
        grid-column: 2;
    }

    .bloc-indicateurs-resumes {
        grid-column: 3;
        display: flex;
        flex-direction: column;
        padding-left: calc(2 * var(--dsem));
        padding-top: calc(1 * var(--dsem));
        padding-bottom: calc(1 * var(--dsem));
    }

    /*--------------------------------------
    // PDF
    --------------------------------------*/

    section.PDF c-voir-egalement,
    section.PDF c-comment-agir,
    section.PDF #leviers-action,
    section.PDF c-encart-pages-precedente-suivante {
        display: none;
    }

    /*--------------------------------------
    // Charts
    --------------------------------------*/

    .chart-radar {
        grid-area: chart-radar;
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .conteneur-chart {
        position: relative;
        max-width: 600px;
        margin: calc(2 * var(--dsem)) auto;
        background-color: transparent;
    }

    @media screen and (max-width: ${unsafeCSS(CSS_BREAKPOINT_MAX_WIDTH_DESKTOP_PETIT + 'px')}) {
        :root {
            --largeur-menu-chapitres: 0px;
        }
    }

    @media screen and (max-width: ${unsafeCSS(CSS_BREAKPOINT_MAX_WIDTH_TABLETTE + 'px')}) {
        .indicateurs-resumes {
            display: flex;
            flex-direction: column;
        }

        .indicateurs-resumes p {
            text-align: left;
            margin: calc(2 * var(--dsem)) 0;
            font-weight: bold;
        }

        .bloc-indicateurs-resumes {
            margin: auto;
            padding: 0;
        }

        c-timeline {
            display: none;
        }
    }

    /* Breakpoint spécifique pour la page diagnostic et le radar */
    @media screen and (max-width: 650px) {
        :root {
            --base: 0.95;
        }

        header button {
            right: 1.5rem;
        }
    }
`;

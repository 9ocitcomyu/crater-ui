import './composants/MenuMobileSommaireDiagnostic.js';
import '../commun/PageSablier.js';
import '../commun/template/TemplatePageAvecMenuTerritoire.js';
import '@lga/design-system/build/composants/FilAriane.js';

import { LienFilAriane } from '@lga/design-system/build/composants/FilAriane.js';
import { CSS_BREAKPOINT_MAX_WIDTH_DESKTOP_PETIT } from '@lga/design-system/build/styles/styles-breakpoints.js';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { css, html, unsafeCSS } from 'lit';
import { customElement } from 'lit/decorators.js';
import { choose } from 'lit/directives/choose.js';

import { EtatRapport, Rapport } from '../../domaines/diagnostics';
import { PAGES_PRINCIPALES } from '../../domaines/pages/configuration/declaration-pages';
import { DonneesPageDiagnostic } from '../../domaines/pages/donnees-pages.js';
import {
    construireUrlPageDiagnosticIndicateur,
    construireUrlPageDiagnosticMaillon,
    construireUrlPageDiagnosticSynthese,
    construireUrlPageDiagnosticTerritoire
} from '../../domaines/pages/pages-utils';
import { IDS_INDICATEURS } from '../../domaines/systeme-alimentaire/configuration/indicateurs.js';
import { IDS_MAILLONS } from '../../domaines/systeme-alimentaire/configuration/maillons';
import { SA } from '../../domaines/systeme-alimentaire/configuration/systeme-alimentaire.js';
import { OptionsBarreMenuTerritoires } from '../commun/BarreMenuTerritoires';
import { importDynamique } from '../commun/importDynamiqueDirective';
import { STYLES_CRATER } from '../commun/pages-styles';
import { ControleurDiagnostic } from './ControleurDiagnostic';

declare global {
    interface HTMLElementTagNameMap {
        'c-page-diagnostic': PageDiagnostic;
    }
}

@customElement('c-page-diagnostic')
export class PageDiagnostic extends ControleurDiagnostic {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        STYLES_CRATER,
        css`
            c-template-page-avec-menu-territoire {
                --largeur-menu-chapitres: 300px;
                --largeur-maximum-contenu: 1100px;
            }

            main {
                background-color: var(--couleur-fond);
            }

            c-menu-navigation-chapitres {
                float: left;
                position: sticky;
                top: var(--hauteur-totale-entete);
                width: var(--largeur-menu-chapitres);
                height: calc(100vh - var(--hauteur-totale-entete));
            }

            #coeur-page {
                padding: calc(2 * var(--dsem));
                padding-right: max(
                    (100vw - var(--largeur-maximum-contenu) - var(--largeur-menu-chapitres)) / 2 + calc(2 * var(--dsem)),
                    calc(2 * var(--dsem))
                );
                padding-left: max(
                    (100vw - var(--largeur-maximum-contenu) - var(--largeur-menu-chapitres)) / 2 + var(--largeur-menu-chapitres) +
                        calc(2 * var(--dsem)),
                    calc(2 * var(--dsem))
                );
                width: 100%;
            }

            #coeur-page-synthese {
                padding: calc(2 * var(--dsem));
                width: 100%;
            }

            c-chapitre-synthese {
                max-width: 1400px;
                margin: auto;
                display: block;
                padding: 0 calc(4 * var(--dsem));
            }

            @media screen and (max-width: ${unsafeCSS(CSS_BREAKPOINT_MAX_WIDTH_DESKTOP_PETIT + 'px')}) {
                c-chapitre-synthese {
                    padding: 0 calc(0.5 * var(--dsem));
                }

                c-menu-navigation-chapitres {
                    display: none;
                }

                #coeur-page {
                    padding: var(--dsem);
                }
            }
        `
    ];

    render() {
        return html`
            <c-template-page-avec-menu-territoire
                idItemActifMenuPrincipal=${PAGES_PRINCIPALES.diagnostic.getId()}
                idElementCibleScroll=${this.donneesPage.idElementCibleScroll ?? ''}
                ?desactiverBoutonSommaire=${this.donneesPage.idTerritoirePrincipal === ''}
                .optionsBarreMenuTerritoires=${this.calculerOptionsBarreMenuTerritoires(this.rapport)}
            >
                <main slot="contenu" aria-live="off">${this.renderCoeurDePage()}</main>
                <c-menu-mobile-sommaire-diagnostic slot="contenu-sommaire" .donneesPage=${this.donneesPage} .rapport=${this.rapport}>
                </c-menu-mobile-sommaire-diagnostic>
            </c-template-page-avec-menu-territoire>
        `;
    }

    private calculerOptionsBarreMenuTerritoires(rapport: Rapport | undefined) {
        if (!rapport || rapport.getEtat() !== EtatRapport.PRET) {
            return {} as OptionsBarreMenuTerritoires;
        }

        return <OptionsBarreMenuTerritoires>{
            onglets: rapport
                .getHierarchieTerritoires()!
                .getListeTerritoires()
                .map((t) => {
                    return {
                        id: t.categorie.codeCategorie,
                        libelle: t.categorie.libelleCategorieHtml,
                        sousLibelle: t.nom
                    };
                }),
            idOngletSelectionne: rapport.getTerritoireActif()!.categorie.codeCategorie
        };
    }

    private renderCoeurDePage() {
        if (this.rapport && this.rapport.getEtat() === EtatRapport.PRET) {
            const chapitreCourant = this.donneesPage.chapitre;
            return html`
                ${chapitreCourant === 'synthese'
                    ? ''
                    : html`<c-menu-navigation-chapitres
                          .diagnostic="${this.rapport!.getDiagnosticActif()}"
                          .donneesPage="${this.donneesPage}"
                      ></c-menu-navigation-chapitres>`}
                <div id="coeur-page${chapitreCourant === 'synthese' ? '-synthese' : ''}">
                    ${chapitreCourant === 'synthese'
                        ? ''
                        : html`<c-fil-ariane
                              .hrefAccueil=${PAGES_PRINCIPALES.accueil.getUrl()}
                              .liens=${this.construireLiensFilAriane(chapitreCourant!, this.donneesPage)}
                          ></c-fil-ariane>`}
                    <div id="chapitre">
                        ${choose(
                            chapitreCourant,
                            [
                                ['synthese', this.renderCoeurDePageSynthese],
                                ['territoire', this.renderCoeurDePageTerritoire],
                                [IDS_MAILLONS.terresAgricoles, this.renderCoeurDePageTerresAgricoles],
                                [IDS_INDICATEURS.sauParHabitant, this.renderCoeurDePageSauParHabitant],
                                [IDS_INDICATEURS.rythmeArtificialisation, this.renderCoeurDePageRythmeArtificialisation],
                                [IDS_INDICATEURS.politiqueAmenagement, this.renderCoeurDePagePolitiqueAmenagement],
                                [IDS_INDICATEURS.partLogementsVacants, this.renderCoeurDePagePartLogementsVacants],
                                [IDS_MAILLONS.agriculteursExploitations, this.renderCoeurDePageAgriculteursExploitations],
                                [IDS_INDICATEURS.partActifsAgricoles, this.renderCoeurDePagePartActifsAgricoles],
                                [IDS_INDICATEURS.revenuAgriculteurs, this.renderCoeurDePageRevenuAgriculteurs],
                                [IDS_INDICATEURS.agesChefsExploitation, this.renderCoeurDePageAgesChefsExploitation],
                                [IDS_INDICATEURS.superficiesExploitations, this.renderCoeurDePageSuperficiesExploitations],
                                [IDS_MAILLONS.intrants, this.renderCoeurDePageIntrants],
                                [IDS_INDICATEURS.qsaNodu, this.renderCoeurDePageQsaNodu],
                                [IDS_INDICATEURS.noduNormalise, this.renderCoeurDePageNoduNormalise],
                                [IDS_MAILLONS.production, this.renderCoeurDePageProduction],
                                [
                                    IDS_INDICATEURS.adequationTheoriqueProductionConsommation,
                                    this.renderCoeurDePageAdequationTheoriqueProductionConsommation
                                ],
                                [IDS_INDICATEURS.partProductionExportee, this.renderCoeurDePagePartProductionExportee],
                                [IDS_INDICATEURS.partSAUBio, this.renderCoeurDePagePartSAUBio],
                                [IDS_INDICATEURS.scoreHVN, this.renderCoeurDePageIndiceHVN],
                                [IDS_MAILLONS.transformationDistribution, this.renderCoeurDePageTransformationDistribution],
                                [IDS_INDICATEURS.partPopulationDependanteVoiture, this.renderCoeurDePagePartPopulationDependanteVoiture],
                                [IDS_INDICATEURS.partTerritoireDependantVoiture, this.renderCoeurDePagePartTerritoireDependantVoiture],
                                [IDS_INDICATEURS.distancesPlusProchesCommerces, this.renderCoeurDePageDistancesPlusProchesCommerces],
                                [IDS_MAILLONS.consommation, this.renderCoeurDePageConsommation],
                                [
                                    IDS_INDICATEURS.partAlimentationAnimaleDansConsommation,
                                    this.renderCoeurDePagePartAlimentationAnimaleDansConsommation
                                ],
                                [IDS_INDICATEURS.tauxPauvrete, this.renderCoeurDePageTauxPauvrete],
                                [IDS_INDICATEURS.aideAlimentaire, this.renderCoeurDePageAideAlimentaire],
                                [IDS_INDICATEURS.partPopulationObese, this.renderCoeurDePagePartPopulationObese]
                            ],
                            () => ''
                        )}
                    </div>
                </div>
            `;
        } else {
            return html`<c-page-sablier></c-page-sablier>`;
        }
    }

    private construireLiensFilAriane(chapitre: string, donneesPage: DonneesPageDiagnostic): LienFilAriane[] {
        const filAriane = [
            <LienFilAriane>{
                libelle: `Diagnostic ${this.rapport?.getTerritoireActif()?.nom}`,
                href: construireUrlPageDiagnosticSynthese(donneesPage.idTerritoirePrincipal, donneesPage.idEchelleTerritoriale)
            }
        ];
        if (chapitre === 'territoire')
            filAriane.push(<LienFilAriane>{
                libelle: 'Présentation du territoire',
                href: construireUrlPageDiagnosticTerritoire(donneesPage.idTerritoirePrincipal ?? '', donneesPage.idEchelleTerritoriale)
            });
        const maillon = SA.getMaillon(chapitre) ?? SA.getIndicateur(chapitre)?.maillon;
        if (maillon)
            filAriane.push(<LienFilAriane>{
                libelle: maillon.nom,
                href: construireUrlPageDiagnosticMaillon(maillon.id, donneesPage.idTerritoirePrincipal ?? '', donneesPage.idEchelleTerritoriale)
            });
        const indicateur = SA.getIndicateur(chapitre);
        if (indicateur)
            filAriane.push(<LienFilAriane>{
                libelle: indicateur.libelle,
                href: construireUrlPageDiagnosticIndicateur(indicateur.id, donneesPage.idTerritoirePrincipal ?? '', donneesPage.idEchelleTerritoriale)
            });
        return filAriane;
    }

    private renderCoeurDePageSynthese = () =>
        importDynamique(
            import('./synthese/ChapitreSynthese.js'),
            html` <c-chapitre-synthese .rapport="${this.rapport}" .donneesPage="${this.donneesPage}"></c-chapitre-synthese>`,
            html` <c-page-sablier></c-page-sablier>`
        );

    private renderCoeurDePageTerritoire = () =>
        importDynamique(
            import('./chapitres/ChapitreTerritoire.js'),
            html` <c-chapitre-territoire .rapport="${this.rapport}" .donneesPage="${this.donneesPage}"></c-chapitre-territoire>`,
            html` <c-page-sablier></c-page-sablier>`
        );

    private renderCoeurDePageTerresAgricoles = () =>
        importDynamique(
            import('./chapitres/ChapitreTerresAgricoles.js'),
            html` <c-chapitre-terres-agricoles .rapport="${this.rapport}" .donneesPage="${this.donneesPage}"></c-chapitre-terres-agricoles>`,
            html` <c-page-sablier></c-page-sablier>`
        );

    private renderCoeurDePageSauParHabitant = () =>
        importDynamique(
            import('./chapitres/indicateurs/ChapitreSauParHabitant.js'),
            html` <c-chapitre-sau-par-habitant .rapport="${this.rapport}" .donneesPage="${this.donneesPage}"></c-chapitre-sau-par-habitant>`,
            html` <c-page-sablier></c-page-sablier>`
        );

    private renderCoeurDePageRythmeArtificialisation = () =>
        importDynamique(
            import('./chapitres/indicateurs/ChapitreRythmeArtificialisation.js'),
            html` <c-chapitre-rythme-artificialisation
                .rapport="${this.rapport}"
                .donneesPage="${this.donneesPage}"
            ></c-chapitre-rythme-artificialisation>`,
            html` <c-page-sablier></c-page-sablier>`
        );

    private renderCoeurDePagePolitiqueAmenagement = () =>
        importDynamique(
            import('./chapitres/indicateurs/ChapitrePolitiqueAmenagement.js'),
            html` <c-chapitre-politique-amenagement
                .rapport="${this.rapport}"
                .donneesPage="${this.donneesPage}"
            ></c-chapitre-politique-amenagement>`,
            html` <c-page-sablier></c-page-sablier>`
        );

    private renderCoeurDePagePartLogementsVacants = () =>
        importDynamique(
            import('./chapitres/indicateurs/ChapitrePartLogementsVacants.js'),
            html` <c-chapitre-part-logements-vacants
                .rapport="${this.rapport}"
                .donneesPage="${this.donneesPage}"
            ></c-chapitre-part-logements-vacants>`,
            html` <c-page-sablier></c-page-sablier>`
        );

    private renderCoeurDePageAgriculteursExploitations = () =>
        importDynamique(
            import('./chapitres/ChapitreAgriculteursExploitations.js'),
            html` <c-chapitre-agriculteurs-exploitations
                .rapport="${this.rapport}"
                .donneesPage="${this.donneesPage}"
            ></c-chapitre-agriculteurs-exploitations>`,
            html` <c-page-sablier></c-page-sablier>`
        );

    private renderCoeurDePagePartActifsAgricoles = () =>
        importDynamique(
            import('./chapitres/indicateurs/ChapitrePartActifsAgricoles.js'),
            html` <c-chapitre-part-actifs-agricoles
                .rapport="${this.rapport}"
                .donneesPage="${this.donneesPage}"
            ></c-chapitre-part-actifs-agricoles>`,
            html` <c-page-sablier></c-page-sablier>`
        );

    private renderCoeurDePageRevenuAgriculteurs = () =>
        importDynamique(
            import('./chapitres/indicateurs/ChapitreRevenuAgriculteurs.js'),
            html` <c-chapitre-revenu-agriculteurs .rapport="${this.rapport}" .donneesPage="${this.donneesPage}"></c-chapitre-revenu-agriculteurs>`,
            html` <c-page-sablier></c-page-sablier>`
        );

    private renderCoeurDePageAgesChefsExploitation = () =>
        importDynamique(
            import('./chapitres/indicateurs/ChapitreAgesChefsExploitation.js'),
            html` <c-chapitre-ages-chefs-exploitations
                .rapport="${this.rapport}"
                .donneesPage="${this.donneesPage}"
            ></c-chapitre-ages-chefs-exploitations>`,
            html` <c-page-sablier></c-page-sablier>`
        );

    private renderCoeurDePageSuperficiesExploitations = () =>
        importDynamique(
            import('./chapitres/indicateurs/ChapitreSuperficiesExploitations.js'),
            html` <c-chapitre-superficies-exploitations
                .rapport="${this.rapport}"
                .donneesPage="${this.donneesPage}"
            ></c-chapitre-superficies-exploitations>`,
            html` <c-page-sablier></c-page-sablier>`
        );

    private renderCoeurDePageIntrants = () =>
        importDynamique(
            import('./chapitres/ChapitreIntrants.js'),
            html` <c-chapitre-intrants .rapport="${this.rapport}" .donneesPage="${this.donneesPage}"></c-chapitre-intrants>`,
            html` <c-page-sablier></c-page-sablier>`
        );

    private renderCoeurDePageQsaNodu = () =>
        importDynamique(
            import('./chapitres/indicateurs/ChapitreQsaNodu.js'),
            html` <c-chapitre-qsa-nodu .rapport="${this.rapport}" .donneesPage="${this.donneesPage}"></c-chapitre-qsa-nodu>`,
            html` <c-page-sablier></c-page-sablier>`
        );

    private renderCoeurDePageNoduNormalise = () =>
        importDynamique(
            import('./chapitres/indicateurs/ChapitreNoduNormalise.js'),
            html` <c-chapitre-nodu-normalise .rapport="${this.rapport}" .donneesPage="${this.donneesPage}"></c-chapitre-nodu-normalise>`,
            html` <c-page-sablier></c-page-sablier>`
        );

    private renderCoeurDePageProduction = () =>
        importDynamique(
            import('./chapitres/ChapitreProduction.js'),
            html` <c-chapitre-production .rapport=${this.rapport} .donneesPage=${this.donneesPage}></c-chapitre-production>`,
            html` <c-page-sablier></c-page-sablier>`
        );

    private renderCoeurDePageAdequationTheoriqueProductionConsommation = () =>
        importDynamique(
            import('./chapitres/indicateurs/ChapitreAdequationTheoriqueProductionConsommation.js'),
            html` <c-chapitre-adequation-theorique-production-consommation
                .rapport=${this.rapport}
                .donneesPage=${this.donneesPage}
            ></c-chapitre-adequation-theorique-production-consommation>`,
            html` <c-page-sablier></c-page-sablier>`
        );

    private renderCoeurDePagePartProductionExportee = () =>
        importDynamique(
            import('./chapitres/indicateurs/ChapitrePartProductionExportee.js'),
            html` <c-chapitre-part-production-exportee
                .rapport=${this.rapport}
                .donneesPage=${this.donneesPage}
            ></c-chapitre-part-production-exportee>`,
            html` <c-page-sablier></c-page-sablier>`
        );

    private renderCoeurDePagePartSAUBio = () =>
        importDynamique(
            import('./chapitres/indicateurs/ChapitrePartSAUBio.js'),
            html` <c-chapitre-part-sau-bio .rapport=${this.rapport} .donneesPage=${this.donneesPage}></c-chapitre-part-sau-bio>`,
            html` <c-page-sablier></c-page-sablier>`
        );

    private renderCoeurDePageIndiceHVN = () =>
        importDynamique(
            import('./chapitres/indicateurs/ChapitreIndiceHVN.js'),
            html` <c-chapitre-indice-hvn .rapport=${this.rapport} .donneesPage=${this.donneesPage}></c-chapitre-indice-hvn>`,
            html` <c-page-sablier></c-page-sablier>`
        );

    private renderCoeurDePageTransformationDistribution = () =>
        importDynamique(
            import('./chapitres/ChapitreTransformationDistribution.js'),
            html` <c-chapitre-transformation-distribution
                .rapport="${this.rapport}"
                .donneesPage="${this.donneesPage}"
            ></c-chapitre-transformation-distribution>`,
            html` <c-page-sablier></c-page-sablier>`
        );

    private renderCoeurDePagePartPopulationDependanteVoiture = () =>
        importDynamique(
            import('./chapitres/indicateurs/ChapitrePartPopulationDependanteVoiture.js'),
            html` <c-chapitre-part-population-dependante-voiture
                .rapport="${this.rapport}"
                .donneesPage="${this.donneesPage}"
            ></c-chapitre-part-population-dependante-voiture>`,
            html` <c-page-sablier></c-page-sablier>`
        );

    private renderCoeurDePagePartTerritoireDependantVoiture = () =>
        importDynamique(
            import('./chapitres/indicateurs/ChapitrePartTerritoireDependantVoiture.js'),
            html` <c-chapitre-part-territoire-dependant-voiture
                .rapport="${this.rapport}"
                .donneesPage="${this.donneesPage}"
            ></c-chapitre-part-territoire-dependant-voiture>`,
            html` <c-page-sablier></c-page-sablier>`
        );

    private renderCoeurDePageDistancesPlusProchesCommerces = () =>
        importDynamique(
            import('./chapitres/indicateurs/ChapitreDistancesPlusProchesCommerces.js'),
            html` <c-chapitre-distances-plus-proches-commerces
                .rapport="${this.rapport}"
                .donneesPage="${this.donneesPage}"
            ></c-chapitre-distances-plus-proches-commerces>`,
            html` <c-page-sablier></c-page-sablier>`
        );

    private renderCoeurDePageConsommation = () =>
        importDynamique(
            import('./chapitres/ChapitreConsommation.js'),
            html` <c-chapitre-consommation .rapport=${this.rapport} .donneesPage=${this.donneesPage}></c-chapitre-consommation>`,
            html` <c-page-sablier></c-page-sablier>`
        );

    private renderCoeurDePagePartAlimentationAnimaleDansConsommation = () =>
        importDynamique(
            import('./chapitres/indicateurs/ChapitrePartAlimentationAnimaleDansConsommation.js'),
            html` <c-chapitre-part-alimentation-animale-dans-consommation
                .rapport="${this.rapport}"
                .donneesPage="${this.donneesPage}"
            ></c-chapitre-part-alimentation-animale-dans-consommation>`,
            html` <c-page-sablier></c-page-sablier>`
        );

    private renderCoeurDePageTauxPauvrete = () =>
        importDynamique(
            import('./chapitres/indicateurs/ChapitreTauxPauvrete.js'),
            html` <c-chapitre-taux-pauvrete .rapport="${this.rapport}" .donneesPage="${this.donneesPage}"></c-chapitre-taux-pauvrete>`,
            html` <c-page-sablier></c-page-sablier>`
        );

    private renderCoeurDePageAideAlimentaire = () =>
        importDynamique(
            import('./chapitres/indicateurs/ChapitreAideAlimentaire.js'),
            html` <c-chapitre-aide-alimentaire .rapport="${this.rapport}" .donneesPage=${this.donneesPage}></c-chapitre-aide-alimentaire>`,
            html` <c-page-sablier></c-page-sablier>`
        );

    private renderCoeurDePagePartPopulationObese = () =>
        importDynamique(
            import('./chapitres/indicateurs/ChapitrePartPopulationObese.js'),
            html` <c-chapitre-part-population-obese
                .rapport="${this.rapport}"
                .donneesPage="${this.donneesPage}"
            ></c-chapitre-part-population-obese>`,
            html` <c-page-sablier></c-page-sablier>`
        );
}

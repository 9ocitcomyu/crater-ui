import '../../commun/JaugeNoteSur10.js';
import '@lga/design-system/build/composants/Bouton.js';
import '@lga/design-system/build/composants/Boite.js';

import { Boite } from '@lga/design-system/build/composants/Boite.js';
import { Bouton } from '@lga/design-system/build/composants/Bouton.js';
import { ICONES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/icones-design-system.js';
import { CSS_BREAKPOINT_MAX_WIDTH_MOBILE_PAYSAGE } from '@lga/design-system/build/styles/styles-breakpoints.js';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { css, html, LitElement, unsafeCSS } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { unsafeHTML } from 'lit/directives/unsafe-html.js';

import { Note, NOTE_VALEUR_WARNING } from '../../../domaines/diagnostics';
import { construireUrlPageMethodologie } from '../../../domaines/pages/pages-utils';
import { stylesIndicateurNotes } from '../../../domaines/systeme-alimentaire/configuration/indicateurs.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-synthese-maillon': SyntheseMaillon;
    }
}

export interface OptionsSyntheseMaillon {
    note: Note | typeof NOTE_VALEUR_WARNING;
    messageSynthese: string;
    idMaillon: string;
    lienCarte?: string;
}

@customElement('c-synthese-maillon')
export class SyntheseMaillon extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        css`
            :host {
                display: block;
                max-width: 800px;
                padding-left: calc(2 * var(--dsem));
                padding-right: calc(2 * var(--dsem));
            }

            main {
                display: flex;
                flex-direction: row;
                align-items: center;
                justify-content: center;
                padding: calc(4 * var(--dsem));
                text-align: left;
            }

            c-jauge-note-sur-10 {
                width: calc(12 * var(--dsem));
                flex-shrink: 0;
            }

            .message {
                margin: auto;
                color: var(--couleur-neutre);
                padding-left: var(--dsem);
                margin-left: var(--dsem);
            }

            #trait-separation {
                height: 1px;
                width: 80%;
                background: var(--couleur-neutre-20);
                margin: auto;
                display: var(--displayPDF, block);
            }

            footer {
                display: var(--displayPDF, flex);
                justify-content: space-evenly;
                padding: var(--dsem);
            }

            @media screen and (max-width: ${unsafeCSS(CSS_BREAKPOINT_MAX_WIDTH_MOBILE_PAYSAGE + 'px')}) {
                :host {
                    padding: 0;
                }

                main {
                    flex-direction: column;
                }

                .message {
                    padding: 0;
                    margin: 0;
                    padding-top: calc(2 * var(--dsem));
                }

                footer {
                    flex-direction: column;
                    padding-left: calc(3 * var(--dsem));
                }
            }
        `
    ];

    @property({ attribute: false })
    options?: OptionsSyntheseMaillon;

    render() {
        if (!this.options) {
            return html``;
        }
        const couleur =
            this.options.note === NOTE_VALEUR_WARNING
                ? stylesIndicateurNotes.calculerCouleur(5)
                : stylesIndicateurNotes.calculerCouleur(this.options!.note);

        return html`
            <c-boite encadrement arrondi=${Boite.ARRONDI.large}>
                <main>
                    <c-jauge-note-sur-10 note=${this.options.note!} couleur=${couleur}></c-jauge-note-sur-10>
                    <p class="message texte-moyen">${unsafeHTML(this.options.messageSynthese)}</p>
                </main>
                <div id="trait-separation"></div>
                <footer>
                    ${this.options.lienCarte
                        ? html`<c-bouton
                              href=${this.options.lienCarte}
                              libelle="Voir la carte"
                              libelleCourt="Carte"
                              libelleTooltip="Voir la carte"
                              type="${Bouton.TYPE.plat}"
                          >
                              ${ICONES_DESIGN_SYSTEM.flecheDroite}
                          </c-bouton>`
                        : ``}
                    <c-bouton
                        href="${construireUrlPageMethodologie(this.options.idMaillon, `evaluation-globale-${this.options.idMaillon}`)}"
                        libelle="Voir la méthodologie"
                        libelleCourt="Méthodologie"
                        libelleTooltip="Voir la méthodologie"
                        type="${Bouton.TYPE.plat}"
                    >
                        ${ICONES_DESIGN_SYSTEM.flecheDroite}
                    </c-bouton>
                </footer>
            </c-boite>
        `;
    }
}

import '../../charts/ChartBarres.js';
import '../composants/IndicateurDetaille.js';
import '../../../commun/PageSablier.js';

import { html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import { SEUIL_LABELLISATION_HVN } from '../../../../domaines/diagnostics/diagnostics/production/MessageHvn.js';
import { EtatRapport, Rapport } from '../../../../domaines/diagnostics/Rapport.js';
import { DonneesPageDiagnostic } from '../../../../domaines/pages/donnees-pages';
import { IDS_INDICATEURS } from '../../../../domaines/systeme-alimentaire/configuration/indicateurs.js';
import { SA } from '../../../../domaines/systeme-alimentaire/configuration/systeme-alimentaire.js';
import { Serie, SerieMultiple } from '../../charts/chart-outils.js';
import { OptionsChartBarres } from '../../charts/ChartBarres.js';
import { OptionsIndicateurDetaille } from '../composants/IndicateurDetaille.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-chapitre-indice-hvn': ChapitreIndiceHVN;
    }
}

@customElement('c-chapitre-indice-hvn')
export class ChapitreIndiceHVN extends LitElement {
    @property({ attribute: false })
    donneesPage?: DonneesPageDiagnostic;

    @property({ attribute: false })
    rapport?: Rapport;

    render() {
        if (this.rapport?.getEtat() === EtatRapport.PRET) {
            return html`<c-indicateur-detaille
                id="indicateur-detaille-${IDS_INDICATEURS.scoreHVN}"
                .options=${this.calculerOptionsIndicateurDetailleHVN(this.rapport, this.donneesPage)}
                nbLignesMessageMinimum="4"
            >
                <c-chart-barres .options=${this.calculerOptionsChartHvn(this.rapport)} slot="chart-indicateur-detaille"></c-chart-barres>
            </c-indicateur-detaille>`;
        } else {
            return html`<c-page-sablier></c-page-sablier>`;
        }
    }

    private calculerOptionsIndicateurDetailleHVN(rapport: Rapport, donneesPage: DonneesPageDiagnostic | undefined): OptionsIndicateurDetaille {
        return {
            indicateur: SA.getIndicateur(IDS_INDICATEURS.scoreHVN)!,
            message: rapport.getDiagnosticActif()!.production.messageHvn,
            donneesPage: donneesPage,
            echelleEstDisponibleDansCarte: rapport.echelleEstDisponibleDansCarte()
        };
    }
    private calculerOptionsChartHvn(rapport: Rapport): OptionsChartBarres {
        return {
            series: new SerieMultiple(
                [
                    new Serie(
                        rapport.getListeDiagnostics().map((d) => d!.production.indicateurHvn.indice1),
                        '1 : diversité des assolements'
                    ),
                    new Serie(
                        rapport.getListeDiagnostics().map((d) => d!.production.indicateurHvn.indice2),
                        '2 : pratiques agricoles durables'
                    ),
                    new Serie(
                        rapport.getListeDiagnostics().map((d) => d!.production.indicateurHvn.indice3),
                        "3 : infrastructures d'intérêt écologique"
                    )
                ],
                rapport.getListeDiagnostics().map((d) => d!.territoire.nom)
            ),
            nomAxeOrdonnees: 'Score HVN (/30)',
            empile: true,
            minY: 0,
            maxY: 30,
            annotations: {
                yaxis: [
                    {
                        y: SEUIL_LABELLISATION_HVN,
                        borderColor: 'var(--couleur-neutre)',
                        strokeDashArray: 5,
                        label: {
                            style: {
                                color: 'var(--blanc)',
                                background: 'var(--couleur-neutre-20)'
                            },
                            text: 'Seuil de labellisation HVN'
                        }
                    }
                ]
            }
        };
    }
}

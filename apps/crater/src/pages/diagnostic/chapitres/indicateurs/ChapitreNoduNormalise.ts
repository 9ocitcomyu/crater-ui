import '../../charts/ChartLignes.js';
import '../composants/IndicateurDetaille.js';
import '../../../commun/PageSablier.js';

import { html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import { EtatRapport, Rapport } from '../../../../domaines/diagnostics/Rapport.js';
import { PAGES_METHODOLOGIE } from '../../../../domaines/pages/configuration/declaration-pages.js';
import { DonneesPageDiagnostic } from '../../../../domaines/pages/donnees-pages.js';
import { IDS_INDICATEURS } from '../../../../domaines/systeme-alimentaire/configuration/indicateurs.js';
import { SA } from '../../../../domaines/systeme-alimentaire/configuration/systeme-alimentaire.js';
import { getVariableCss, Serie, SerieMultiple } from '../../charts/chart-outils.js';
import { OptionsChartLignes } from '../../charts/ChartLignes.js';
import { OptionsIndicateurDetaille } from '../composants/IndicateurDetaille.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-chapitre-nodu-normalise': ChapitreNoduNormalise;
    }
}

@customElement('c-chapitre-nodu-normalise')
export class ChapitreNoduNormalise extends LitElement {
    @property({ attribute: false })
    donneesPage?: DonneesPageDiagnostic;

    @property({ attribute: false })
    rapport?: Rapport;

    render() {
        if (this.rapport?.getEtat() === EtatRapport.PRET) {
            return html`<c-indicateur-detaille
                id="indicateur-detaille-${IDS_INDICATEURS.noduNormalise}"
                .options=${this.calculerOptionsIndicateurDetailleNoduNormalise(this.rapport, this.donneesPage)}
                nbLignesMessageMinimum="0"
            >
                <c-chart-lignes .options=${this.calculerOptionsChartNoduNormalise(this.rapport)} slot="chart-indicateur-detaille"></c-chart-lignes>
            </c-indicateur-detaille>`;
        } else {
            return html`<c-page-sablier></c-page-sablier>`;
        }
    }

    private calculerOptionsIndicateurDetailleNoduNormalise(
        rapport: Rapport,
        donneesPage: DonneesPageDiagnostic | undefined
    ): OptionsIndicateurDetaille {
        return {
            indicateur: SA.getIndicateur(IDS_INDICATEURS.noduNormalise)!,
            message: `Cet indicateur correspond au ratio entre le <c-lien href=${PAGES_METHODOLOGIE.intrants.getUrl()}#${
                IDS_INDICATEURS.noduNormalise
            }>NODU</c-lien> et la surface agricole du territoire. Il peut s’interpréter comme le nombre moyen de traitements de pesticides utilisés à leur dosage maximal autorisé que reçoivent les terres agricoles du territoire.`,
            donneesPage: donneesPage,
            echelleEstDisponibleDansCarte: rapport.echelleEstDisponibleDansCarte()
        };
    }

    private calculerOptionsChartNoduNormalise(rapport: Rapport): OptionsChartLignes {
        return {
            series: new SerieMultiple(
                rapport.getListeDiagnostics().map((d) => {
                    return new Serie(d!.intrants.pesticides.indicateursMoyennesTriennales.nodusNormalises, d!.territoire.nom);
                })
            ),
            xaxisCategories: rapport.getDiagnosticPays()?.intrants.pesticides.indicateursMoyennesTriennales.annees ?? [],
            yaxisTitre: 'NODU normalisé [-]',
            couleurs: [
                getVariableCss('--c-ligne-serie-2'),
                getVariableCss('--c-ligne-serie-3'),
                getVariableCss('--c-ligne-serie-4'),
                getVariableCss('--c-ligne-serie-5')
            ]
        };
    }
}

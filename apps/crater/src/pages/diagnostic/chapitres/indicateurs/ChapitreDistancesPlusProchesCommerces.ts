import '../../charts/ChartBarres.js';
import '../composants/IndicateurDetaille.js';
import '../../../commun/PageSablier.js';
import '../../charts/ChartDistancesAuxCommerces.js';

import { html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import { EtatRapport, Rapport } from '../../../../domaines/diagnostics/Rapport.js';
import { DonneesPageDiagnostic } from '../../../../domaines/pages/donnees-pages';
import { IDS_INDICATEURS } from '../../../../domaines/systeme-alimentaire/configuration/indicateurs.js';
import { SA } from '../../../../domaines/systeme-alimentaire/configuration/systeme-alimentaire.js';
import { OptionsIndicateurDetaille } from '../composants/IndicateurDetaille.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-chapitre-distances-plus-proches-commerces': ChapitreDistancesPlusProchesCommerces;
    }
}

@customElement('c-chapitre-distances-plus-proches-commerces')
export class ChapitreDistancesPlusProchesCommerces extends LitElement {
    @property({ attribute: false })
    donneesPage?: DonneesPageDiagnostic;

    @property({ attribute: false })
    rapport?: Rapport;

    render() {
        if (this.rapport?.getEtat() === EtatRapport.PRET) {
            return html`<c-indicateur-detaille
                id="indicateur-detaille-${IDS_INDICATEURS.distancesPlusProchesCommerces}"
                .options=${this.calculerOptionsIndicateurDetailleDistancesAuxCommerces(this.rapport, this.donneesPage)}
            >
                <c-chart-distances-plus-proches-commerces
                    .indicateurs=${this.rapport.getDiagnosticActif()?.transformationDistribution.indicateursProximiteCommercesParTypesCommerces}
                    slot="chart-indicateur-detaille"
                ></c-chart-distances-plus-proches-commerces>
            </c-indicateur-detaille>`;
        } else {
            return html`<c-page-sablier></c-page-sablier>`;
        }
    }

    private calculerOptionsIndicateurDetailleDistancesAuxCommerces(
        rapport: Rapport,
        donneesPage: DonneesPageDiagnostic | undefined
    ): OptionsIndicateurDetaille {
        return {
            indicateur: SA.getIndicateur(IDS_INDICATEURS.distancesPlusProchesCommerces)!,
            donneesPage: donneesPage,
            echelleEstDisponibleDansCarte: rapport.echelleEstDisponibleDansCarte()
        };
    }
}

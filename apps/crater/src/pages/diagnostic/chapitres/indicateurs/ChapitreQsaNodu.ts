import '../../charts/ChartLignesDeuxAxesY.js';
import '../composants/IndicateurDetaille.js';
import '../../../commun/PageSablier.js';

import { html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import { EtatRapport, Rapport } from '../../../../domaines/diagnostics/Rapport.js';
import { DonneesPageDiagnostic } from '../../../../domaines/pages/donnees-pages.js';
import { IDS_INDICATEURS } from '../../../../domaines/systeme-alimentaire/configuration/indicateurs.js';
import { SA } from '../../../../domaines/systeme-alimentaire/configuration/systeme-alimentaire.js';
import { getVariableCss, Serie, SerieMultiple } from '../../charts/chart-outils.js';
import { OptionsChartLignesDeuxAxesY } from '../../charts/ChartLignesDeuxAxesY.js';
import { OptionsIndicateurDetaille } from '../composants/IndicateurDetaille.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-chapitre-qsa-nodu': ChapitreQsaNodu;
    }
}

@customElement('c-chapitre-qsa-nodu')
export class ChapitreQsaNodu extends LitElement {
    @property({ attribute: false })
    donneesPage?: DonneesPageDiagnostic;

    @property({ attribute: false })
    rapport?: Rapport;

    render() {
        if (this.rapport?.getEtat() === EtatRapport.PRET) {
            return html` <c-indicateur-detaille
                id="indicateur-detaille-${IDS_INDICATEURS.qsaNodu}"
                .options=${this.calculerOptionsIndicateurDetailleQsaNodu(this.rapport, this.donneesPage)}
                nbLignesMessageMinimum="5"
            >
                <c-chart-lignes-2-axes-y
                    .options=${this.calculerOptionsChartQsaNodu(this.rapport)}
                    slot="chart-indicateur-detaille"
                ></c-chart-lignes-2-axes-y>
            </c-indicateur-detaille>`;
        } else {
            return html`<c-page-sablier></c-page-sablier>`;
        }
    }

    private calculerOptionsIndicateurDetailleQsaNodu(rapport: Rapport, donneesPage: DonneesPageDiagnostic | undefined): OptionsIndicateurDetaille {
        return {
            indicateur: SA.getIndicateur(IDS_INDICATEURS.qsaNodu)!,
            message: rapport.getDiagnosticActif()!.intrants.messagePesticides,
            donneesPage: donneesPage,
            echelleEstDisponibleDansCarte: rapport.echelleEstDisponibleDansCarte()
        };
    }

    private calculerOptionsChartQsaNodu(rapport: Rapport): OptionsChartLignesDeuxAxesY {
        return {
            series: new SerieMultiple([
                new Serie(
                    rapport.getDiagnosticActif()!.intrants.pesticides.indicateursMoyennesTriennales.quantitesSubstancesTotalesKg,
                    'Quantité Substance Active (QSA) totale [kg]'
                ),
                new Serie(
                    rapport.getDiagnosticActif()!.intrants.pesticides.indicateursMoyennesTriennales.quantitesSubstancesAvecDUKg,
                    'Quantité Substance Active (QSA) avec Doses Unités [kg]'
                ),
                new Serie(
                    rapport.getDiagnosticActif()!.intrants.pesticides.indicateursMoyennesTriennales.nodusHa,
                    'Nombre de doses unités (NODU) [ha]'
                )
            ]),
            categoriesXaxis: [2017, 2018, 2019, 2020],
            nomsSeriesYaxis: [
                'Quantité Substance Active (QSA) totale [kg]',
                'Quantité Substance Active (QSA) totale [kg]',
                'Nombre de doses unités (NODU) [ha]'
            ],
            titreYaxisGauche: 'Quantité Substance Active [kg]',
            titreYaxisDroite: 'Nombre de Doses Unités [ha]',
            couleurYaxisGauche: getVariableCss('--c-ligne-serie-1'),
            couleurYaxisGaucheBis: getVariableCss('--c-ligne-serie-1-bis'),
            couleurYaxisDroite: getVariableCss('--c-ligne-serie-2')
        };
    }
}

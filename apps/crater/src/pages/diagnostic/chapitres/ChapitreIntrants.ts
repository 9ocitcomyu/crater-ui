import '../../commun/TimeLine.js';
import '../charts/ChartLignes';
import '../charts/ChartLignesDeuxAxesY';
import './composants/CommentAgir.js';
import './composants/IndicateurDetaille.js';
import './composants/IndicateurResume.js';
import './composants/PourquoiCestImportant.js';
import './composants/VoirEgalement.js';
import './SyntheseMaillon.js';
import '@lga/design-system/build/composants/Lien.js';
import '@lga/design-system/build/composants/EncartPagesPrecedenteEtSuivante.js';

import { formaterNombreEnNDecimalesString } from '@lga/commun/build/outils/base';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { ifDefined } from 'lit/directives/if-defined.js';

import { Rapport } from '../../../domaines/diagnostics';
import { DonneesPageDiagnostic } from '../../../domaines/pages/donnees-pages';
import { construireUrlPageDiagnosticMaillon } from '../../../domaines/pages/pages-utils';
import { IDS_INDICATEURS } from '../../../domaines/systeme-alimentaire/configuration/indicateurs.js';
import { IDS_MAILLONS } from '../../../domaines/systeme-alimentaire/configuration/maillons';
import { SA } from '../../../domaines/systeme-alimentaire/configuration/systeme-alimentaire';
import { STYLES_CRATER } from '../../commun/pages-styles';
import { STYLES_CHAPITRES_DIAGNOSTIC } from '../diagnostic-utils.js';
import { creerOptionsSyntheseMaillon } from './chapitres-outils';
import { OptionsIndicateurResume } from './composants/IndicateurResume';

declare global {
    interface HTMLElementTagNameMap {
        'c-chapitre-intrants': ChapitreIntrants;
    }
}

@customElement('c-chapitre-intrants')
export class ChapitreIntrants extends LitElement {
    static styles = [STYLES_DESIGN_SYSTEM, STYLES_CRATER, STYLES_CHAPITRES_DIAGNOSTIC];

    @property({ attribute: false })
    donneesPage?: DonneesPageDiagnostic;

    @property({ attribute: false })
    rapport?: Rapport;

    @property({ type: Boolean })
    affichagePDF? = false;

    private readonly maillon = SA.getMaillon(IDS_MAILLONS.intrants);

    render() {
        if (!this.rapport) return;

        return html`
            <section class="${this.affichagePDF ? 'PDF' : ''}">
                <h1>${this.maillon?.nom}</h1>

                <c-synthese-maillon
                    .options=${creerOptionsSyntheseMaillon(this.rapport, this.rapport.getDiagnosticActif()!.intrants, IDS_MAILLONS.intrants)}
                >
                </c-synthese-maillon>

                <h2 id="pourquoi-cest-important">Pourquoi c'est important ?</h2>
                <c-pourquoi-cest-important .maillon=${this.maillon}></c-pourquoi-cest-important>

                <h2 id="etat-lieux">Quel est l'état des lieux ?</h2>
                <div class="indicateurs-resumes">
                    <p>
                        <span>Quel usage de pesticides ?</span>
                    </p>
                    <c-timeline></c-timeline>
                    <div class="bloc-indicateurs-resumes">
                        <c-indicateur-resume
                            .options=${this.calculerOptionsIndicateurResumeQsaNodu(this.rapport, this.donneesPage)}
                            id="indicateur-resume-${IDS_INDICATEURS.qsaNodu}"
                        ></c-indicateur-resume>
                        <c-indicateur-resume
                            .options=${this.calculerOptionsIndicateurResumeNoduNormalise(this.rapport, this.donneesPage)}
                            id="indicateur-resume-${IDS_INDICATEURS.noduNormalise}"
                        ></c-indicateur-resume>
                    </div>
                </div>

                <c-voir-egalement .liensIndicateurs=${this.maillon!.indicateursAutresMaillons} .donneesPage=${this.donneesPage}></c-voir-egalement>

                <h2 id="leviers-action">Comment ma collectivité peut-elle s&#39;améliorer ?</h2>
                <c-comment-agir
                    .maillon=${this.maillon}
                    idTerritoireParcel=${ifDefined(this.rapport.getDiagnosticActif()?.idTerritoireParcel || undefined)}
                ></c-comment-agir>

                <c-encart-pages-precedente-suivante
                    libellePagePrecedente=${ifDefined(SA.getMaillon(IDS_MAILLONS.agriculteursExploitations)?.nom)}
                    hrefPagePrecedente=${construireUrlPageDiagnosticMaillon(
                        IDS_MAILLONS.agriculteursExploitations,
                        this.donneesPage?.idTerritoirePrincipal ?? '',
                        this.donneesPage?.idEchelleTerritoriale
                    )}
                    libellePageSuivante=${ifDefined(SA.getMaillon(IDS_MAILLONS.production)?.nom)}
                    hrefPageSuivante=${construireUrlPageDiagnosticMaillon(
                        IDS_MAILLONS.production,
                        this.donneesPage?.idTerritoirePrincipal ?? '',
                        this.donneesPage?.idEchelleTerritoriale
                    )}
                ></c-encart-pages-precedente-suivante>
            </section>
        `;
    }

    private calculerOptionsIndicateurResumeQsaNodu(rapport: Rapport, donneesPage: DonneesPageDiagnostic | undefined): OptionsIndicateurResume {
        return {
            indicateur: SA.getIndicateur(IDS_INDICATEURS.qsaNodu)!,
            chiffresCles: [
                {
                    chiffre: rapport.getDiagnosticActif()!.intrants.qsaTotalAnneeFinale!,
                    libelleApresChiffre: 'de substances actives achetées'
                },
                {
                    chiffre: `${rapport.getDiagnosticActif()!.intrants.evolutionNodu! > 0 ? `+` : ``}${formaterNombreEnNDecimalesString(
                        rapport.getDiagnosticActif()!.intrants.evolutionNodu,
                        0
                    )} %`,
                    libelleApresChiffre: `de doses unités utilisées par an entre ${
                        rapport.getDiagnosticActif()!.intrants.pesticides.indicateursMoyennesTriennales.anneeInitiale.annee
                    } et ${rapport.getDiagnosticActif()!.intrants.pesticides.indicateursMoyennesTriennales.anneeFinale.annee}`
                }
            ],
            donneesPage: donneesPage
        };
    }
    private calculerOptionsIndicateurResumeNoduNormalise(rapport: Rapport, donneesPage: DonneesPageDiagnostic | undefined): OptionsIndicateurResume {
        return {
            indicateur: SA.getIndicateur(IDS_INDICATEURS.noduNormalise)!,
            chiffresCles: [
                {
                    chiffre: formaterNombreEnNDecimalesString(
                        rapport.getDiagnosticActif()!.intrants.pesticides.indicateursMoyennesTriennales.anneeFinale.noduNormalise,
                        1
                    ),
                    libelleApresChiffre: 'fois la dose annuelle maximale autorisée pour une substance donnée'
                }
            ],
            donneesPage: donneesPage
        };
    }
}

import '@lga/design-system/build/composants/Bouton.js';
import '@lga/design-system/build/composants/Lien.js';

import { Bouton } from '@lga/design-system/build/composants/Bouton.js';
import { ICONES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/icones-design-system';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system';
import { css, html, LitElement } from 'lit';
import { customElement, property, state } from 'lit/decorators.js';
import { unsafeHTML } from 'lit/directives/unsafe-html.js';
import { unsafeSVG } from 'lit/directives/unsafe-svg.js';

import { construireUrlPageMethodologie } from '../../../../domaines/pages/pages-utils';
import { Indicateur } from '../../../../domaines/systeme-alimentaire';
import { ICONES_SVG } from '../../../../domaines/systeme-alimentaire/configuration/icones_svg';
import { STYLES_CRATER } from '../../../commun/pages-styles';

declare global {
    interface HTMLElementTagNameMap {
        'c-titre-indicateur': TitreIndicateur;
    }
}

@customElement('c-titre-indicateur')
export class TitreIndicateur extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        STYLES_CRATER,
        css`
            .titre {
                display: flex;
                margin-top: calc(2 * var(--dsem));
                margin-bottom: calc(2 * var(--dsem));
                align-items: center;
            }

            h2 {
                margin: 0;
            }

            p {
                margin-top: 0;
                margin-bottom: 0;
            }

            .texte {
                color: var(--couleur-neutre);
                padding-right: var(--dsem);
            }

            .details {
                display: block;
                position: absolute;
                background-color: var(--couleur-primaire);
                border-radius: calc(1 * var(--dsem));
                padding: calc(1 * var(--dsem));
                z-index: 100;
                margin-right: var(--dsem);
            }

            .details * {
                color: white;
            }

            .details div {
                padding-bottom: 1rem;
            }

            .details a:hover {
                text-decoration: underline;
                cursor: pointer;
            }

            #lien-methodologie {
                margin-top: calc(2 * var(--dsem));
                margin-bottom: 0;
            }

            c-lien {
                --couleur: var(--couleur-blanc);
            }
        `
    ];

    @property({ attribute: false })
    indicateur?: Indicateur;

    @property()
    lienCarte?: string;

    @state()
    visible = false;

    @property({ type: Boolean })
    masquerLienMethodologie = false;

    render() {
        const titreHTML = html` <div class="titre">
            <h2 class="texte">${this.indicateur?.libelle}</h2>
            <c-bouton @click="${this.ouvrir}" libelleTooltip="Plus d'infos sur cet indicateur" type="${Bouton.TYPE.plat}"
                >${ICONES_DESIGN_SYSTEM.question}</c-bouton
            >
            ${this.lienCarte
                ? html`<c-bouton href=${this.lienCarte} libelleTooltip="Voir la carte" type="${Bouton.TYPE.plat}">
                      ${unsafeSVG(ICONES_SVG.carte)}
                  </c-bouton>`
                : ''}
        </div>`;
        const detailHTML = this.visible
            ? html` <div class="details">
                  <p>${unsafeHTML(this.indicateur?.description)}</p>
                  ${!this.masquerLienMethodologie
                      ? html`<p id="lien-methodologie">
                            ${this.indicateur?.id
                                ? html`<c-lien href=${construireUrlPageMethodologie(this.indicateur?.maillon.id, this.indicateur?.id)}
                                      >→ voir la méthodologie</c-lien
                                  >`
                                : ``}
                        </p>`
                      : ''}
              </div>`
            : ``;
        return html`<div>${titreHTML} ${detailHTML}</div>`;
    }

    private ouvrir() {
        this.visible = true;
        window.addEventListener('click', this.gererClickFermeture, { capture: true });
    }

    private fermer() {
        this.visible = false;
        window.removeEventListener('click', this.gererClickFermeture, { capture: true });
    }

    private gererClickFermeture = (event: Event) => {
        if (!this.contains(event.target as Node)) {
            this.fermer();
        }
    };
}

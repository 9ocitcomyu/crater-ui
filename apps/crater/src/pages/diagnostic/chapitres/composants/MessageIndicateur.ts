import { arrondirANDecimales } from '@lga/commun/build/outils/base';
import { CSS_BREAKPOINT_MAX_WIDTH_TABLETTE } from '@lga/design-system/build/styles/styles-breakpoints.js';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system';
import { css, html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { ifDefined } from 'lit/directives/if-defined.js';
import { unsafeHTML } from 'lit/directives/unsafe-html.js';

import { STYLES_CRATER } from '../../../commun/pages-styles';

declare global {
    interface HTMLElementTagNameMap {
        'c-message-indicateur': MessageIndicateur;
    }
}

@customElement('c-message-indicateur')
export class MessageIndicateur extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        STYLES_CRATER,
        css`
            div {
                margin-bottom: 1.5rem;
            }
            ::slotted(p),
            p {
                margin: 0;
            }
            ::slotted(em),
            em {
                color: var(--couleur-accent);
                font-weight: bold;
                font-style: normal;
            }
            ::slotted(strong),
            strong {
                font-weight: bold;
            }
        `
    ];

    @property({ type: Number })
    nbLignesMinimum = 0;

    @property()
    message?: string;

    render() {
        const hauteurLigne = parseInt(window.getComputedStyle(this).lineHeight);
        let hauteur;
        if (window.innerWidth > CSS_BREAKPOINT_MAX_WIDTH_TABLETTE) {
            hauteur = arrondirANDecimales(this.nbLignesMinimum! * hauteurLigne, 0);
        }
        return html`
            <div style="min-height:${ifDefined(hauteur)}px">
                <slot name="message"></slot>
                ${this.message ? html`<p>${unsafeHTML(this.message)}</p>` : ''}
            </div>
        `;
    }
}

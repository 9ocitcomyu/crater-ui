import '../../commun/TimeLine.js';
import '../charts/ChartBarres.js';
import '../charts/ChartTreemapCultures.js';
import '../charts/ChartTreemapDoubleProductionBesoins.js';
import './composants/CommentAgir.js';
import './composants/IndicateurDetaille.js';
import './composants/IndicateurResume.js';
import './composants/PourquoiCestImportant.js';
import './composants/VoirEgalement.js';
import './SyntheseMaillon.js';
import '@lga/design-system/build/composants/EncartPagesPrecedenteEtSuivante.js';

import { formaterNombreEnEntierString } from '@lga/commun/build/outils/base';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { ifDefined } from 'lit/directives/if-defined.js';

import { Rapport } from '../../../domaines/diagnostics';
import { DonneesPageDiagnostic } from '../../../domaines/pages/donnees-pages.js';
import { construireUrlPageDiagnosticMaillon } from '../../../domaines/pages/pages-utils';
import { IDS_INDICATEURS } from '../../../domaines/systeme-alimentaire/configuration/indicateurs';
import { IDS_MAILLONS } from '../../../domaines/systeme-alimentaire/configuration/maillons';
import { SA } from '../../../domaines/systeme-alimentaire/configuration/systeme-alimentaire';
import { STYLES_CRATER } from '../../commun/pages-styles';
import { STYLES_CHAPITRES_DIAGNOSTIC } from '../diagnostic-utils.js';
import { creerOptionsSyntheseMaillon } from './chapitres-outils.js';
import { OptionsIndicateurResume } from './composants/IndicateurResume.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-chapitre-production': ChapitreProduction;
    }
}

@customElement('c-chapitre-production')
export class ChapitreProduction extends LitElement {
    static styles = [STYLES_DESIGN_SYSTEM, STYLES_CRATER, STYLES_CHAPITRES_DIAGNOSTIC];

    @property({ attribute: false })
    donneesPage?: DonneesPageDiagnostic;

    @property({ attribute: false })
    rapport?: Rapport;

    @property({ type: Boolean })
    affichagePDF? = false;

    private readonly maillon = SA.getMaillon(IDS_MAILLONS.production);

    render() {
        if (!this.rapport) return;

        return html`
            <section class="${this.affichagePDF ? 'PDF' : ''}">
                <h1>${this.maillon?.nom}</h1>
                <c-synthese-maillon
                    .options=${creerOptionsSyntheseMaillon(
                        this.rapport,
                        this.rapport!.getDiagnosticActif()!.production,
                        IDS_MAILLONS.production,
                        IDS_INDICATEURS.noteProduction
                    )}
                >
                </c-synthese-maillon>
                <h2 id="pourquoi-cest-important">Pourquoi c'est important ?</h2>
                <c-pourquoi-cest-important .maillon=${this.maillon}></c-pourquoi-cest-important>

                <h2 id="etat-lieux">Quel est l'état des lieux ?</h2>
                <div class="indicateurs-resumes">
                    <p>La production du territoire pourrait-elle théoriquement couvrir la consommation des habitants ?</p>
                    <c-timeline></c-timeline>
                    <div class="bloc-indicateurs-resumes">
                        <c-indicateur-resume
                            .options=${this.calculerOptionsIndicateurResumeAdequationTheoriqueProductionConsommation(this.rapport, this.donneesPage)}
                            id="indicateur-resume-${IDS_INDICATEURS.adequationTheoriqueProductionConsommation}"
                        ></c-indicateur-resume>
                    </div>
                    <p>Quelle part de la production est réellement consommée localement ?</p>
                    <c-timeline></c-timeline>
                    <div class="bloc-indicateurs-resumes">
                        <c-indicateur-resume
                            .options=${this.calculerOptionsIndicateurResumePartProductionExportee(this.donneesPage)}
                            id="indicateur-resume-part-production-exportee"
                        ></c-indicateur-resume>
                    </div>
                    <p>Est ce que les pratiques agricoles sont respectueuses de la biodiversité ?</p>
                    <c-timeline></c-timeline>
                    <div class="bloc-indicateurs-resumes">
                        <c-indicateur-resume
                            .options=${this.calculerOptionsIndicateurResumePartSauBio(this.rapport, this.donneesPage)}
                            id="indicateur-resume-${IDS_INDICATEURS.partSAUBio}"
                        ></c-indicateur-resume>
                        <c-indicateur-resume
                            .options=${this.calculerOptionsIndicateurResumeIndiceHvn(this.rapport, this.donneesPage)}
                            id="indicateur-resume-${IDS_INDICATEURS.scoreHVN}"
                        ></c-indicateur-resume>
                    </div>
                </div>

                <c-voir-egalement .liensIndicateurs=${this.maillon!.indicateursAutresMaillons} .donneesPage=${this.donneesPage}></c-voir-egalement>

                <h2 id="leviers-action">Comment ma collectivité peut-elle s&#39;améliorer ?</h2>
                <c-comment-agir
                    .maillon=${this.maillon}
                    idTerritoireParcel=${ifDefined(this.rapport.getDiagnosticActif()?.idTerritoireParcel || undefined)}
                ></c-comment-agir>

                <c-encart-pages-precedente-suivante
                    libellePagePrecedente=${ifDefined(SA.getMaillon(IDS_MAILLONS.intrants)?.nom)}
                    hrefPagePrecedente=${construireUrlPageDiagnosticMaillon(
                        IDS_MAILLONS.intrants,
                        this.donneesPage?.idTerritoirePrincipal ?? '',
                        this.donneesPage?.idEchelleTerritoriale
                    )}
                    libellePageSuivante=${ifDefined(SA.getMaillon(IDS_MAILLONS.transformationDistribution)?.nom)}
                    hrefPageSuivante=${construireUrlPageDiagnosticMaillon(
                        IDS_MAILLONS.transformationDistribution,
                        this.donneesPage?.idTerritoirePrincipal ?? '',
                        this.donneesPage?.idEchelleTerritoriale
                    )}
                ></c-encart-pages-precedente-suivante>
            </section>
        `;
    }

    private calculerOptionsIndicateurResumeAdequationTheoriqueProductionConsommation(
        rapport: Rapport,
        donneesPage: DonneesPageDiagnostic | undefined
    ): OptionsIndicateurResume {
        return {
            indicateur: SA.getIndicateur(IDS_INDICATEURS.adequationTheoriqueProductionConsommation)!,
            chiffresCles: [
                {
                    chiffre: formaterNombreEnEntierString(rapport.getDiagnosticActif()!.production.tauxAdequationMoyenPonderePourcent!) + ' %',
                    libelleApresChiffre: 'de la consommation actuelle pourrait en théorie être couverte par la production locale'
                }
            ],
            donneesPage: donneesPage
        };
    }

    private calculerOptionsIndicateurResumePartProductionExportee(donneesPage: DonneesPageDiagnostic | undefined): OptionsIndicateurResume {
        return {
            indicateur: SA.getIndicateur(IDS_INDICATEURS.partProductionExportee)!,
            chiffresCles: [
                {
                    libelleAvantChiffre: "à l'échelle d'un bassin de vie",
                    chiffre: 'plus de 90 %',
                    libelleApresChiffre: 'de la production est exportée'
                },
                {
                    libelleAvantChiffre: 'et dans le même temps',
                    chiffre: 'plus de 90 %',
                    libelleApresChiffre: 'des produits consommés sont importés'
                }
            ],
            donneesPage: donneesPage
        };
    }

    private calculerOptionsIndicateurResumePartSauBio(rapport: Rapport, donneesPage: DonneesPageDiagnostic | undefined): OptionsIndicateurResume {
        return {
            indicateur: SA.getIndicateur(IDS_INDICATEURS.partSAUBio)!,
            chiffresCles: [
                {
                    libelleAvantChiffre: '&nbsp',
                    chiffre: formaterNombreEnEntierString(rapport.getDiagnosticActif()!.production.partSauBioPourcent!) + ' %',
                    libelleApresChiffre: 'de la SAU en bio ou en cours de conversion'
                }
            ],
            donneesPage: donneesPage
        };
    }

    private calculerOptionsIndicateurResumeIndiceHvn(rapport: Rapport, donneesPage: DonneesPageDiagnostic | undefined): OptionsIndicateurResume {
        return {
            indicateur: SA.getIndicateur(IDS_INDICATEURS.scoreHVN)!,
            chiffresCles: [
                {
                    libelleAvantChiffre: '&nbsp',
                    chiffre: formaterNombreEnEntierString(rapport.getDiagnosticActif()!.production.indicateurHvn.indiceTotal!) + ' /30',
                    libelleApresChiffre: 'score permettant de caractériser les systèmes agricoles qui maintiennent un haut niveau de biodiversité '
                }
            ],
            donneesPage: donneesPage
        };
    }
}

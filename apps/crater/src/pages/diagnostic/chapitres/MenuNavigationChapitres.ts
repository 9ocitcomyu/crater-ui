import '@lga/design-system/build/composants/MenuAccordeon.js';

import { MenuAccordeonItem } from '@lga/design-system/build/composants/MenuAccordeon.js';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system';
import { css, html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import { Diagnostic, Note, NOTE_VALEUR_WARNING } from '../../../domaines/diagnostics';
import { DonneesPageDiagnostic } from '../../../domaines/pages/donnees-pages.js';
import {
    construireUrlPageDiagnosticMaillon,
    construireUrlPageDiagnosticSynthese,
    construireUrlPageDiagnosticTerritoire
} from '../../../domaines/pages/pages-utils';
import { stylesIndicateurNotes } from '../../../domaines/systeme-alimentaire/configuration/indicateurs';
import { IDS_MAILLONS } from '../../../domaines/systeme-alimentaire/configuration/maillons';
import { SA } from '../../../domaines/systeme-alimentaire/configuration/systeme-alimentaire';
import { STYLES_CRATER } from '../../commun/pages-styles';

declare global {
    interface HTMLElementTagNameMap {
        'c-menu-navigation-chapitres': MenuNavigationChapitres;
    }
}

@customElement('c-menu-navigation-chapitres')
export class MenuNavigationChapitres extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        STYLES_CRATER,
        css`
            :host {
                display: block;
                width: 100%;
                background-color: var(--couleur-blanc);
                overflow-y: auto;
                border-right: 1px solid var(--couleur-neutre-clair);
                padding: var(--dsem);
            }
            .conteneur-menu-bas {
                position: relative;
                margin: 20px auto;
                z-index: 0;
            }
            .menu-bas {
                padding-left: 20px;
            }
            .trait {
                position: absolute;
                border: solid 3px var(--couleur-neutre-20);
                border-radius: 30px;
                top: 0;
                left: -20px;
                width: 60px;
                height: 100%;
            }
        `
    ];

    @property({ attribute: false })
    diagnostic?: Diagnostic;

    @property({ attribute: false })
    donneesPage?: DonneesPageDiagnostic;

    render() {
        const chapitreSelectionne = SA.getIndicateur(this.donneesPage?.chapitre ?? '')
            ? SA.getIndicateur(this.donneesPage?.chapitre ?? '')?.maillon.id
            : this.donneesPage?.chapitre ?? '';
        return html`
            <c-menu-accordeon
                class="menu-haut"
                .idItemSelectionne=${chapitreSelectionne}
                .items=${<MenuAccordeonItem[]>[
                    {
                        id: 'synthese',
                        libelle: 'Synthèse',
                        href: construireUrlPageDiagnosticSynthese(
                            this.donneesPage?.idTerritoirePrincipal ?? '',
                            this.donneesPage?.idEchelleTerritoriale
                        )
                    },
                    {
                        id: 'territoire',
                        libelle: 'Présentation du territoire',
                        href: construireUrlPageDiagnosticTerritoire(
                            this.donneesPage?.idTerritoirePrincipal ?? '',
                            this.donneesPage?.idEchelleTerritoriale
                        )
                    }
                ]}
            ></c-menu-accordeon>
            <div class="conteneur-menu-bas">
                <c-menu-accordeon
                    class="menu-bas"
                    .idItemSelectionne="${chapitreSelectionne}"
                    .items=${<MenuAccordeonItem[]>[
                        this.creerItemMenuMaillon(IDS_MAILLONS.terresAgricoles, this.diagnostic?.terresAgricoles.note ?? null, this.donneesPage),
                        this.creerItemMenuMaillon(
                            IDS_MAILLONS.agriculteursExploitations,
                            this.diagnostic?.agriculteursExploitations.note ?? null,
                            this.donneesPage
                        ),
                        this.creerItemMenuMaillon(IDS_MAILLONS.intrants, this.diagnostic?.intrants.note ?? null, this.donneesPage),
                        this.creerItemMenuMaillon(IDS_MAILLONS.production, this.diagnostic?.production.note ?? null, this.donneesPage),
                        this.creerItemMenuMaillon(
                            IDS_MAILLONS.transformationDistribution,
                            this.diagnostic?.transformationDistribution.note ?? null,
                            this.donneesPage
                        ),
                        this.creerItemMenuMaillon(IDS_MAILLONS.consommation, this.diagnostic?.consommation.note ?? null, this.donneesPage)
                    ]}
                ></c-menu-accordeon>
                <div class="trait"></div>
            </div>
        `;
    }

    private creerItemMenuMaillon(idMaillon: string, noteMaillon: Note, donneesPage: DonneesPageDiagnostic | undefined) {
        return <MenuAccordeonItem>{
            id: idMaillon,
            libelle: SA.getMaillon(idMaillon)?.nom,
            sousLibelle: `Score : ${noteMaillon === null ? '-' : noteMaillon}/10`,
            href: construireUrlPageDiagnosticMaillon(idMaillon, donneesPage?.idTerritoirePrincipal ?? '', donneesPage?.idEchelleTerritoriale),
            sousItems: [
                {
                    id: `${idMaillon}#pourquoi-cest-important`,
                    libelle: "Pourquoi c'est important ?",
                    href: construireUrlPageDiagnosticMaillon(
                        idMaillon,
                        donneesPage?.idTerritoirePrincipal ?? '',
                        donneesPage?.idEchelleTerritoriale,
                        'pourquoi-cest-important'
                    )
                },
                {
                    id: `${idMaillon}#etat-lieux`,
                    libelle: 'Les indicateurs',
                    href: construireUrlPageDiagnosticMaillon(
                        idMaillon,
                        donneesPage?.idTerritoirePrincipal ?? '',
                        donneesPage?.idEchelleTerritoriale,
                        'etat-lieux'
                    )
                },
                {
                    id: `${idMaillon}#leviers-action`,
                    libelle: "Les leviers d'action",
                    href: construireUrlPageDiagnosticMaillon(
                        idMaillon,
                        donneesPage?.idTerritoirePrincipal ?? '',
                        donneesPage?.idEchelleTerritoriale,
                        'leviers-action'
                    )
                }
            ],
            icone: `
                <svg width="20" height="20" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <circle cx="16" cy="16" r="14" fill="${stylesIndicateurNotes.calculerCouleur(
                        noteMaillon === NOTE_VALEUR_WARNING ? 5 : noteMaillon
                    )}"/>
                </svg>`
        };
    }
}

import '../commun/template/TemplatePageAccueil.js';
import '@lga/design-system/build/composants/Lien.js';

import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { css, html, LitElement } from 'lit';
import { customElement } from 'lit/decorators.js';
import { unsafeSVG } from 'lit/directives/unsafe-svg.js';

import { LIENS_EXTERNES, PAGES_AIDE, PAGES_PRINCIPALES } from '../../domaines/pages/configuration/declaration-pages';
import { ICONES_SVG } from '../../domaines/systeme-alimentaire/configuration/icones_svg.js';
import imageAdeme from '../../ressources/images/logo-ademe.png';
import imageAgenceBio from '../../ressources/images/logo-agence-bio.png';
import imageAgreste from '../../ressources/images/logo-agreste.png';
import imageAnrPia from '../../ressources/images/logo-anr-pia.png';
import imageCerema from '../../ressources/images/logo-cerema.png';
import imageEul from '../../ressources/images/logo-eul.png';
import imageFondationCarasso from '../../ressources/images/logo-fondation-carasso.png';
import imageIGN from '../../ressources/images/logo-ign.png';
import imageINSEE from '../../ressources/images/logo-insee.png';
import imageLaPoste from '../../ressources/images/logo-laposte.png';
import imageMaa from '../../ressources/images/logo-maa.png';
import imageMetropoleNCA from '../../ressources/images/logo-nice-cote-azur.png';
import imageOFB from '../../ressources/images/logo-ofb.png';
import imageOSM from '../../ressources/images/logo-openstreetmap.png';
import imageOT from '../../ressources/images/logo-ot.png';
import imageParcel from '../../ressources/images/logo-parcel.png';
import imageSolagro from '../../ressources/images/logo-solagro.png';
import { STYLES_CRATER } from '../commun/pages-styles';

declare global {
    interface HTMLElementTagNameMap {
        'c-page-projet': PageProjet;
    }
}

@customElement('c-page-projet')
export class PageProjet extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        STYLES_CRATER,
        css`
            main {
                padding-right: max((100vw - var(--largeur-maximum-contenu)) / 2, var(--dsem));
                padding-left: max((100vw - var(--largeur-maximum-contenu)) / 2, var(--dsem));
            }

            h1 {
                display: flex;
                flex-direction: column;
                align-items: center;
                gap: calc(var(--dsem) * 2);
                padding-bottom: calc(4 * var(--dsem));
            }

            h1 > svg {
                fill: var(--couleur-primaire);
            }

            .logos {
                display: flex;
                flex-wrap: wrap;
                justify-content: center;
                align-items: center;
            }

            .logo {
                margin: 0.5em 0.5em 0.5em 0.5em;
            }

            .historique {
                padding-left: 5px;
                display: grid;
                grid-template-columns: 30% 70%;
            }

            .historique p {
                grid-column: 1;
                text-align: right;
                padding-right: 1em;
                margin-top: auto;
                margin-bottom: auto;
            }

            .historique p::after {
                content: '';
                position: absolute;
                display: inline-block;
                background-color: var(--couleur-primaire);
                width: 20px;
                height: 20px;
                margin-left: 8px;
                margin-top: 2px;
                border-radius: 50%;
            }

            .historique ul {
                grid-column: 2;
                border-left: 5px solid var(--couleur-primaire);
                padding-left: 2em;
                padding-top: 1em;
                padding-bottom: 1em;
                list-style: none;
                margin: 0;
            }

            .historique ul.sous-liste {
                border: none;
                padding-left: 1em;
                text-align: center;
            }

            .historique li {
                padding-bottom: 0.3em;
            }
        `
    ];

    render() {
        return html`
            <c-template-page-accueil idItemActifMenuPrincipal=${PAGES_PRINCIPALES.projet.getId()}>
                <main slot="contenu">
                    <h1>Le projet CRATer ${unsafeSVG(ICONES_SVG.soulignerTexte)}</h1>

                    <h2>Pourquoi CRATer ?</h2>

                    <p>
                        Il est urgent et capital de repenser l’<c-lien href=${PAGES_AIDE.comprendreLeSystemeAlimentaire.getUrl()}
                            >organisation de nos systèmes alimentaires</c-lien
                        >
                        et construire de nouveaux modèles plus durables et plus résilients, en commençant par évaluer les systèmes actuels.
                    </p>
                    <p>
                        <strong>
                            C’est l’objectif de CRATer : proposer un outil numérique de sensibilisation et d'aide au diagnostic de la résilience
                            alimentaire des territoires </strong
                        >.
                    </p>

                    <p>Son objectif est double :</p>
                    <ul>
                        <li>
                            d'une part participer à la prise de conscience d’un large public (citoyens, élus, etc...) sur les enjeux de résilience
                            alimentaire ;
                        </li>
                        <li>
                            et d'autre part faciliter le travail lors de la construction de diagnostics terrain approfondis, par exemple lors de la
                            réalisation de Projets Alimentaires Territoriaux (PAT) dont le déploiement se généralise dans les collectivités.
                        </li>
                    </ul>

                    <p>
                        Le contenu du diagnostic proposé se veut donc accessible à tous, et ne nécessite pas de connaissances préalables. Il doit
                        aider à l’identification des enjeux essentiels, des vulnérabilités et des leviers d’action prioritaires sur chaque territoire.
                    </p>
                    <p>
                        L’application s’adresse également aux acteurs terrains, membres de collectivités et spécialistes du domaine afin de leur
                        faciliter le travail de consolidation et d’interprétation des données. Les résultats sont fournis sous forme d’un
                        pré-diagnostic, qui ne se substitue pas à une étude terrain, mais permet en amont de rassembler une partie des indicateurs
                        utiles.
                    </p>

                    <p>Le contenu de l’application, les données et le code sont disponibles en licence ouverte.</p>

                    <h2>Crédits</h2>
                    <ul>
                        <li>Les illustrations en page d'accueil sont de storyset (Freepik) .</li>
                        <li>
                            Les icones utilisées dans la synthèse sont de :
                            <ul>
                                <li>pour le nombre d'habitants : People by Shashank Singh from the Noun Project</li>
                                <li>pour la superficie du territoire : Landscape by Surya Cannavale from the Noun Project</li>
                                <li>pour le nombre d'hectares agricoles : Agriculture by Made from the Noun Project</li>
                            </ul>
                        </li>
                    </ul>

                    <h2>Remerciements</h2>

                    <p>Merci à toutes celles et ceux qui font avancer le projet au quotidien :</p>
                    <ul>
                        <li>l'équipe de l'association Les Greniers d'Abondance et au délà tous les bénévoles qui contribuent au projet ;</li>
                        <li>les représentants des collectivités qui participent à la définition de la feuille de route ;</li>
                        <li>les experts et acteurs de la recherche qui nous aident dans la construction des indicateurs.</li>
                    </ul>

                    <p>Nous tenons également à remercier pour leur confiance et leur soutien financier :</p>
                    <ul>
                        <li>
                            l'<c-lien href="https://www.ademe.fr/">ADEME</c-lien> et le
                            <c-lien href="https://agriculture.gouv.fr/">Ministère de l'Agriculture et de l'Alimentation</c-lien>
                        </li>
                        <li>
                            l’<c-lien href="https://anr.fr/">Agence Nationale de la Recherche</c-lien> au titre du Programme d’Investissements
                            d’Avenir portant la référence <c-lien href="https://anr.fr/ProjetIA-17-CONV-0004">ANR-17-CONV-0004</c-lien>
                        </li>
                        <li>la <c-lien href="https://www.fondationcarasso.org/">fondation Daniel et Nina CARASSO</c-lien></li>
                        <li>
                            et la <c-lien href="https://www.nicecotedazur.org/">métropole de Nice Côte d'Azur</c-lien>
                            pour son soutien au
                            <c-lien href="pdf/Rapport_d_etude_pour_la_metropole_de_Nice_Cote_d_Azur-v1.1.pdf"
                                >projet d'amélioration de l'utilisation des données du
                                <abbr title="Recensement Parcellaire Graphique">RPG</abbr></c-lien
                            >.
                        </li>
                    </ul>

                    <p class="logos">
                        <img class="logo" src="${imageAdeme}" width="84" height="100" alt="ADEME" loading="lazy" />
                        <img
                            class="logo"
                            src="${imageMaa}"
                            width="180"
                            height="113"
                            alt="Ministère de l'Agriculture et de l'Alimentation"
                            loading="lazy"
                        />
                        <img
                            class="logo"
                            src="${imageAnrPia}"
                            alt="Agence Nationale de la Recherche, Programme d’Investissements d’Avenir"
                            width="100"
                            height="100"
                            loading="lazy"
                        />
                        <img class="logo" src="${imageEul}" alt="École Urbaine de Lyon" width="190" height="66" loading="lazy" />
                        <img
                            class="logo"
                            src="${imageFondationCarasso}"
                            alt="Fondation Daniel et Nina Carasso"
                            width="173"
                            height="76"
                            loading="lazy"
                        />
                        <img class="logo" src="${imageMetropoleNCA}" alt="Métropole Nice Côte d'Azur" width="160" height="56" loading="lazy" />
                    </p>

                    <p>Enfin, merci à tous nos partenaires :</p>
                    <p></p>
                    <ul>
                        <li>
                            l'équipe <c-lien href="https://solagro.org/">Solagro</c-lien> pour la mise à disposition et l'aide à l'analyse des données
                            HVN ;
                        </li>
                        <li>
                            <c-lien href="https://lebasic.com/">Le BASIC</c-lien> et les partenaires de
                            <c-lien href="https://parcel-app.org">l'outil PARCEL</c-lien> pour leur collaboration ;
                        </li>
                        <li>ainsi que l'ensemble des fournisseurs de données :</li>
                    </ul>

                    <p class="logos">
                        <img class="logo" src="${imageAgreste}" alt="Agreste" width="200" height="31" loading="lazy" />
                        <img class="logo" src="${imageCerema}" alt="CEREMA" width="100" height="99" loading="lazy" />
                        <img class="logo" src="${imageIGN}" alt="IGN" width="91" height="100" loading="lazy" />
                        <img class="logo" src="${imageINSEE}" alt="INSEE" width="86" height="100" loading="lazy" />
                        <img class="logo" src="${imageLaPoste}" alt="La Poste" width="150" height="58" loading="lazy" />
                        <img class="logo" src="${imageOSM}" alt="OpenStreetMap France" width="81" height="100" loading="lazy" />
                        <img class="logo" src="${imageSolagro}" alt="Solagro" width="150" height="45" loading="lazy" />
                        <img class="logo" src="${imageAgenceBio}" alt="Agence Bio" width="100" height="68" loading="lazy" />
                        <img class="logo" src="${imageParcel}" alt="Parcel" width="200" height="45" loading="lazy" />
                        <img class="logo" src="${imageOFB}" alt="Office Français de la Biodiversité" width="200" height="74" loading="lazy" />
                        <img class="logo" src="${imageOT}" alt="Observatoire des territoires" width="241" height="70" loading="lazy" />
                    </p>

                    <h2>Comment contribuer ?</h2>

                    <p>
                        CRATer est développé par des membres de l'association Les Greniers d'Abondance, selon un modèle open-source, sous licence
                        <c-lien href="https://www.gnu.org/licenses/agpl-3.0.en.html">GNU AGPL v3</c-lien>.
                    </p>
                    <p>
                        Le code source du projet est disponible sur
                        <c-lien href="https://framagit.org/groups/lga">https://framagit.org/groups/lga</c-lien>.
                    </p>
                    <p>
                        La tableau des développements en cours ou prévus est disponible
                        <c-lien href="https://framagit.org/lga/crater/-/boards">ici</c-lien>.
                    </p>
                    <p>Pour nous aider vous pouvez :</p>
                    <ul>
                        <li>
                            donner votre avis sur l'application en nous faisant des retours
                            <c-lien href=${PAGES_PRINCIPALES.contact.getUrl()}> ici</c-lien> ;
                        </li>
                        <li><c-lien href=${LIENS_EXTERNES.lienFaireUnDon}> faire un don</c-lien> ;</li>
                        <li>
                            si vous êtes développeur.euse, contribuer directement à
                            <c-lien href="https://framagit.org/groups/lga/-/issues">notre TODO liste</c-lien> via des merge requests ;
                        </li>
                        <li>
                            et si ce n'est pas le cas, mais que vous êtes motivé.e pour participer,
                            <c-lien href=${PAGES_PRINCIPALES.contact.getUrl()}>contactez-nous</c-lien>!
                        </li>
                    </ul>

                    <h2>Historique des versions</h2>

                    <div class="historique">
                        <p>Octobre 2020</p>
                        <ul>
                            <li>Première diffusion de l'application</li>
                            <li>
                                Le diagnostic est réalisé à partir d'une commune, et il est décliné sur son EPCI, son département, sa région et la
                                France
                            </li>
                            <li>
                                4 composantes du système alimentaire sont analysées (adéquation théorique production/besoins, pratiques agricoles,
                                population agricole et politique foncière)
                            </li>
                            <li>Périmètre France Métropolitaine</li>
                        </ul>
                        <p>Février 2021</p>
                        <ul>
                            <li>
                                Le diagnostic est consultable directement pour un EPCI, un département, une région ou le pays (sans passer
                                obligatoirement par la commune)
                            </li>
                            <li>
                                Possibilité d'utiliser une url pour cibler un diagnostic sur n'importe quel territoire (par exemple pour le
                                département de l'Allier :
                                <c-lien href="https://crater.resiliencealimentaire.org/diagnostic?idTerritoire=D-03"
                                    >https://crater.resiliencealimentaire.org/diagnostic?idTerritoire=D-03</c-lien
                                >)
                            </li>
                            <li>Amélioration de la navigation et de l'affichage sur écran mobile</li>
                        </ul>
                        <p>Avril 2021</p>
                        <ul>
                            <li>
                                Une carte de France permet de visualiser globalement le niveau de résilience des territoires pour différentes
                                thématiques du système alimentaire
                            </li>
                            <li>La recherche de territoire est accessible directement depuis la page diagnostic</li>
                            <li>Refonte de la charte graphique et de la page d'accueil</li>
                        </ul>
                        <p>Septembre 2021</p>
                        <ul>
                            <li>
                                Ajout de la notion de surface agricole productive et surface agricole peu productive pour une meilleure exploitation
                                des données du <abbr title="Recensement Parcellaire Graphique">RPG</abbr>
                            </li>
                            <li>
                                Mise à jour des indicateurs pour exclure systématiquement les surfaces peu productives et ne prendre en compte que les
                                surfaces productives dans les calculs
                            </li>
                            <li>
                                Ajout du chapitre "Présentation du territoire" dans la page de résultat du diagnostic et clarification du chapitre
                                "Production / Besoins"
                            </li>
                        </ul>
                        <p>Décembre 2021</p>
                        <ul>
                            <li>Ajout des diagnostics pour de nouveaux types de territoires comme les PNR ou les SCOT</li>
                            <li>
                                Enrichissement du chapitre "Population Agricole" avec des indicateurs sur l'âge des exploitants, le nombre
                                d'exploitations, et leur surface
                            </li>
                            <li>
                                Enrichissement du chapitre "Politique Foncière" avec des indicateurs sur la part des logements vacants et leur
                                évolution
                            </li>
                            <li>Mise à jour du référentiel des territoires (géographie au 1e janvier 2021 de l'INSEE)</li>
                        </ul>
                        <p>Février 2022</p>
                        <ul>
                            <li>
                                Enrichissement du chapitre "Pratiques agricoles" avec des indicateurs sur l'usage des produits phytosanitaires
                                (pesticides)
                            </li>
                            <li>Cartographie de ces résultats dans la page Carte</li>
                            <li>Ajout de passerelles entre les pages Diagnostic et Carte</li>
                        </ul>
                        <p>Juin 2022</p>
                        <ul>
                            <li>
                                Ajout du chapitre "Proximité aux commerces" avec des indicateurs permettant d'approcher la dépendance théorique des
                                habitants à la voiture pour réaliser leurs achats alimentaires
                            </li>
                        </ul>
                        <p>Septembre 2022</p>
                        <ul>
                            <li>Réorganisation du contenu avec un nouveau découpage des thématiques en maillons du système alimentaire :</li>
                            <ul class="sous-liste">
                                <li>Adéquation Besoins Production / Pratiques agricoles / Population Agricole / Politique Foncière</li>
                                <li>→</li>
                                <li>
                                    Terres Agricoles / Agriculteurs & Exploitations / Intrants / Production / Transformation & Distribution /
                                    Consommation
                                </li>
                            </ul>
                            <li>Mise à jour du référentiel des territoires (géographie au 1e janvier 2022 de l'INSEE)</li>
                            <li>Ajout de l'échelle communale dans le module cartographique</li>
                            <li>Ajout d'une barre de recherche des territoires dans le module cartographique</li>
                        </ul>
                        <p>Décembre 2022</p>
                        <ul>
                            <li>Refonte de la navigation sur mobile</li>
                            <li>Amélioration des performances de l'API pour éviter des erreurs fréquentes de timeout</li>
                            <li>Ajout de contenus pédagogiques sur les enjeux du système alimentaire</li>
                            <li>Ajout d'une FAQ et d'un glossaire</li>
                        </ul>
                        <p>Février 2023</p>
                        <ul>
                            <li>Refonte finale du design global de l'application</li>
                        </ul>
                        <p>Avril 2023</p>
                        <ul>
                            <li>Ajout de l'indicateur Taux de pauvreté dans le chapitre Consommation</li>
                            <li>Ajout de données sur la spécialisation agricole de la production</li>
                        </ul>
                    </div>
                </main>
            </c-template-page-accueil>
        `;
    }
}

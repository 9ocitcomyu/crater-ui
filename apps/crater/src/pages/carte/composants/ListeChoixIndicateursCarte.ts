import { EvenementFermer } from '@lga/design-system/build/evenements/EvenementFermer.js';
import { EvenementSelectionner } from '@lga/design-system/build/evenements/EvenementSelectionner';
import { CSS_BREAKPOINT_MAX_WIDTH_TABLETTE } from '@lga/design-system/build/styles/styles-breakpoints.js';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { css, html, LitElement, unsafeCSS } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { unsafeHTML } from 'lit/directives/unsafe-html.js';
import { unsafeSVG } from 'lit/directives/unsafe-svg.js';

import { SA } from '../../../domaines/systeme-alimentaire/configuration/systeme-alimentaire.js';
import { IndicateurCarte } from '../../../domaines/systeme-alimentaire/Indicateur.js';
import { Maillon } from '../../../domaines/systeme-alimentaire/Maillon.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-liste-choix-indicateurs-carte': ListeChoixIndicateursCarte;
    }
}

@customElement('c-liste-choix-indicateurs-carte')
export class ListeChoixIndicateursCarte extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        css`
            :host {
                display: block;
                height: 100%;
                overflow-y: auto;
                --couleur-icone: var(--couleur-primaire);
                background-color: var(--couleur-blanc);
            }

            input {
                display: none;
            }

            label {
                background-color: var(--couleur-neutre-clair);
                box-shadow: 0 0.5px 3px 0 var(--couleur-neutre-20);
                cursor: pointer;
                display: grid;
                grid-template-columns: auto 1fr;
                margin-top: calc(0.5 * var(--dsem));
                margin-bottom: calc(0.5 * var(--dsem));
                border-radius: calc(0.5 * var(--dsem));
            }

            label:hover {
                filter: brightness(90%);
            }

            input:checked + label {
                box-shadow: inset 0px 0px 0px 2px var(--couleur-primaire);
                cursor: auto;
            }

            .icone {
                margin-right: 10px;
                display: grid;
                grid-row-start: 1;
                justify-content: end;
            }

            .icone svg {
                margin: auto;
                width: 40px;
                height: 40px;
            }

            .maillon {
                padding-right: var(--dsem);
                display: grid;
                grid-template-columns: auto 1fr;
                grid-template-rows: auto 1fr;
            }

            .titre-maillon {
                color: var(--couleur-primaire-sombre);
                margin: auto 0;
            }

            .indicateurs {
                grid-column-start: 2;
                background-color: var(--couleur-blanc);
                display: flex;
                flex-direction: column;
                padding: 0;
                list-style-type: none;
            }

            .icone-indicateur {
                margin: auto;
                padding: var(--dsem);
            }

            .titre-indicateur {
                margin: auto 0;
            }

            @media screen and (max-width: ${unsafeCSS(CSS_BREAKPOINT_MAX_WIDTH_TABLETTE + 'px')}) {
                ul {
                    margin: 0;
                }
            }
        `
    ];

    private listeIndicateurs = SA.indicateursCarte.map((indicateurCarte: IndicateurCarte) => ({
        id: indicateurCarte.id,
        icone: indicateurCarte.icone,
        libelle: indicateurCarte.libelle,
        description: indicateurCarte.description,
        idMaillon: indicateurCarte.maillon.id
    }));

    private listeMaillons = SA.maillons.map((maillon: Maillon) => ({
        id: maillon.id,
        nom: maillon.nom.toUpperCase(),
        icone: maillon.icone
    }));

    @property()
    idIndicateurActif?: string;

    render() {
        return html` ${this.listeMaillons
            .filter((maillon) => this.listeIndicateurs.map((i) => i.idMaillon).includes(maillon.id))
            .map(
                (maillon) =>
                    html`
                        <div class="maillon">
                            <div class="icone">${unsafeSVG(maillon.icone)}</div>
                            <div class="titre-maillon titre-petit">${unsafeHTML(maillon.nom)}</div>
                            <ul class="indicateurs" @change=${this.changerBoutonActif}>
                                ${this.listeIndicateurs
                                    .filter((i) => i.idMaillon === maillon.id)
                                    .map(
                                        (i) =>
                                            html`<li>
                                                <input
                                                    type="radio"
                                                    name="groupe-boutons"
                                                    id="${i.id}"
                                                    value="${i.id}"
                                                    .checked=${i.id === this.idIndicateurActif}
                                                />
                                                <label for="${i.id}">
                                                    <div class="icone-indicateur">${unsafeSVG(i.icone)}</div>
                                                    <div class="titre-indicateur texte-titre">${unsafeHTML(i.libelle)}</div>
                                                </label>
                                            </li>`
                                    )}
                            </ul>
                        </div>
                    `
            )}`;
    }

    changerBoutonActif(e: Event) {
        this.dispatchEvent(new EvenementSelectionner((<HTMLElement>e.target)?.id));
        this.dispatchEvent(new EvenementFermer(true));
    }
}

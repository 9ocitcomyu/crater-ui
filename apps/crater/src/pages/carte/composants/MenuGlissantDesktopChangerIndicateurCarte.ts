import '@lga/design-system/build/composants/PanneauGlissant.js';
import '@lga/design-system/build/composants/Bouton.js';
import './ListeChoixIndicateursCarte';
import '../../commun/template/TemplatePanneau.js';

import { Bouton } from '@lga/design-system/build/composants/Bouton.js';
import { PanneauGlissant } from '@lga/design-system/build/composants/PanneauGlissant.js';
import { EvenementFermer } from '@lga/design-system/build/evenements/EvenementFermer';
import { ICONES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/icones-design-system';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { css, html, LitElement } from 'lit';
import { customElement, property, query } from 'lit/decorators.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-menu-glissant-desktop-changer-indicateur-carte': MenuGlissantDesktopChangerIndicateurCarte;
    }
}

@customElement('c-menu-glissant-desktop-changer-indicateur-carte')
export class MenuGlissantDesktopChangerIndicateurCarte extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        css`
            :host {
                display: block;
                height: 100%;
            }

            #nom-indicateur {
                color: var(--couleur-neutre);
            }

            c-bouton {
                margin: auto;
                margin-right: 0;
            }

            div[slot='panneau'] {
                padding: calc(2 * var(--dsem));
                margin-top: var(--hauteur-totale-entete);
                background-color: var(--couleur-blanc);
                height: calc(100vh - var(--hauteur-totale-entete));
            }

            c-liste-choix-indicateurs-carte {
                height: 90%;
            }
        `
    ];

    @property()
    idIndicateurActif?: string;

    @query('#bouton-retour')
    private boutonRetour?: Bouton;

    render() {
        return html`
            <c-panneau-glissant coteGlissement=${PanneauGlissant.COTE_GLISSEMENT.gauche} largeur="40vw">
                <c-bouton slot="bouton-ouvrir" type=${Bouton.TYPE.relief} libelle="Changer" libelleTooltip="Changer d'indicateur"> </c-bouton>
                <div slot="panneau">
                    <c-bouton id="bouton-retour" libelle="Retour" libelleTooltip="Retour" type="${Bouton.TYPE.plat}" @click="${this.fermer}">
                        ${ICONES_DESIGN_SYSTEM.flecheGauche}
                    </c-bouton>
                    <c-liste-choix-indicateurs-carte idIndicateurActif=${this.idIndicateurActif!}></c-liste-choix-indicateurs-carte>
                </div>
            </c-panneau-glissant>
        `;
    }

    private fermer() {
        this.boutonRetour?.dispatchEvent(new EvenementFermer());
    }
}

import { getApiBaseUrl } from '@lga/commun/build/env/config-baseurl';

import { DomaineCarte } from '../../domaines/carte/DomaineCarte';
import { chargerHierarchieTerritoires } from '../../requetes-api/requetes-api-territoires';

export class ControleurCarte {
    chargerTerritoirePrincipal(idTerritoirePrincipal: string, domaineCarte: DomaineCarte): Promise<DomaineCarte> {
        return chargerHierarchieTerritoires(getApiBaseUrl(), idTerritoirePrincipal).then((h) => {
            domaineCarte.positionnerTerritoirePrincipal(h.territoirePrincipal, h.departement?.id);
            return domaineCarte;
        });
    }
}

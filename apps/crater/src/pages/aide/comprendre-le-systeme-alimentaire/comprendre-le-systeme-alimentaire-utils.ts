import { LienFilAriane } from '@lga/design-system/build/composants/FilAriane';
import { MenuAccordeonItem } from '@lga/design-system/build/composants/MenuAccordeon';

import { PAGES_AIDE, PAGES_COMPRENDRE_LE_SA, PAGES_PRINCIPALES } from '../../../domaines/pages/configuration/declaration-pages';

export const ITEMS_MENU_COMPRENDRE_LE_SA: MenuAccordeonItem[] = [
    {
        id: 'comprendre-le-sa',
        libelle: 'Comprendre le système alimentaire',
        sousItems: [
            {
                id: PAGES_COMPRENDRE_LE_SA.systemeActuel.getId(),
                libelle: PAGES_COMPRENDRE_LE_SA.systemeActuel.getTitreCourt(),
                href: PAGES_COMPRENDRE_LE_SA.systemeActuel.getUrl()
            },
            {
                id: PAGES_COMPRENDRE_LE_SA.defaillancesVulnerabilites.getId(),
                libelle: PAGES_COMPRENDRE_LE_SA.defaillancesVulnerabilites.getTitreCourt(),
                href: PAGES_COMPRENDRE_LE_SA.defaillancesVulnerabilites.getUrl()
            },
            {
                id: PAGES_COMPRENDRE_LE_SA.transitionAgricoleAlimentaire.getId(),
                libelle: PAGES_COMPRENDRE_LE_SA.transitionAgricoleAlimentaire.getTitreCourt(),
                href: PAGES_COMPRENDRE_LE_SA.transitionAgricoleAlimentaire.getUrl()
            }
        ]
    }
];

export function construireLiensFilArianePagesComprendreLeSA(libellePage: string, hrefPage: string): LienFilAriane[] {
    return [
        {
            libelle: `Aide`,
            href: PAGES_PRINCIPALES.aide.getUrl()
        },
        {
            libelle: `Comprendre le système alimentaire`,
            href: PAGES_AIDE.comprendreLeSystemeAlimentaire.getUrl()
        },
        {
            libelle: libellePage,
            href: hrefPage
        }
    ];
}

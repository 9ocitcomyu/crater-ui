import '../../commun/template/TemplatePageAvecSommaire.js';

import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { unsafeHTML } from 'lit/directives/unsafe-html.js';

import { PAGES_METHODOLOGIE, PAGES_PRINCIPALES } from '../../../domaines/pages/configuration/declaration-pages';
import { SourceDonnee, sourcesDonnees } from '../../../domaines/systeme-alimentaire/configuration/sources-donnees.js';
import { STYLES_CRATER } from '../../commun/pages-styles';
import { construireLiensFilArianePagesMethodologie, ITEMS_MENU_METHODOLOGIE, STYLES_METHODOLOGIE } from './methodologie-utils.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-page-methodologie-sources-donnees': PageMethodologieSourcesDonnees;
    }
}

@customElement('c-page-methodologie-sources-donnees')
export class PageMethodologieSourcesDonnees extends LitElement {
    static styles = [STYLES_DESIGN_SYSTEM, STYLES_CRATER, STYLES_METHODOLOGIE];

    render() {
        return html`
            <c-template-page-avec-sommaire
                idItemActifMenuPrincipal="${PAGES_PRINCIPALES.aide.getId()}"
                idItemActifMenuSommaire="${PAGES_METHODOLOGIE.sourcesDonnees.getId()}"
                .itemsMenuSommaire="${ITEMS_MENU_METHODOLOGIE}"
                .liensFilAriane="${construireLiensFilArianePagesMethodologie(
                    PAGES_METHODOLOGIE.sourcesDonnees.getTitreCourt(),
                    PAGES_METHODOLOGIE.sourcesDonnees.getUrl()
                )}"
                idElementCibleScroll=${this.idElementCibleScroll}
            >
                <div slot="contenu">${this.renderCoeurPage()}</div>
            </c-template-page-avec-sommaire>
        `;
    }

    @property()
    idElementCibleScroll = '';

    private renderCoeurPage() {
        return html`
            <section class="texte-moyen">
                <h1>Sources de données</h1>

                <div>
                    ${Object.values(sourcesDonnees).map(
                        (sd: SourceDonnee) =>
                            html` <table>
                                <thead id="${sd.id}">
                                    <tr>
                                        <th class="texte-titre">${sd.nom}</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Définition</td>
                                        <td>${unsafeHTML(sd.definition)}</td>
                                    </tr>
                                    <tr>
                                        <td>Année(s)</td>
                                        <td>${sd.annees}</td>
                                    </tr>
                                    <tr>
                                        <td>Périmètre géographique</td>
                                        <td>${sd.perimetre_geographique}</td>
                                    </tr>
                                    <tr>
                                        <td>Source</td>
                                        <td>${unsafeHTML(sd.source)}</td>
                                    </tr>
                                    ${sd.limites
                                        ? html` <tr>
                                              <td>Limites</td>
                                              <td>${unsafeHTML(sd.limites)}</td>
                                          </tr>`
                                        : ''}
                                </tbody>
                            </table>`
                    )}
                </div>
            </section>
        `;
    }
}

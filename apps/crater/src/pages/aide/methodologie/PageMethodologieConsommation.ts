import '../../commun/template/TemplatePageAvecSommaire.js';

import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { unsafeHTML } from 'lit/directives/unsafe-html.js';

import { PAGES_METHODOLOGIE, PAGES_PRINCIPALES } from '../../../domaines/pages/configuration/declaration-pages';
import { IDS_INDICATEURS } from '../../../domaines/systeme-alimentaire/configuration/indicateurs.js';
import { sourcesDonnees } from '../../../domaines/systeme-alimentaire/configuration/sources-donnees.js';
import { STYLES_CRATER } from '../../commun/pages-styles';
import { construireLiensFilArianePagesMethodologie, ITEMS_MENU_METHODOLOGIE, STYLES_METHODOLOGIE } from './methodologie-utils.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-page-methodologie-consommation': PageMethodologieConsommation;
    }
}

@customElement('c-page-methodologie-consommation')
export class PageMethodologieConsommation extends LitElement {
    static styles = [STYLES_DESIGN_SYSTEM, STYLES_CRATER, STYLES_METHODOLOGIE];

    @property()
    idElementCibleScroll = '';

    render() {
        return html`
            <c-template-page-avec-sommaire
                idItemActifMenuPrincipal="${PAGES_PRINCIPALES.aide.getId()}"
                idItemActifMenuSommaire="${PAGES_METHODOLOGIE.consommation.getId()}"
                .itemsMenuSommaire="${ITEMS_MENU_METHODOLOGIE}"
                .liensFilAriane="${construireLiensFilArianePagesMethodologie(
                    PAGES_METHODOLOGIE.consommation.getTitreCourt(),
                    PAGES_METHODOLOGIE.consommation.getUrl()
                )}"
                idElementCibleScroll=${this.idElementCibleScroll}
            >
                <div slot="contenu">${this.renderCoeurPage()}</div>
            </c-template-page-avec-sommaire>
        `;
    }

    private renderCoeurPage() {
        return html`
            <section class="texte-moyen">
                <h1>Règles de gestion pour le maillon Consommation</h1>

                <h2 id="${IDS_INDICATEURS.partAlimentationAnimaleDansConsommation}">
                    Part de l'alimentation d'origine animale dans l'empreinte en surface de la consommation
                </h2>
                <p>
                    Cet indicateur mesure la part de l'alimentation d'origine animale dans l'empreinte en surface de la consommation totale des
                    habitants (selon le régime alimentaire actuel moyen d'un Français).
                </p>

                <h3>Données d'entrée</h3>

                <p>Cet indicateur utilise les données de consommation de PARCEL, comme expliqué dans le chapitre Production.</p>

                <h3>Méthode de calcul</h3>
                <p>L'indicateur est calculé comme suit :</p>
                <blockquote>
                    <p>part_alimentation_origine_animale [%] = consommation_alimentation_orgine_animale / consommation_totale * 100</p>
                </blockquote>
                <p>avec :</p>
                <ul>
                    <li>
                        consommation_alimentation_orgine_animale : surface nécessaire pour produire les aliments d'origine animale consommés par les
                        habitants [ha]
                    </li>
                    <li>consommation_totale : surface nécessaire pour produire l'ensemble des aliments consommés par les habitants [ha]</li>
                </ul>

                <h2 id="${IDS_INDICATEURS.tauxPauvrete}">Taux de pauvreté (seuil de 60%)</h2>
                <p>${unsafeHTML(sourcesDonnees.taux_pauvrete.definition)}</p>

                <h3>Données d'entrée et méthode de calcul</h3>

                <p>Cet indicateur utilise directement les données de l'${unsafeHTML(sourcesDonnees.taux_pauvrete.source)}.</p>

                <h2 id="${IDS_INDICATEURS.noteConsommation}">Évaluation globale du maillon Consommation</h2>
                <p>
                    Aucune note n'est actuellement disponible mais un message, donné à l'échelle nationale, évalue le régime alimentaire actuel moyen
                    des Français ainsi que la précarité alimentaire.
                </p>
            </section>
        `;
    }
}

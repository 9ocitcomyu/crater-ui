import '../../commun/template/TemplatePageAvecSommaire.js';
import '@lga/design-system/build/composants/Lien.js';

import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import { PAGES_METHODOLOGIE, PAGES_PRINCIPALES } from '../../../domaines/pages/configuration/declaration-pages';
import { IDS_INDICATEURS } from '../../../domaines/systeme-alimentaire/configuration/indicateurs.js';
import { STYLES_CRATER } from '../../commun/pages-styles';
import { construireLiensFilArianePagesMethodologie, ITEMS_MENU_METHODOLOGIE, STYLES_METHODOLOGIE } from './methodologie-utils.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-page-methodologie-production': PageMethodologieProduction;
    }
}

@customElement('c-page-methodologie-production')
export class PageMethodologieProduction extends LitElement {
    static styles = [STYLES_DESIGN_SYSTEM, STYLES_CRATER, STYLES_METHODOLOGIE];

    render() {
        return html`
            <c-template-page-avec-sommaire
                idItemActifMenuPrincipal="${PAGES_PRINCIPALES.aide.getId()}"
                idItemActifMenuSommaire="${PAGES_METHODOLOGIE.production.getId()}"
                .itemsMenuSommaire="${ITEMS_MENU_METHODOLOGIE}"
                .liensFilAriane="${construireLiensFilArianePagesMethodologie(
                    PAGES_METHODOLOGIE.production.getTitreCourt(),
                    PAGES_METHODOLOGIE.production.getUrl()
                )}"
                idElementCibleScroll=${this.idElementCibleScroll}
            >
                <div slot="contenu">${this.renderCoeurPage()}</div>
            </c-template-page-avec-sommaire>
        `;
    }

    @property()
    idElementCibleScroll = '';

    private renderCoeurPage() {
        return html`
            <section class="texte-moyen">
                <h1>Règles de gestion pour le maillon Production</h1>

                <p>L'objectif de cette partie du diagnostic est de rendre compte :</p>
                <ul>
                    <li>
                        de la part de la consommation de la population d'un territoire qui peut être couverte
                        <i>en théorie</i> par la production agricole de ce territoire dans l'hypothèse où l’on relocaliserait tout. Notez qu'il ne
                        s'agit pas de la part de la consommation réellement couverte par la production locale. Les flux logistiques sont aujourd'hui
                        totalement dissociés de la disponibilité locale, si bien qu'à l'échelle d'un bassin de vie, presque toute la production est
                        généralement exportée, et tous les biens consommés sont importés depuis d'autres territoires. De même, le niveau de production
                        d'un territoire ne présage en rien de sa dépendance à des facteurs de production importés de loin : pétrole, engrais,
                        pesticides... Cette seule note ne suffit donc pas à estimer le degré d'autonomie d'un territoire ;
                    </li>
                    <li>de la durabilité et de l'impact des pratiques agricoles.</li>
                </ul>

                <h2 id="${IDS_INDICATEURS.adequationTheoriqueProductionConsommation}">L'adéquation théorique entre production et consommation</h2>
                <p>
                    Cet indicateur représente la part de la consommation du territoire qui pourrait <i>en théorie</i> être couverte par sa propre
                    production.
                </p>

                <h3>Nomenclature des groupes de cultures</h3>
                <p>
                    Pour présenter la production et la consommation par grandes catégories de cultures, CRATer utilise sa propre nomenclature de
                    “<strong>cultures</strong>” et “<strong>groupes de cultures</strong>”
                </p>
                <p>Les &quot;<strong>groupes de cultures</strong>&quot; sont les suivants :</p>
                <ul>
                    <li>Céréales : blé, maïs, orge...</li>
                    <li>Oléoprotéagineux : légumineuses, tourteaux, colza, tournesol...</li>
                    <li>Fourrages : cultures à destination exclusive des animaux (prairies, maïs pour fourrage, ...)</li>
                    <li>Fruits et légumes : fruits, légumes et fruits à coque</li>
                    <li>Autres cultures : betterave sucrière, oliviers, pommes de terre, tabac, vignes, plantes médicinales...</li>
                    <li>Surfaces non cultivées : jachères...</li>
                </ul>

                <p>
                    <strong
                        >Nota: les “Surfaces non cultivées“, dites aussi “Surfaces peu productives“, ne sont pas prises en compte dans les
                        calculs.</strong
                    >
                </p>

                <p>Chaque “<strong>groupe de cultures</strong>” rassemble un ensemble de “<strong>cultures</strong>”, qui permettent :</p>
                <ul>
                    <li>
                        de faire les correspondances avec la codification des cultures en provenance du le
                        <abbr title="Recensement Parcellaire Graphique">RPG</abbr>, pour identifier les différents types de production ;
                    </li>
                    <li>
                        de faire les correspondances avec la codification des produits fournis par l’application PARCEL, pour identifier les
                        différents types de consommation.
                    </li>
                </ul>

                <h3>La production agricole du territoire : l’évaluation des surfaces agricoles utiles par types de cultures</h3>
                <p>
                    Cet indicateur mesure la production agricole d'un territoire, en hectares de surface agricole utile. Il est calculé pour chaque
                    &quot;groupe de cultures&quot; et pour toutes les cultures confondues (somme de tous les groupes de cultures).
                </p>
                <p>
                    Les valeurs sont calculées à partir du <abbr title="Recensement Parcellaire Graphique">RPG</abbr> (voir
                    <c-lien href=${PAGES_METHODOLOGIE.sourcesDonnees.getUrl()}>sources de données</c-lien>), en évaluant les surfaces agricoles utiles
                    pour chaque culture présente sur le territoire de chaque commune.
                </p>
                <p>
                    On agrège ensuite ces données sur les groupes de cultures CRATer via une correspondance entre les codes cultures définis dans le
                    <abbr title="Recensement Parcellaire Graphique">RPG</abbr> et les groupes de cultures CRATer, au niveau de chaque commune, puis
                    sur les différentes échelles territoriales.
                </p>
                <p>
                    <strong>Limitations actuelles</strong> : pour l'instant, CRATer ne prend pas en compte la succession culturale dans la même année
                    (utilisation de la seule culture principale, et pas des cultures secondaires qui sont disponibles dans le
                    <abbr title="Recensement Parcellaire Graphique">RPG</abbr>). En conséquence, les données de production obtenues risque d'être
                    sous-estimées, mais la plupart du temps cela ne devrait pas impacter les ordres de grandeur des résultats. D’une part parce que
                    ces cultures secondaires sont encore relativement peu répandues et d’autre part parce que leur contribution à l’alimentation est
                    souvent bien plus faible que celle de la culture principale (e.g. moutarde, cameline, chanvre).
                </p>

                <h3>La consommation du territoire</h3>
                <p>
                    Elle est obtenue à partir de l’application PARCEL (voir
                    <c-lien href=${PAGES_METHODOLOGIE.sourcesDonnees.getUrl()}>sources de données</c-lien>), qui fournit des évaluations de cette
                    consommation pour la population d’un territoire, par types de produits, pour l'assiette actuelle et sur la base de la part des
                    surfaces actuelles dédiées aux productions bio. Les produits représentent les produits alimentaires, après transformation.
                    L'assiette actuelle correspond à celle d'un Français moyen actuellement.
                </p>
                <p>Pour chaque produit, les valeurs sont ensuite traduites en hectares de surfaces agricoles utiles par types de cultures :</p>

                <ul>
                    <li>
                        en utilisant une correspondance simple entre les produits PARCEL et les cultures CRATer pour les produits qui ne sont pas des
                        produits de l’élevage ;
                    </li>
                    <li>
                        en distribuant la consommation liée aux produits de l’élevage sur des cultures destinées à l’alimentation animale selon des
                        ratios connus pour chaque région (part de céréales, fourrages, tourteaux, etc. dans l’alimentation animale).
                    </li>
                </ul>
                <h3>Méthode de calcul</h3>
                <p>L’indicateur est calculé via les étapes suivantes :</p>

                <blockquote>
                    <ul>
                        <li>
                            pour chaque groupe de culture, le ratio [en %] entre la production [en ha] et la consommation [en ha] est calculé et capé
                            à 100% (pour ne pas tenir compte des surproductions qui pourraient masquer les déficits d'autres groupes de cultures in
                            fine) ;
                        </li>
                        <li>
                            le ratio de chaque groupe de culture est pondéré par la part du groupe de cultures dans la consommation totale du
                            territoire. A titre d’exemple, pour la France, ces pondérations sont les suivantes : Céréales (19%), Fruits, Légume &amp;
                            Fruits à coque (2,2%), Oléoprotéagineux (11,3%), Fourrages (65,5%), Autres cultures (2%)
                        </li>
                        <li>la moyenne effectuée avec ces pondérations permet d’obtenir l'indicateur qui a une valeur entre 0 et 100%.</li>
                    </ul>
                </blockquote>

                <h2 id="${IDS_INDICATEURS.partSAUBio}">Part de surface biologique dans la surface agricole utile</h2>
                <h3>Données d’entrée</h3>
                <p>
                    Cet indicateur utilise les données de l’Agence Bio (voir
                    <c-lien href=${PAGES_METHODOLOGIE.sourcesDonnees.getUrl()}>sources de données</c-lien>) donnant les surfaces agricoles biologiques
                    (celles labellisées AB et celles en conversion) et les surfaces agricoles productives (voir
                    <c-lien href="#nomenclature-surfaces-agricoles">nomenclature des surfaces agricoles</c-lien>) issues du
                    <abbr title="Recensement Parcellaire Graphique">RPG</abbr> (voir
                    <c-lien href=${PAGES_METHODOLOGIE.sourcesDonnees.getUrl()}>sources de données</c-lien>).
                </p>
                <h3>Méthode de calcul</h3>
                <p>L’indicateur est calculé selon la formule suivante :</p>
                <blockquote>
                    <p>part_surfaces_biologiques [en %] = surfaces_biologiques / surface_agricole_utile_productive * 100</p>
                </blockquote>
                <p>avec :</p>
                <ul>
                    <li>surfaces_biologiques : surfaces agricoles biologiques (en conversion ou labellisées AB) en 2019 [ha]</li>
                    <li>surface_agricole_utile_productive : surface agricole utile productive en 2017 [ha]</li>
                </ul>
                <h3>Limites</h3>
                <ul>
                    <li>
                        Comme expliqué dans la partie <c-lien href=${PAGES_METHODOLOGIE.sourcesDonnees.getUrl()}>sources de données</c-lien>, les
                        surfaces agricoles totales utilisées sont sous-estimées. Le ratio sera donc mal estimé si les deux surfaces ne sont pas
                        sous-estimées du même ordre de grandeur. La part de SAU Bio résultat est toujours ramenée à une valeur entre 0 et 100%,
                        notamment pour des valeurs qui dépasseraient 100% en cas de problèmes de cohérence de souces de données.
                    </li>
                    <li>
                        Pour des raisons de confidentialité, les données ne sont pas disponibles sur les territoires pour lesquels il y a moins de 3
                        exploitants en bio. Dans ces cas là, cet indicateur (et la note globale pour l’axe pratiques agricoles n’est pas disponible).
                    </li>
                    <li>
                        La surface agricole utile productive est celle de 2017 et non de 2019 comme il faudrait en tout rigueur (mise à jour à venir)
                    </li>
                </ul>
                <h3>Message détaillé</h3>
                <blockquote>
                    <p>
                        “La surface agricole biologique représente [valeur] de sa SAU productive ([valeur_absolue] ha), ce qui représente
                        [valeur/valeur_France] fois la moyenne nationale.”
                    </p>
                </blockquote>

                <h2 id="${IDS_INDICATEURS.scoreHVN}">Le score Haute Valeur Naturelle</h2>
                <p>
                    Cet indicateur est calculé par <c-lien href="https://solagro.org">Solagro</c-lien>. La méthodologie utilisée pour le calcul HVN
                    est consultable dans cette
                    <c-lien href="https://solagro.org/nos-domaines-d-intervention/agroecologie/haute-valeur-naturelle">page</c-lien>.
                </p>
                <p>
                    La description synthétique ci-dessous reprend les informations essentielles qui sont réutilisées dans CRATer, à savoir le score
                    HVN global sur 30, et ses 3 composantes, chacune notée sur 10 : L’indice de Haute Valeur Naturelle caractérise les systèmes
                    agricoles qui maintiennent un haut niveau de biodiversité. Il agrège des composantes très représentatives de la nature des modes
                    de production comme la dépendance aux intrants, le poids des monocultures, etc… Trois dimensions, notées de 0 à 10, sont prises en
                    compte :
                </p>
                <blockquote>
                    <ul>
                        <li>
                            indicateur 1 : Cet indicateur évalue la diversité de l’assolement et la part des prairies dans l’assolement. Des points
                            sont perdus dès qu’une culture dépasse 10% de la SAU.<br />Cet indicateur évalue aussi la capacité à stocker du carbone et
                            protéger les ressources en eau via la présence de prairies permanentes et temporaires ainsi que des couverts végétaux.
                        </li>
                        <li>
                            indicateur 2 : Cet indicateur tient compte du niveau de fertilisation minérale azotée apportée aux prairies, ainsi que des
                            espèces cultivées et des rendements moyens obtenus. Il prend également en compte l'extensivité des élevages.
                        </li>
                        <li>
                            indicateur 3 : Le dernier indicateur identifie la présence d’éléments fixes du paysage (infrastructures agroécologiques)
                            dont la liste dépend des informations statistiquement disponibles actuellement : lisières de bois, haies, vergers
                            traditionnels, étangs piscicoles et prairies humides (Pointereau, 2007 et 2010).
                        </li>
                    </ul>
                </blockquote>
                <p>
                    Le score global est noté sur 30. Un territoire est considéré comme territoire à haute valeur naturelle à partir du seuil de
                    14,78/30.
                </p>
                <p>
                    Cet indicateur est fournit à l’échelle de la commune par SOLAGRO. CRATer calcule sa valeur pour les échelles géographiques
                    supérieures (EPCI, département...) en faisant une moyenne pondérée par la SAU productive.
                </p>
                <h3>Message détaillé</h3>
                <blockquote>
                    <ul>
                        <li>
                            Score HVN global &lt; 14.78 : “Le territoire présente une haute valeur naturelle inférieure à la cible du cahier des
                            charges Solagro. La réduction de l'intensité des cheptels, la réduction des intrants chimiques et une meilleures gestion
                            des infrastructures agroécologiques (haies, lisières, prairies humides...) sont les pistes d'amélioration à explorer.”
                        </li>
                        <li>
                            Score HVN global &gt;= 14,78 : “Le territoire présente une haute valeur naturelle correspondant à la cible de
                            labellisation du cahier des charges Solagro. Le territoire bénéficie d'exploitations agricoles mettant en œuvre une
                            diversité d'assolement, des pratiques agricoles extensives et présentant des infrastructures agroécologiques semi-naturels
                            témoignant de la qualité de service environnemental.”
                        </li>
                    </ul>
                </blockquote>

                <h2 id="${IDS_INDICATEURS.noteProduction}">Évaluation globale du maillon Production</h2>
                <h3>Note</h3>
                <p>La note est calculée sur la base de deux notes :</p>
                <ul>
                    <li>
                        <p>Une note basé sur l'indicateur I = adéquation théorique entre production et consommation :</p>
                        <blockquote>NA = I / 10</blockquote>
                    </li>
                    <li>
                        <p>Une note reflétant les pratiques agricoles basée sur deux notes :</p>
                        <ul>
                            <li>
                                <p>
                                    une note N1 calculée sur base de l’indicateur de <strong>part de surface biologique</strong>, bornée entre 0 et 10
                                    et obtenue par interpolation entre :
                                </p>
                                <blockquote>
                                    <ul>
                                        <li>la note 0, obtenue si la part de surface agricole bio est de 0%</li>
                                        <li>
                                            la note 5, obtenue si la part de surface agricole bio est la même que la part moyenne surface agricole bio
                                            en France
                                        </li>
                                    </ul>
                                </blockquote>
                            </li>
                            <li>
                                <p>
                                    une note N2 calculée sur base de l’indicateur <strong>score Haute Valeur Naturelle</strong> en ramenant ce score
                                    d’une note sur 30 à une note sur 10 :
                                </p>
                                <blockquote>N2 = Score HVN / 30</blockquote>
                            </li>
                            <li>
                                <p>ces deux notes sont agrégées comme suit :</p>
                                <blockquote>NB = (N1 + N2) / 2</blockquote>
                            </li>
                        </ul>
                        <p>Les deux notes sont finalement combinées de la façon suivante :</p>
                        <blockquote>N = (NA + NB) / 2</blockquote>
                    </li>
                </ul>

                <h3>Message de synthèse</h3>
                <p>Le message est composé deux parties :</p>
                <ul>
                    <li>
                        <p>Une partie basée sur la note NA :</p>
                        <blockquote>
                            <ul>
                                <li>Si NA = 10, alors message = &quot;Production suffisante pour couvrir tous les besoins&quot;</li>
                                <li>
                                    Si NA &gt;= 7 et &lt; 10, alors message = &quot;Production presque suffisante pour couvrir les besoins, quelques
                                    secteurs déficitaires&quot;
                                </li>
                                <li>Si NA &gt;= 3 et &lt; 7, alors message = &quot;Production insuffisante pour couvrir les besoins&quot;</li>
                                <li>Si NA &lt; 3, alors message = &quot;Production nettement insuffisante pour couvrir les besoins&quot;</li>
                            </ul>
                        </blockquote>
                    </li>
                    <li>
                        <p>Une partie basée sur la note NB :</p>
                        <blockquote>
                            <ul>
                                <li>Si NB &gt;= 7 et &lt; 10, alors message = &quot;Pratiques agricoles favorables à la biodiversité.&quot;</li>
                                <li>Si NB &gt;= 3 et &lt; 7, alors message = &quot;Pratiques agricoles préjudiciables à la biodiversité.&quot;</li>
                                <li>Si NB &lt; 3, alors message = &quot;Pratiques agricoles très préjudiciables à la biodiversité.&quot;</li>
                            </ul>
                        </blockquote>
                    </li>
                </ul>
            </section>
        `;
    }
}

import '@lga/design-system/build/composants/Lien.js';

import { html, TemplateResult } from 'lit';

export interface SectionPageGlossaire {
    id: string;
    libelle: string;
    definition: TemplateResult;
}

export const PAGE_GLOSSAIRE_CONTENU: SectionPageGlossaire[] = [
    {
        id: 'agriculture-biologique',
        libelle: 'Agriculture biologique',
        definition: html`
            Agriculture promouvant des pratiques préservant l’environnement et les ressources naturelles, et favorables au bien-être et à la santé des
            animaux, des agriculteurs et des consommateurs. Certifiée par un label européen, elle répond à un cahier des charges précis. En
            particulier, l’utilisation de produits fertilisants et phytosanitaires de synthèse est interdite.
        `
    },
    {
        id: 'agroecologie',
        libelle: 'Agroécologie',
        definition: html`Ensemble d’idées et de pratiques consistant à appliquer à l’agronomie des savoirs issus de l’écologie scientifique, dans le
        but de concevoir des systèmes agraires durables. L’agroécologie ne répond pas à un cahier des charges ou un itinéraire technique précis mais
        constitue un cadre d’analyse général mettant l’accent sur certains grands principes agronomiques.`
    },
    {
        id: 'autonomie-alimentaire',
        libelle: 'Autonomie alimentaire',
        definition: html`Possibilité pour les habitants d’un territoire de subvenir à leurs besoins alimentaires avec les seules ressources de ce
        territoire.`
    },
    {
        id: 'durabilite',
        libelle: 'Durabilité',
        definition: html`Caractéristique d'un système dont le fonctionnement répond aux besoins du présent sans compromettre les capacités des
        générations futures à répondre aux leurs.`
    },
    {
        id: 'epci',
        libelle: 'EPCI',
        definition: html`Etablissement public de coopération intercommunale.`
    },
    {
        id: 'pat',
        libelle: 'PAT',
        definition: html`Projet alimentaire territorial. Les PAT ont pour objectif de relocaliser l'agriculture et l'alimentation dans les territoires
        en soutenant l'installation d'agriculteurs, les circuits courts ou les produits locaux dans les cantines. Issus de la Loi d'avenir pour
        l'agriculture qui encourage leur développement depuis 2014, ils sont élaborés de manière collective à l’initiative des acteurs d'un territoire
        (collectivités, entreprises agricoles et agroalimentaires, artisans, citoyens etc.).`
    },
    {
        id: 'menace',
        libelle: 'Menace',
        definition: html`contexte susceptible de produire des perturbations touchant un ou plusieurs maillons des systèmes alimentaires. Plus une
        menace s’aggrave, plus ces perturbations sont fréquentes et intenses. Exemples : dérèglement climatique, épuisement des ressources
        pétrolières.`
    },
    {
        id: 'otex',
        libelle: 'OTEX',
        definition: html`Les exploitations sont classées selon leur spécialisation : l'orientation technico-économique (OTEX). Ce classement se fait à 
        partir des coefficients de production brute standard (PBS). Une exploitation est spécialisée dans un domaine si la PBS de la ou des productions 
        concernées dépasse deux tiers du total. <c-lien href=https://agreste.agriculture.gouv.fr/agreste-web/methodon/N-Otex/methodon/>Voir Agreste</c-lien>.`
    },
    {
        id: 'perturbation',
        libelle: 'Perturbation',
        definition: html`Tendance ou événement affectant les systèmes alimentaires. Les perturbations graduelles (ou stress) ont des conséquences
        diffuses et progressives. Elles sont relativement prévisibles, bien que leur dynamique soit variable dans l’espace et le temps. Elles
        conduisent à des dégradations de fond des maillons constitutifs des systèmes alimentaires et peuvent accroître la vulnérabilité de ces
        derniers face aux perturbations brutales. Exemple : diminution du niveau moyen d’humidité des sols agricoles, déclin des insectes auxiliaires
        de cultures. Les perturbations brutales (ou chocs) correspondent quant à elles à des événements soudains, peu prévisibles, provoquant des
        situations de crise aux conséquences plus ou moins graves selon leur durée, leur intensité, et la vulnérabilité des éléments affectés.
        Exemples : vague de chaleur, choc pétrolier.`
    },
    {
        id: 'precarite-alimentaire',
        libelle: 'Précarité alimentaire',
        definition: html`Forme d’insécurité alimentaire résultant de la précarité économique. Le terme est notamment employé dans les pays du Nord
        pour souligner les conséquences de la pauvreté sur l’accès à l’alimentation.`
    },
    {
        id: 'resilience',
        libelle: 'Résilience',
        definition: html`Capacité d’un système à maintenir ou à retrouver ses fonctions essentielles lorsqu’il est soumis à une perturbation.`
    },
    {
        id: 'resilience-alimentaire',
        libelle: 'Résilience alimentaire',
        definition: html`Capacité d’un système alimentaire et de ses éléments constitutifs à garantir la sécurité alimentaire au cours du temps,
        malgré des perturbations variées et non prévues.`
    },

    {
        id: 'securite-alimentaire',
        libelle: 'Sécurité alimentaire',
        definition: html`Assurée sur un territoire lorsque tous ses habitants ont à tout moment la possibilité physique, sociale et économique de se
        procurer une nourriture suffisante, saine et nutritive leur permettant de satisfaire leurs besoins et préférences alimentaires pour mener une
        vie saine et active.`
    },
    {
        id: 'souverainete-alimentaire',
        libelle: 'Souveraineté alimentaire',
        definition: html`Possibilité d’organiser le système alimentaire d’un territoire selon les choix politiques de ses habitants, en particulier
        pour ce qui relève des conditions sociales et environnementales de production.`
    },
    {
        id: 'systeme-agro-industriel',
        libelle: 'Système agro-industriel',
        definition: html`Système alimentaire dominant caractérisé par l’importance de l’industrie et des grandes firmes multinationales dans son
        fonctionnement. Ses attributs sont ceux d’autres secteurs industriels : productivisme, standardisation, concentration, financiarisation,
        mondialisation.`
    },
    {
        id: 'systeme-alimentaire',
        libelle: 'Système alimentaire',
        definition: html`Ensemble des activités qui permettent de produire, transformer, transporter, consommer les aliments qui nourrissent
        quotidiennement la population.`
    },
    {
        id: 'zero-artificialisation',
        libelle: 'Zéro Artificialisation',
        definition: html`L'artificialisation est la transformation d'un sol naturel, agricole ou forestier pour l'affecter à des fonctions urbaines ou
        de transport (habitat, activités, commerces, infrastructures, équipements publics…). Cette transformation entraîne une imperméabilisation
        partielle ou totale et a des répercussions directes sur l’environnement. L'objectif Zéro Artificialisation consiste à stopper la consommation
        de nouveaux espaces.`
    }
];

import '@lga/design-system/build/composants/Lien.js';
import '../commun/ReseauxSociaux.js';

import { CSS_BREAKPOINT_MAX_WIDTH_MOBILE_PAYSAGE, CSS_BREAKPOINT_MAX_WIDTH_TABLETTE } from '@lga/design-system/build/styles/styles-breakpoints';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system';
import { css, html, LitElement, unsafeCSS } from 'lit';
import { customElement } from 'lit/decorators.js';
import { unsafeSVG } from 'lit/directives/unsafe-svg.js';

import { PAGES_PRINCIPALES } from '../../domaines/pages/configuration/declaration-pages';
import { ICONES_SVG } from '../../domaines/systeme-alimentaire/configuration/icones_svg';
import LOGO_LGA from '../../ressources/images/logo-les-greniers-abondance.svg';

declare global {
    interface HTMLElementTagNameMap {
        'c-pied-page': PiedPage;
    }
}

@customElement('c-pied-page')
export class PiedPage extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        css`
            footer {
                display: flex;
                justify-content: center;
                align-items: center;
                background-color: var(--couleur-secondaire-sombre);
                height: fit-content;
                color: var(--couleur-blanc);
            }
            main {
                display: flex;
                justify-content: center;
                flex-wrap: wrap;
                padding: calc(10 * var(--dsem)) 0;
                gap: calc(8 * var(--dsem));
                margin: 0 auto;
                padding-left: max((100vw - var(--largeur-maximum-contenu)) / 2, 1rem);
                padding-right: max((100vw - var(--largeur-maximum-contenu)) / 2, 1rem);
            }

            article {
                max-width: 450px;
                padding: calc(2 * var(--dsem));
            }

            h3 {
                display: flex;
                flex-direction: column;
            }
            h3 > svg {
                width: 26px;
                fill: var(--couleur-accent);
            }

            c-lien {
                --couleur: var(--couleur-blanc);
                --couleur-hover: var(--couleur-primaire);
            }

            #logoLGA {
                width: 200px;
                height: auto;
            }

            #informations > c-lien,
            #api-donnees > c-lien {
                --couleur: var(--couleur-accent);
                color: var(--couleur-accent);
            }

            @media screen and (max-width: ${unsafeCSS(CSS_BREAKPOINT_MAX_WIDTH_TABLETTE + 'px')}) {
                main {
                    gap: 0;
                    padding: 0 0 var(--hauteur-menu-pied-page) 0;
                }
            }

            @media screen and (max-width: ${unsafeCSS(CSS_BREAKPOINT_MAX_WIDTH_MOBILE_PAYSAGE + 'px')}) {
                article {
                    max-width: 250px;
                }
            }
        `
    ];

    render() {
        return html`
            <footer>
                <main>
                    <article>
                        <h3>Le projet CRATer${unsafeSVG(ICONES_SVG.traitNumeroEncartAccueil)}</h3>
                        <p>
                            CRATer est développé sous licences
                            <c-lien href="https://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY-NC-SA 4.0</c-lien> (contenu),
                            <c-lien href="https://www.etalab.gouv.fr/licence-ouverte-open-licence">ETALAB</c-lien> (données) et
                            <c-lien href="https://www.gnu.org/licenses/agpl-3.0.en.html">GNU AGPL v3</c-lien> (code).
                        </p>
                        <p id="informations"><c-lien href=${PAGES_PRINCIPALES.projet.getUrl()}>&#x2192; Plus d'informations</c-lien></p>
                        <p id="api-donnees"><c-lien href=${PAGES_PRINCIPALES.api.getUrl()}>&#x2192; API et données</c-lien></p>
                    </article>
                    <article>
                        <h3>Contact et réseaux${unsafeSVG(ICONES_SVG.traitNumeroEncartAccueil)}</h3>
                        <a href="https://resiliencealimentaire.org" target="_blank"
                            ><img id="logoLGA" src="${LOGO_LGA}" width="16" height="16" loading="lazy"
                        /></a>
                        <p>
                            Les Greniers d'Abondance<br />Association loi 1901<br /><c-lien href="https://resiliencealimentaire.org/mentions-legales/"
                                >Mentions Légales</c-lien
                            >
                        </p>
                        <c-reseaux-sociaux></c-reseaux-sociaux>
                    </article>
                </main>
            </footer>
        `;
    }
}

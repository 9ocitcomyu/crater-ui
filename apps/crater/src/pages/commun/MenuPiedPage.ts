import '@lga/design-system/build/composants/BoutonCarre.js';
import './template/TemplatePanneau.js';
import '@lga/design-system/build/composants/PanneauGlissant.js';

import { css, html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { unsafeSVG } from 'lit/directives/unsafe-svg.js';

import { PAGES_PRINCIPALES } from '../../domaines/pages/configuration/declaration-pages';
import { ICONES_SVG } from '../../domaines/systeme-alimentaire/configuration/icones_svg';

declare global {
    interface HTMLElementTagNameMap {
        'c-menu-pied-page': MenuPiedPage;
    }
}

@customElement('c-menu-pied-page')
export class MenuPiedPage extends LitElement {
    static styles = css`
        :host {
            display: block;
            width: 100%;
            height: 100%;
        }

        div {
            width: 100%;
            height: 100%;
            display: flex;
            justify-content: space-evenly;
            background-color: var(--couleur-blanc);
            align-items: center;
        }
    `;

    @property()
    idItemActif?: string;

    @property({ type: Boolean })
    desactiverBoutonSommaire = false;

    render() {
        return html`
            <div>
                <c-bouton-carre
                    ?selectionne=${this.idItemActif === PAGES_PRINCIPALES.diagnostic.getId()}
                    libelle=${PAGES_PRINCIPALES.diagnostic.getTitreCourt()}
                    libelleTooltip=${PAGES_PRINCIPALES.diagnostic.getTitreCourt()}
                    href="${PAGES_PRINCIPALES.diagnostic.getUrl()}"
                    >${unsafeSVG(ICONES_SVG.diagnostic)}</c-bouton-carre
                >
                <c-bouton-carre
                    ?selectionne=${this.idItemActif === PAGES_PRINCIPALES.carte.getId()}
                    libelle=${PAGES_PRINCIPALES.carte.getTitreCourt()}
                    libelleTooltip=${PAGES_PRINCIPALES.carte.getTitreCourt()}
                    href="${PAGES_PRINCIPALES.carte.getUrl()}"
                    >${unsafeSVG(ICONES_SVG.carte)}</c-bouton-carre
                >
                <c-bouton-carre
                    ?selectionne=${this.idItemActif === PAGES_PRINCIPALES.aide.getId()}
                    libelle=${PAGES_PRINCIPALES.aide.getTitreCourt()}
                    libelleTooltip=${PAGES_PRINCIPALES.aide.getTitreCourt()}
                    href="${PAGES_PRINCIPALES.aide.getUrl()}"
                    >${unsafeSVG(ICONES_SVG.aide)}</c-bouton-carre
                >
                ${this.desactiverBoutonSommaire
                    ? html`<c-bouton-carre slot="bouton-ouvrir" libelle="Menu" libelleTooltip="Menu" desactive
                          >${unsafeSVG(ICONES_SVG.sommaire)}</c-bouton-carre
                      >`
                    : html` <c-panneau-glissant>
                          <c-bouton-carre slot="bouton-ouvrir" libelle="Menu" libelleTooltip="Menu">${unsafeSVG(ICONES_SVG.sommaire)}</c-bouton-carre>
                          <c-template-panneau slot="panneau">
                              <slot name="contenu-sommaire" slot="contenu-panneau"></slot>
                          </c-template-panneau>
                      </c-panneau-glissant>`}
            </div>
        `;
    }
}

import { calculerValeurEntiereNonNulle, formaterNombreEnEntierString } from '@lga/commun/build/outils/base';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system';
import { css, html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { unsafeSVG } from 'lit/directives/unsafe-svg.js';

import { Note, NOTE_VALEUR_WARNING } from '../../domaines/diagnostics';

declare global {
    interface HTMLElementTagNameMap {
        'c-jauge-note-sur-10': JaugeNoteSur10;
    }
}

const WARNING_SVG_PATH = `<path id="PathWarning" d="M24 25.96V19.415" stroke="#161E3E" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"/>
<path id="Path_2" d="M23.9983 31.2188C23.7568 31.2188 23.5608 31.4148 23.5625 31.6562C23.5625 31.8977 23.7585 32.0938 24 32.0938C24.2415 32.0938 24.4375 31.8977 24.4375 31.6562C24.4375 31.4148 24.2415 31.2188 23.9983 31.2188" stroke="#161E3E" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"/>
<path id="Path_3" fill-rule="evenodd" clip-rule="evenodd" d="M27.5507 10.2432L40.954 33.7002C42.5115 36.4267 40.5427 39.82 37.4032 39.82H10.5967C7.45545 39.82 5.4867 36.4267 7.04595 33.7002L20.4492 10.2432C22.019 7.49396 25.981 7.49396 27.5507 10.2432Z" stroke="#161E3E" stroke-width="3" stroke-linecap="round"
stroke-linejoin="round"/>`;

@customElement('c-jauge-note-sur-10')
export class JaugeNoteSur10 extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        css`
            :host {
                display: block;
            }
            svg {
                width: 100%;
                height: 100%;
            }
            svg .cercle-partiel {
                stroke-width: 3px;
                animation: animation-jauge-note 1000ms ease-in-out;
            }
            @keyframes animation-jauge-note {
                from {
                    stroke-dasharray: 0, 100;
                }
            }
            svg .cercle-complet {
                stroke-width: 2px;
            }
            svg .texte-note,
            svg .texte-10 {
                font-family: var(--font-family-titre);
                fill: var(--couleur-neutre);
            }
            svg svg path {
                fill: none;
            }
        `
    ];

    @property({
        converter: {
            fromAttribute: (valueString: string | null | undefined) =>
                valueString === NOTE_VALEUR_WARNING
                    ? NOTE_VALEUR_WARNING
                    : valueString === '' || isNaN(Number(valueString))
                    ? null
                    : Number(valueString)
        }
    })
    note: Note = null;

    @property()
    couleur = '#000000';

    hexToRgba(couleuEnHex: string, degreTransparence = 1) {
        const r = parseInt(couleuEnHex.slice(1, 3), 16);
        const g = parseInt(couleuEnHex.slice(3, 5), 16);
        const b = parseInt(couleuEnHex.slice(5, 7), 16);
        return `rgba(${r}, ${g}, ${b}, ${degreTransparence})`;
    }

    render() {
        const niveauJaugeSur100 = calculerValeurEntiereNonNulle(this.note === '!' ? 10 : this.note) * 10;

        return html` <svg viewbox="0 0 36 36" xmlns="http://www.w3.org/2000/svg">
            <path
                class="cercle-complet"
                stroke=${this.hexToRgba(this.couleur, 0.3)}
                fill="none"
                d="M18 2.0845 a 15.9155 15.9155 0 0 1 0 31.831 a 15.9155 15.9155 0 0 1 0 -31.831"
            />
            <path
                class="cercle-partiel"
                stroke="${this.couleur}"
                stroke-dasharray="${niveauJaugeSur100},100"
                stroke-linecap="round"
                fill="none"
                d="M18 2.0845 a 15.9155 15.9155 0 0 1 0 31.831 a 15.9155 15.9155 0 0 1 0 -31.831"
            />

            ${this.note === NOTE_VALEUR_WARNING
                ? unsafeSVG(`<svg x="10" y="10" viewbox="0 0 50 50" width="16" height="16">${WARNING_SVG_PATH}</svg>`)
                : unsafeSVG(`
                
                <text class="texte-note" x="${
                    this.note === 10 ? '58%' : '50%'
                }" y="50%" font-size="14px" dominant-baseline="central" text-anchor="end">
                ${formaterNombreEnEntierString(this.note)}
                </text>
                <text class="texte-10"  x="${
                    this.note === 10 ? '58%' : '50%'
                }" y="50%" font-size="6px" dominant-baseline="hanging" text-anchor="start">/10</text>
            
            `)}
        </svg>`;
    }
}

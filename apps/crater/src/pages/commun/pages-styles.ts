import { css } from 'lit';

export const STYLES_CRATER = css`
    :host {
        --largeur-maximum-contenu: calc(1100px);
    }

    h1 {
        color: var(--couleur-primaire-sombre);
        margin-top: calc(2 * var(--dsem));
        margin-bottom: calc(4 * var(--dsem));
    }

    h2,
    h3 {
        color: var(--couleur-primaire-sombre);
        margin-top: calc(4 * var(--dsem));
        margin-bottom: calc(2 * var(--dsem));
    }

    h4,
    h5,
    h6 {
        color: var(--couleur-primaire-sombre);
        margin-top: 0;
        margin-bottom: 0;
    }

    figure {
        padding-top: calc(4 * var(--dsem));
        margin: 0;
        text-align: center;
        overflow: scroll;
    }

    figure img {
        width: 100%;
        height: auto;
        max-width: 600px;
        justify-content: center;
    }

    figcaption {
        max-width: 700px;
        margin: auto;
    }
`;

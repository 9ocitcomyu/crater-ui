import '@lga/commun/build/composants/champ-recherche-territoire/ChampRechercheTerritoire.js';
import '@lga/design-system/build/composants/BarreOnglets.js';
import '@lga/design-system/build/composants/Bouton.js';

import { getApiBaseUrl } from '@lga/commun/build/env/config-baseurl';
import { OngletItem, OptionsBarreOnglets } from '@lga/design-system/build/composants/BarreOnglets.js';
import { EvenementSelectionner } from '@lga/design-system/build/evenements/EvenementSelectionner';
import { css, html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { ifDefined } from 'lit/directives/if-defined.js';

import { EchelleTerritoriale } from '../../domaines/carte/EchelleTerritoriale';
import { PAGES_PRINCIPALES } from '../../domaines/pages/configuration/declaration-pages';
import { EvenementSelectionnerCategorieTerritoire } from '../../evenements/EvenementSelectionnerCategorieTerritoire';

declare global {
    interface HTMLElementTagNameMap {
        'c-barre-menu-territoires': BarreMenuTerritoires;
    }
}

export interface OptionsBarreMenuTerritoires {
    onglets: OngletItem[];
    idOngletSelectionne: string;
    filtrerRecherchePourCarte?: boolean;
}

@customElement('c-barre-menu-territoires')
export class BarreMenuTerritoires extends LitElement {
    static styles = css`
        :host {
            display: block;
            height: 100%;
            width: 100%;
        }

        nav {
            display: flex;
            flex-direction: row;
            align-items: center;
            justify-content: space-between;
            padding: 0 calc(3 * var(--dsem));
            background-color: var(--couleur-blanc);
            border-bottom: 1px solid var(--couleur-neutre-clair);
            height: 100%;
        }

        c-champ-recherche-territoire {
            width: 25%;
            min-width: 25rem;
            max-width: 40rem;
        }
    `;

    @property({ attribute: false })
    options?: OptionsBarreMenuTerritoires;

    render() {
        const optionsBarreOnglet: OptionsBarreOnglets = {
            onglets: this.options?.onglets ?? [],
            idOngletSelectionne: this.options?.idOngletSelectionne
        };
        return html`
            <nav>
                <c-barre-onglets .options=${optionsBarreOnglet} @selectionner=${this.actionSelectionCategorieTerritoire}></c-barre-onglets>
                <c-champ-recherche-territoire
                    codesCategorieTerritoire=${ifDefined(
                        this.options?.filtrerRecherchePourCarte ? EchelleTerritoriale.listerTousLesCodeCategories.join(' ') : undefined
                    )}
                    apiBaseUrl=${getApiBaseUrl()}
                    lienFormulaireDemandeAjoutTerritoire="${PAGES_PRINCIPALES.demandeAjoutTerritoire.getUrl()}"
                >
                </c-champ-recherche-territoire>
            </nav>
        `;
    }

    private actionSelectionCategorieTerritoire(event: Event) {
        const evenementSelectionner = <EvenementSelectionner>event;
        this.dispatchEvent(new EvenementSelectionnerCategorieTerritoire(evenementSelectionner.detail.idItemSelectionne));
        evenementSelectionner.stopPropagation();
    }
}

import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system';
import { css, html, LitElement } from 'lit';
import { customElement } from 'lit/decorators.js';

import { PAGES_PRINCIPALES } from '../../../domaines/pages/configuration/declaration-pages';
import LOGO from '../../../ressources/images/crater-logo-nom-a-droite.svg';

declare global {
    interface HTMLElementTagNameMap {
        'c-identite-site': IdentiteSite;
    }
}

@customElement('c-identite-site')
export class IdentiteSite extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        css`
            :host {
                height: 100%;
            }

            a {
                cursor: pointer;
            }
            img {
                margin: auto;
                display: block;
                width: 150px;
                height: auto;
                padding-left: 15px;
            }
        `
    ];

    render() {
        return html`
            <a href=${PAGES_PRINCIPALES.accueil.getUrl()}>
                <img class="logo" src="${LOGO}" width="16" height="16" />
            </a>
        `;
    }
}

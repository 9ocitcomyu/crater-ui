import { CSS_BREAKPOINT_MAX_WIDTH_MOBILE_PAYSAGE } from '@lga/design-system/build/styles/styles-breakpoints';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system';
import { css, html, LitElement, unsafeCSS } from 'lit';
import { customElement } from 'lit/decorators.js';
import { unsafeSVG } from 'lit/directives/unsafe-svg.js';

import { ICONES_SVG } from '../../../domaines/systeme-alimentaire/configuration/icones_svg';

declare global {
    interface HTMLElementTagNameMap {
        'c-aller-plus-loin': AllerPlusLoin;
    }
}
@customElement('c-aller-plus-loin')
export class AllerPlusLoin extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        css`
            :host {
                width: 100%;
                display: block;
                background: var(--couleur-primaire-clair);
                border-radius: calc(2 * var(--dsem)) calc(2 * var(--dsem)) 200px;
            }
            main {
                padding: calc(2 * var(--dsem));
            }
            h2 {
                margin-top: 0;
                margin-bottom: var(--dsem);
            }
            svg {
                fill: var(--couleur-primaire);
                margin-bottom: calc(2 * var(--dsem));
            }

            @media screen and (max-width: ${unsafeCSS(CSS_BREAKPOINT_MAX_WIDTH_MOBILE_PAYSAGE + 'px')}) {
                main {
                    padding-bottom: calc(5 * var(--dsem));
                }
            }
        `
    ];
    render() {
        return html`
            <main>
                <h2 class="titre-moyen">Pour aller plus loin</h2>
                ${unsafeSVG(ICONES_SVG.soulignerTexte)}
                <slot></slot>
            </main>
        `;
    }
}

import '../commun/template/TemplatePageAccueil.js';
import '@lga/design-system/build/composants/Bouton.js';

import { Bouton } from '@lga/design-system/build/composants/Bouton';
import { ICONES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/icones-design-system';
import { CSS_BREAKPOINT_MAX_WIDTH_TABLETTE } from '@lga/design-system/build/styles/styles-breakpoints.js';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { css, html, LitElement, unsafeCSS } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { unsafeHTML } from 'lit/directives/unsafe-html.js';

import { PAGES_PRINCIPALES } from '../../domaines/pages/configuration/declaration-pages';
import { STYLES_CRATER } from '../commun/pages-styles';

declare global {
    interface HTMLElementTagNameMap {
        'c-page-erreur': PageErreur;
    }
}
@customElement('c-page-erreur')
export class PageErreur extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        STYLES_CRATER,
        css`
            main {
                display: flex;
                flex-direction: column;
                align-items: center;
                padding: calc(2 * var(--dsem));
                gap: calc(40 * var(--dsem));
            }

            section {
                display: flex;
                flex-direction: column;
                align-items: center;
                gap: calc(5 * var(--dsem));
                max-width: var(--largeur-maximum-contenu);
            }

            h1 > svg {
                fill: var(--couleur-accent);
            }

            h1 {
                display: flex;
                flex-direction: column;
                align-items: center;
                gap: calc(var(--dsem) * 2);
                text-align: center;
                width: 80%;
            }

            c-bouton {
                width: 50%;
            }

            #message-erreur {
                margin-bottom: calc(4 * var(--dsem));
                max-width: 800px;
                padding: 0.5rem;
                border-radius: var(--dsem);
                border: 1px solid var(--couleur-neutre-40);
            }

            @media screen and (max-width: ${unsafeCSS(CSS_BREAKPOINT_MAX_WIDTH_TABLETTE + 'px')}) {
                main {
                    gap: calc(35 * var(--dsem));
                }

                section {
                    gap: var(--dsem);
                }
            }

            @media screen and (max-width: 600px) {
                h1 {
                    width: 100%;
                }

                main {
                    gap: calc(30 * var(--dsem));
                }
            }
        `
    ];

    @property()
    codeErreur = '';

    @property()
    messageErreur = '';

    render() {
        document.title = `Erreur ${this.codeErreur} - ${this.messageErreur} | CRATer`;
        return html`
            <c-template-page-accueil>
                <main slot="contenu">
                    <section>
                        <h1>Problème...</h1>
                        <div class="message" id="message-erreur">
                            <p>
                                Une erreur est survenue ${this.messageErreur ? html`(message : "${unsafeHTML(this.messageErreur)}")` : html``}
                                <br />
                                Merci de corriger ou d'essayer à nouveau dans quelques secondes.
                                <br />Si le problème persiste, vous pouvez nous envoyer un message via le
                                <c-lien href=${PAGES_PRINCIPALES.contact.getUrl()}>formulaire de contact</c-lien>
                            </p>
                        </div>

                        <c-bouton
                            href="${PAGES_PRINCIPALES.accueil.getUrl()}"
                            libelle="Retour à l'accueil"
                            libelleTooltip="Retour à l'accueil"
                            type="${Bouton.TYPE.plat}"
                            positionIcone="${Bouton.POSITION_ICONE.droite}"
                        >
                            ${ICONES_DESIGN_SYSTEM.flecheDroite}
                        </c-bouton>
                    </section>
                </main>
            </c-template-page-accueil>
        `;
    }
}

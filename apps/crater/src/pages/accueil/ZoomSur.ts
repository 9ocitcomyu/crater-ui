import '../commun/EncartLienVersPage.js';

import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system';
import { css, html, LitElement } from 'lit';
import { customElement } from 'lit/decorators.js';
import { ifDefined } from 'lit/directives/if-defined.js';
import { unsafeSVG } from 'lit/directives/unsafe-svg.js';

import { EchelleTerritoriale } from '../../domaines/carte/EchelleTerritoriale';
import { construireUrlPageCarte } from '../../domaines/pages/pages-utils';
import { ICONES_SVG } from '../../domaines/systeme-alimentaire/configuration/icones_svg';
import { IDS_INDICATEURS } from '../../domaines/systeme-alimentaire/configuration/indicateurs';
import PESTICIDES_VISUEL from '../../ressources/illustrations/accueil/pesticides.png';
import { POSITION_ILLUSTRATION } from '../commun/EncartLienVersPage.js';
import { STYLES_ZONES_COMPLEMENTS_ACCUEIL } from './accueil-utils';

declare global {
    interface HTMLElementTagNameMap {
        'c-zoom-sur': ZoomSur;
    }
}

@customElement('c-zoom-sur')
export class ZoomSur extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        STYLES_ZONES_COMPLEMENTS_ACCUEIL,
        css`
            main {
                padding-top: calc(4 * var(--dsem));
            }
        `
    ];

    render() {
        return html`
            <div id="contenu">
                <header class="titre-large">Zoom sur... ${unsafeSVG(ICONES_SVG.soulignerTexte)}</header>
                <main>
                    <c-encart-lien-vers-page
                        titre="Le fléau des pesticides"
                        href=${ifDefined(construireUrlPageCarte(IDS_INDICATEURS.noduNormalise, EchelleTerritoriale.Epcis.id))}
                        positionIllustration=${POSITION_ILLUSTRATION.gauche}
                        libelleBouton="Voir la carte des pesticides"
                        libelleCourtBouton="Voir la carte"
                    >
                        <span slot="texte">
                            L’utilisation massive des pesticides participe à la mort d’un nombre incalculable d’insectes, de plantes et d’autres êtres
                            vivants non nuisibles aux cultures.
                            <span class="texte-titre"
                                >À l'échelle mondiale, on estime que moins de 0,1% des molécules toxiques employées atteind effectivement sa
                                cible.</span
                            >
                        </span>
                        ${PESTICIDES_VISUEL}
                        <img slot="illustration" src="${PESTICIDES_VISUEL}" width="462" height="288" loading="lazy" />
                    </c-encart-lien-vers-page>
                </main>
            </div>
        `;
    }
}

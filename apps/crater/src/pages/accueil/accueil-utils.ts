import { css } from 'lit';

export const STYLES_ZONES_COMPLEMENTS_ACCUEIL = css`
    :host {
        display: block;
    }
    #contenu {
        display: flex;
        flex-direction: column;
        align-items: center;
        gap: calc(2 * var(--dsem));
    }
    main {
        display: flex;
        flex-direction: column;
        align-items: center;
        gap: calc(6 * var(--dsem));
    }
    header {
        display: flex;
        flex-direction: column;
        align-items: center;
        gap: var(--dsem);
        color: var(--couleur-primaire-sombre);
    }
    header > svg {
        fill: var(--couleur-primaire);
    }

    img {
        height: auto;
    }
`;

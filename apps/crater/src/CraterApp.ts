import '@lga/design-system/public/theme-defaut/theme-defaut.css';
import './pages/commun/PageSablier.js';

import { estActifModeBeta, PARAMETRE_URL_ACTIVER_FONCTIONS_BETA } from '@lga/commun/build/outils/mode-beta';
import { css, html, LitElement } from 'lit';
import { DirectiveResult } from 'lit/async-directive.js';
import { customElement } from 'lit/decorators.js';
import { ifDefined } from 'lit/directives/if-defined.js';
import pagejs, { Context as ContextPageJS } from 'page';

import {
    PAGES_AIDE,
    PAGES_CARTE,
    PAGES_COMPRENDRE_LE_SA,
    PAGES_DIAGNOSTIC,
    PAGES_METHODOLOGIE,
    PAGES_PRINCIPALES
} from './domaines/pages/configuration/declaration-pages';
import { ContexteApp } from './domaines/pages/ContexteApp';
import { DonneesPage, DonneesPageCarte, DonneesPageDiagnostic } from './domaines/pages/donnees-pages';
import { ModelePage } from './domaines/pages/ModelePage';
import { Page } from './domaines/pages/Page.js';
import { EvenementErreur } from './evenements/EvenementErreur';
import { EvenementMajDonneesPage } from './evenements/EvenementMajDonneesPage';
import { EvenementNaviguer } from './evenements/EvenementNaviguer';
import { importDynamique } from './pages/commun/importDynamiqueDirective';
import {
    creerDonneesInitialesPageBase,
    creerDonneesInitialesPageCarte,
    creerDonneesInitialesPageDiagnostic,
    creerDonneesInitialesPageDiagnosticIndicateur,
    creerDonneesInitialesPageDiagnosticMaillon,
    routePageCarteIndicateur,
    routePageDiagnostic,
    routePageDiagnosticIndicateur,
    routePageDiagnosticMaillon,
    routePageStatique
} from './routeur-utils.js';
import { enregistrerSuiviPage } from './suivi-audience.js';

import('./pages/diagnostic/PageDiagnostic.js');

declare global {
    interface HTMLElementTagNameMap {
        'c-crater-app': CraterApp;
    }
}

@customElement('c-crater-app')
export class CraterApp extends LitElement {
    // Composant créé dans le DOM principal (pas de shadow dom), pour pouvoir scroller sur les hash internes
    protected createRenderRoot() {
        return this;
    }
    static styles = css``;

    private contexteApp: ContexteApp = new ContexteApp(new Page(PAGES_PRINCIPALES.accueil));

    private templatePageCourante: DirectiveResult = html`<c-page-sablier></c-page-sablier>`;

    constructor() {
        super();
        this.configurerRoutes();
    }

    private configurerRoutes() {
        // Redirections pour retro-compatibilité
        pagejs.redirect('/accueil', PAGES_PRINCIPALES.accueil.getUrl()); // Plus de /accueil, la HP est à la racine

        // Déclaration des routes
        pagejs('*', this.logguerRoute);
        pagejs(`${routePageStatique(PAGES_PRINCIPALES.accueil)}`, this.routerVersPageAccueil, this.verifierErreur, this.majFinale);
        // Diagnostic
        pagejs(`${routePageDiagnostic(PAGES_DIAGNOSTIC.synthese)}`, this.routerVersPageDiagnosticSynthese, this.verifierErreur, this.majFinale);
        pagejs(`${routePageDiagnostic(PAGES_DIAGNOSTIC.territoire)}`, this.routerVersPageDiagnosticTerritoire, this.verifierErreur, this.majFinale);
        pagejs(`${routePageDiagnostic(PAGES_DIAGNOSTIC.pdf)}`, this.routerVersPageDiagnosticPdf, this.verifierErreur, this.majFinale);
        pagejs(`${routePageDiagnosticMaillon(PAGES_DIAGNOSTIC.maillons)}`, this.routerVersPageDiagnosticMaillon, this.verifierErreur, this.majFinale);
        pagejs(
            `${routePageDiagnosticIndicateur(PAGES_DIAGNOSTIC.indicateurs)}`,
            this.routerVersPageDiagnosticIndicateur,
            this.verifierErreur,
            this.majFinale
        );
        pagejs(`${routePageStatique(PAGES_PRINCIPALES.diagnostic)}`, this.routerVersPageDiagnosticAccueil, this.verifierErreur, this.majFinale);
        // Carte
        pagejs(`${routePageCarteIndicateur(PAGES_CARTE.indicateurs)}`, this.routerVersPageCarteIndicateur, this.verifierErreur, this.majFinale);
        pagejs(`${routePageStatique(PAGES_PRINCIPALES.carte)}`, this.routerVersPageCarte, this.verifierErreur, this.majFinale);

        // Aide
        pagejs(`${routePageStatique(PAGES_PRINCIPALES.aide)}`, this.routerVersPageAideAccueil, this.verifierErreur, this.majFinale);
        // Aide - Comprendre le SA
        pagejs(
            `${routePageStatique(PAGES_AIDE.comprendreLeSystemeAlimentaire)}`,
            this.routerVersPageLeSystemeActuel,
            this.verifierErreur,
            this.majFinale
        );
        pagejs(`${routePageStatique(PAGES_COMPRENDRE_LE_SA.systemeActuel)}`, this.routerVersPageLeSystemeActuel, this.verifierErreur, this.majFinale);
        pagejs(
            `${routePageStatique(PAGES_COMPRENDRE_LE_SA.defaillancesVulnerabilites)}`,
            this.routerVersPageDefaillancesVulnerabilites,
            this.verifierErreur,
            this.majFinale
        );
        pagejs(
            `${routePageStatique(PAGES_COMPRENDRE_LE_SA.transitionAgricoleAlimentaire)}`,
            this.routerVersPageTransitionAgricoleAlimentaire,
            this.verifierErreur,
            this.majFinale
        );
        // Aide - Methodologie
        pagejs(`${routePageStatique(PAGES_AIDE.methodologie)}`, this.routerVersPageMethodologiePresentation, this.verifierErreur, this.majFinale);
        pagejs(
            `${routePageStatique(PAGES_METHODOLOGIE.presentationGenerale)}`,
            this.routerVersPageMethodologiePresentation,
            this.verifierErreur,
            this.majFinale
        );
        pagejs(
            `${routePageStatique(PAGES_METHODOLOGIE.terresAgricoles)}`,
            this.routerVersPageMethodologieTerresAgricoles,
            this.verifierErreur,
            this.majFinale
        );
        pagejs(
            `${routePageStatique(PAGES_METHODOLOGIE.agriculteursExploitations)}`,
            this.routerVersPageMethodologieAgriculteursExploitations,
            this.verifierErreur,
            this.majFinale
        );
        pagejs(`${routePageStatique(PAGES_METHODOLOGIE.intrants)}`, this.routerVersPageMethodologieIntrants, this.verifierErreur, this.majFinale);
        pagejs(`${routePageStatique(PAGES_METHODOLOGIE.production)}`, this.routerVersPageMethodologieProduction, this.verifierErreur, this.majFinale);
        pagejs(
            `${routePageStatique(PAGES_METHODOLOGIE.transformationDistribution)}`,
            this.routerVersPageMethodologieTransformationDistribution,
            this.verifierErreur,
            this.majFinale
        );
        pagejs(
            `${routePageStatique(PAGES_METHODOLOGIE.consommation)}`,
            this.routerVersPageMethodologieConsommation,
            this.verifierErreur,
            this.majFinale
        );
        pagejs(
            `${routePageStatique(PAGES_METHODOLOGIE.sourcesDonnees)}`,
            this.routerVersPageMethodologieSourcesDonnees,
            this.verifierErreur,
            this.majFinale
        );
        pagejs(
            `${routePageStatique(PAGES_METHODOLOGIE.licenceUtilisation)}`,
            this.routerVersPageMethodologieLicenceUtilisation,
            this.verifierErreur,
            this.majFinale
        );
        // Aide - FAQ-Glossaire
        pagejs(`${routePageStatique(PAGES_AIDE.faq)}`, this.routerVersPageFAQ, this.verifierErreur, this.majFinale);
        pagejs(`${routePageStatique(PAGES_AIDE.glossaire)}`, this.routerVersPageGlossaire, this.verifierErreur, this.majFinale);

        // Projet
        pagejs(`${routePageStatique(PAGES_PRINCIPALES.projet)}`, this.routerVersPageProjet, this.verifierErreur, this.majFinale);

        // Contact
        pagejs(`${routePageStatique(PAGES_PRINCIPALES.contact)}`, this.routerVersPageContact, this.verifierErreur, this.majFinale);
        pagejs(
            `${routePageStatique(PAGES_PRINCIPALES.demandeAjoutTerritoire)}`,
            this.routerVersPageDemandeAjoutTerritoire,
            this.verifierErreur,
            this.majFinale
        );
        // API
        pagejs(`${routePageStatique(PAGES_PRINCIPALES.api)}`, this.routerVersPageApi, this.verifierErreur, this.majFinale);

        pagejs('*', this.routerVersPageErreurNotFound);
        pagejs();
    }

    private logguerRoute = (contextPageJS: ContextPageJS, next: () => unknown) => {
        console.log('Routage vers ', contextPageJS.canonicalPath);
        next();
    };

    private verifierErreur = (contextPageJS: ContextPageJS, next: () => unknown) => {
        if (this.contexteApp.estEnErreur()) {
            this.templatePageCourante = importDynamique(
                import('./pages/erreur/PageErreur.js'),
                html`<c-page-erreur
                    codeErreur=${ifDefined(this.contexteApp.erreur?.code)}
                    messageErreur="${ifDefined(this.contexteApp.erreur?.message)}"
                ></c-page-erreur>`,
                html`<c-page-sablier></c-page-sablier>`
            );
            this.contexteApp.annulerErreur();
        }
        next();
    };

    private majFinale = (contextPageJS: ContextPageJS) => {
        this.positionnerInformationsPage(this.contexteApp.pageCourante);
        enregistrerSuiviPage(contextPageJS.canonicalPath, document.title);
        // request update explicite ici, car asservir le render aux maj d'url captées par pagejs
        this.requestUpdate();
    };

    private positionnerInformationsPage(page: Page<DonneesPage>) {
        document.title = page.getTitreLong() + ' | CRATer';
        this.positionnerLienCanoniqueHeader(page.getUrlCanonique());
        this.positionnerMetaDescriptionHeader(page.getMetaDescription());
    }

    private positionnerLienCanoniqueHeader(urlCanoniqueRelative: string) {
        const lienCanonique = <HTMLLinkElement>document.getElementById('lien-url-canonique') || document.createElement('link');
        lienCanonique.id = 'lien-url-canonique';
        lienCanonique.rel = 'canonical';
        lienCanonique.href = `${document.location.origin}${urlCanoniqueRelative}`;
        document.head.appendChild(lienCanonique);
    }

    private positionnerMetaDescriptionHeader(metaDescription: string | undefined) {
        if (metaDescription !== undefined) {
            const elementMetaDescription = <HTMLMetaElement>document.getElementById('meta-description') || document.createElement('meta');
            elementMetaDescription.id = 'meta-description';
            elementMetaDescription.name = 'description';
            elementMetaDescription.content = metaDescription;
            document.head.appendChild(elementMetaDescription);
        } else {
            const elementMetaDescription = <HTMLMetaElement>document.getElementById('meta-description');
            if (elementMetaDescription) {
                elementMetaDescription.remove();
            }
        }
    }

    private routerVersPageAccueil = (contextPageJS: ContextPageJS, next: () => unknown) => {
        this.sauvegarderContexteApp(PAGES_PRINCIPALES.accueil, creerDonneesInitialesPageBase(contextPageJS));
        this.templatePageCourante = importDynamique(
            import('./pages/accueil/PageAccueil.js'),
            html`<c-page-accueil idElementCibleScroll="${ifDefined(this.contexteApp.pageCourante.donnees.idElementCibleScroll)}"></c-page-accueil>`,
            html`<c-page-sablier></c-page-sablier>`
        );
        next();
    };

    private routerVersPageDiagnosticAccueil = (contextPageJS: ContextPageJS, next: () => unknown) => {
        this.sauvegarderContexteApp(PAGES_PRINCIPALES.diagnostic);
        this.templatePageCourante = importDynamique(
            import('./pages/diagnostic/PageDiagnosticAccueil.js'),
            html`<c-page-diagnostic-accueil></c-page-diagnostic-accueil>`,
            html`<c-page-sablier></c-page-sablier>`
        );
        next();
    };

    private routerVersPageDiagnosticSynthese = (contextPageJS: ContextPageJS, next: () => unknown) => {
        this.sauvegarderContexteApp(
            PAGES_DIAGNOSTIC.synthese as ModelePage<DonneesPage>,
            creerDonneesInitialesPageDiagnostic(contextPageJS, 'synthese')
        );
        this.templatePageCourante = importDynamique(
            import('./pages/diagnostic/PageDiagnostic.js'),
            html`<c-page-diagnostic .donneesPage=${<DonneesPageDiagnostic>this.contexteApp.pageCourante.donnees}></c-page-diagnostic>`,
            html`<c-page-sablier></c-page-sablier>`
        );
        next();
    };

    private routerVersPageDiagnosticTerritoire = (contextPageJS: ContextPageJS, next: () => unknown) => {
        this.sauvegarderContexteApp(
            PAGES_DIAGNOSTIC.territoire as ModelePage<DonneesPage>,
            creerDonneesInitialesPageDiagnostic(contextPageJS, 'territoire')
        );
        this.templatePageCourante = importDynamique(
            import('./pages/diagnostic/PageDiagnostic.js'),
            html` <c-page-diagnostic .donneesPage=${<DonneesPageDiagnostic>this.contexteApp.pageCourante.donnees}></c-page-diagnostic>`,
            html` <c-page-sablier></c-page-sablier>`
        );
        next();
    };

    private routerVersPageDiagnosticMaillon = (contextPageJS: ContextPageJS, next: () => unknown) => {
        this.sauvegarderContexteApp(PAGES_DIAGNOSTIC.maillons as ModelePage<DonneesPage>, creerDonneesInitialesPageDiagnosticMaillon(contextPageJS));
        this.templatePageCourante = importDynamique(
            import('./pages/diagnostic/PageDiagnostic.js'),
            html`<c-page-diagnostic .donneesPage=${<DonneesPageDiagnostic>this.contexteApp.pageCourante.donnees}></c-page-diagnostic>`,
            html`<c-page-sablier></c-page-sablier>`
        );
        next();
    };

    private routerVersPageDiagnosticIndicateur = (contextPageJS: ContextPageJS, next: () => unknown) => {
        this.sauvegarderContexteApp(
            PAGES_DIAGNOSTIC.indicateurs as ModelePage<DonneesPage>,
            creerDonneesInitialesPageDiagnosticIndicateur(contextPageJS)
        );
        this.templatePageCourante = importDynamique(
            import('./pages/diagnostic/PageDiagnostic.js'),
            html`<c-page-diagnostic .donneesPage=${<DonneesPageDiagnostic>this.contexteApp.pageCourante.donnees}></c-page-diagnostic>`,
            html`<c-page-sablier></c-page-sablier>`
        );
        next();
    };
    private routerVersPageDiagnosticPdf = (contextPageJS: ContextPageJS, next: () => unknown) => {
        this.sauvegarderContexteApp(PAGES_DIAGNOSTIC.pdf as ModelePage<DonneesPage>, creerDonneesInitialesPageDiagnostic(contextPageJS, 'pdf'));
        this.templatePageCourante = importDynamique(
            import('./pages/diagnostic/PageDiagnosticPdf.js'),
            html`<c-page-diagnostic-pdf .donneesPage=${<DonneesPageDiagnostic>this.contexteApp.pageCourante.donnees}></c-page-diagnostic-pdf>`,
            html`<c-page-sablier></c-page-sablier>`
        );
        next();
    };

    private routerVersPageCarte = (contextPageJS: ContextPageJS, next: () => unknown) => {
        this.sauvegarderContexteApp(PAGES_PRINCIPALES.carte);
        this.templatePageCourante = importDynamique(
            import('./pages/carte/PageCarteAccueil.js'),
            html`<c-page-carte-accueil></c-page-carte-accueil>`,
            html`<c-page-sablier></c-page-sablier>`
        );
        next();
    };

    private routerVersPageCarteIndicateur = (contextPageJS: ContextPageJS, next: () => unknown) => {
        this.sauvegarderContexteApp(PAGES_CARTE.indicateurs as ModelePage<DonneesPage>, creerDonneesInitialesPageCarte(contextPageJS));
        console.log('routerVersPageCarteIndicateur', this.contexteApp.pageCourante.donnees);
        this.templatePageCourante = importDynamique(
            import('./pages/carte/PageCarte.js'),
            html`<c-page-carte .donneesPage=${<DonneesPageCarte>this.contexteApp.pageCourante.donnees}></c-page-carte>`,
            html`<c-page-sablier></c-page-sablier>`
        );
        next();
    };

    private routerVersPageAideAccueil = (contextPageJS: ContextPageJS, next: () => unknown) => {
        this.sauvegarderContexteApp(PAGES_PRINCIPALES.aide);
        this.templatePageCourante = importDynamique(
            import('./pages/aide/PageAide.js'),
            html`<c-page-aide></c-page-aide>`,
            html`<c-page-sablier></c-page-sablier>`
        );
        next();
    };

    private routerVersPageLeSystemeActuel = (contextPageJS: ContextPageJS, next: () => unknown) => {
        this.sauvegarderContexteApp(PAGES_COMPRENDRE_LE_SA.systemeActuel);
        this.templatePageCourante = importDynamique(
            import('./pages/aide/comprendre-le-systeme-alimentaire/PageAideSystemeActuel.js'),
            html`<c-page-aide-systeme-actuel></c-page-aide-systeme-actuel>`,
            html`<c-page-sablier></c-page-sablier>`
        );
        next();
    };
    private routerVersPageDefaillancesVulnerabilites = (contextPageJS: ContextPageJS, next: () => unknown) => {
        this.sauvegarderContexteApp(PAGES_COMPRENDRE_LE_SA.defaillancesVulnerabilites);
        this.templatePageCourante = importDynamique(
            import('./pages/aide/comprendre-le-systeme-alimentaire/PageAideDefaillancesVulnerabilites.js'),
            html`<c-page-aide-defaillances-vulnerabilites></c-page-aide-defaillances-vulnerabilites>`,
            html`<c-page-sablier></c-page-sablier>`
        );
        next();
    };
    private routerVersPageTransitionAgricoleAlimentaire = (contextPageJS: ContextPageJS, next: () => unknown) => {
        this.sauvegarderContexteApp(PAGES_COMPRENDRE_LE_SA.transitionAgricoleAlimentaire);
        this.templatePageCourante = importDynamique(
            import('./pages/aide/comprendre-le-systeme-alimentaire/PageAideTransitionAgricoleEtAlimentaire.js'),
            html`<c-page-aide-transition-agricole-et-alimentaire></c-page-aide-transition-agricole-et-alimentaire>`,
            html`<c-page-sablier></c-page-sablier>`
        );
        next();
    };

    private routerVersPageMethodologiePresentation = (contextPageJS: ContextPageJS, next: () => unknown) => {
        this.sauvegarderContexteApp(PAGES_METHODOLOGIE.presentationGenerale, creerDonneesInitialesPageBase(contextPageJS));
        this.templatePageCourante = importDynamique(
            import('./pages/aide/methodologie/PageMethodologiePresentationGenerale.js'),
            html`<c-page-methodologie-presentation-generale
                idElementCibleScroll="${this.contexteApp.pageCourante.donnees.idElementCibleScroll ?? ''}"
            ></c-page-methodologie-presentation-generale>`,
            html`<c-page-sablier></c-page-sablier>`
        );
        next();
    };
    private routerVersPageMethodologieTerresAgricoles = (contextPageJS: ContextPageJS, next: () => unknown) => {
        this.sauvegarderContexteApp(PAGES_METHODOLOGIE.terresAgricoles, creerDonneesInitialesPageBase(contextPageJS));
        this.templatePageCourante = importDynamique(
            import('./pages/aide/methodologie/PageMethodologieTerresAgricoles.js'),
            html`<c-page-methodologie-terres-agricoles
                idElementCibleScroll="${this.contexteApp.pageCourante.donnees.idElementCibleScroll ?? ''}"
            ></c-page-methodologie-terres-agricoles>`,
            html`<c-page-sablier></c-page-sablier>`
        );
        next();
    };
    private routerVersPageMethodologieAgriculteursExploitations = (contextPageJS: ContextPageJS, next: () => unknown) => {
        this.sauvegarderContexteApp(PAGES_METHODOLOGIE.agriculteursExploitations, creerDonneesInitialesPageBase(contextPageJS));
        this.templatePageCourante = importDynamique(
            import('./pages/aide/methodologie/PageMethodologieAgriculteursExploitations.js'),
            html`<c-page-methodologie-agriculteurs-exploitations
                idElementCibleScroll="${this.contexteApp.pageCourante.donnees.idElementCibleScroll ?? ''}"
            ></c-page-methodologie-agriculteurs-exploitations>`,
            html`<c-page-sablier></c-page-sablier>`
        );
        next();
    };
    private routerVersPageMethodologieIntrants = (contextPageJS: ContextPageJS, next: () => unknown) => {
        this.sauvegarderContexteApp(PAGES_METHODOLOGIE.intrants, creerDonneesInitialesPageBase(contextPageJS));
        this.templatePageCourante = importDynamique(
            import('./pages/aide/methodologie/PageMethodologieIntrants.js'),
            html`<c-page-methodologie-intrants
                idElementCibleScroll="${this.contexteApp.pageCourante.donnees.idElementCibleScroll ?? ''}"
            ></c-page-methodologie-intrants>`,
            html`<c-page-sablier></c-page-sablier>`
        );
        next();
    };
    private routerVersPageMethodologieProduction = (contextPageJS: ContextPageJS, next: () => unknown) => {
        this.sauvegarderContexteApp(PAGES_METHODOLOGIE.production, creerDonneesInitialesPageBase(contextPageJS));
        this.templatePageCourante = importDynamique(
            import('./pages/aide/methodologie/PageMethodologieProduction.js'),
            html`<c-page-methodologie-production
                idElementCibleScroll="${this.contexteApp.pageCourante.donnees.idElementCibleScroll ?? ''}"
            ></c-page-methodologie-production>`,
            html`<c-page-sablier></c-page-sablier>`
        );
        next();
    };
    private routerVersPageMethodologieTransformationDistribution = (contextPageJS: ContextPageJS, next: () => unknown) => {
        this.sauvegarderContexteApp(PAGES_METHODOLOGIE.transformationDistribution, creerDonneesInitialesPageBase(contextPageJS));
        this.templatePageCourante = importDynamique(
            import('./pages/aide/methodologie/PageMethodologieTransformationDistribution.js'),
            html`<c-page-methodologie-transformation-distribution
                idElementCibleScroll="${this.contexteApp.pageCourante.donnees.idElementCibleScroll ?? ''}"
            ></c-page-methodologie-transformation-distribution>`,
            html`<c-page-sablier></c-page-sablier>`
        );
        next();
    };
    private routerVersPageMethodologieConsommation = (contextPageJS: ContextPageJS, next: () => unknown) => {
        this.sauvegarderContexteApp(PAGES_METHODOLOGIE.consommation, creerDonneesInitialesPageBase(contextPageJS));
        this.templatePageCourante = importDynamique(
            import('./pages/aide/methodologie/PageMethodologieConsommation.js'),
            html`<c-page-methodologie-consommation
                idElementCibleScroll="${this.contexteApp.pageCourante.donnees.idElementCibleScroll ?? ''}"
            ></c-page-methodologie-consommation>`,
            html`<c-page-sablier></c-page-sablier>`
        );
        next();
    };
    private routerVersPageMethodologieSourcesDonnees = (contextPageJS: ContextPageJS, next: () => unknown) => {
        this.sauvegarderContexteApp(PAGES_METHODOLOGIE.sourcesDonnees, creerDonneesInitialesPageBase(contextPageJS));
        this.templatePageCourante = importDynamique(
            import('./pages/aide/methodologie/PageMethodologieSourcesDonnees.js'),
            html`<c-page-methodologie-sources-donnees
                idElementCibleScroll="${this.contexteApp.pageCourante.donnees.idElementCibleScroll ?? ''}"
            ></c-page-methodologie-sources-donnees>`,
            html`<c-page-sablier></c-page-sablier>`
        );
        next();
    };
    private routerVersPageMethodologieLicenceUtilisation = (contextPageJS: ContextPageJS, next: () => unknown) => {
        this.sauvegarderContexteApp(PAGES_METHODOLOGIE.licenceUtilisation, creerDonneesInitialesPageBase(contextPageJS));
        this.templatePageCourante = importDynamique(
            import('./pages/aide/methodologie/PageMethodologieLicenceUtilisation.js'),
            html`<c-page-methodologie-licence-utilisation
                idElementCibleScroll="${this.contexteApp.pageCourante.donnees.idElementCibleScroll ?? ''}"
            ></c-page-methodologie-licence-utilisation>`,
            html`<c-page-sablier></c-page-sablier>`
        );
        next();
    };
    private routerVersPageFAQ = (contextPageJS: ContextPageJS, next: () => unknown) => {
        this.sauvegarderContexteApp(PAGES_AIDE.faq, creerDonneesInitialesPageBase(contextPageJS));
        this.templatePageCourante = importDynamique(
            import('./pages/aide/faq/PageFAQ.js'),
            html`<c-page-faq idElementCibleScroll="${this.contexteApp.pageCourante.donnees.idElementCibleScroll ?? ''}"></c-page-faq>`,
            html`<c-page-sablier></c-page-sablier>`
        );
        next();
    };
    private routerVersPageGlossaire = (contextPageJS: ContextPageJS, next: () => unknown) => {
        this.sauvegarderContexteApp(PAGES_AIDE.glossaire, creerDonneesInitialesPageBase(contextPageJS));
        this.templatePageCourante = importDynamique(
            import('./pages/aide/glossaire/PageGlossaire.js'),
            html`<c-page-glossaire idElementCibleScroll="${this.contexteApp.pageCourante.donnees.idElementCibleScroll ?? ''}"></c-page-glossaire>`,
            html`<c-page-sablier></c-page-sablier>`
        );
        next();
    };

    private routerVersPageProjet = (contextPageJS: ContextPageJS, next: () => unknown) => {
        this.sauvegarderContexteApp(PAGES_PRINCIPALES.projet);
        this.templatePageCourante = importDynamique(
            import('./pages/projet/PageProjet.js'),
            html`<c-page-projet></c-page-projet>`,
            html`<c-page-sablier></c-page-sablier>`
        );
        next();
    };

    private routerVersPageApi = (contextPageJS: ContextPageJS, next: () => unknown) => {
        this.sauvegarderContexteApp(PAGES_PRINCIPALES.api);
        this.templatePageCourante = importDynamique(
            import('./pages/api/PageApi.js'),
            html`<c-page-api></c-page-api>`,
            html`<c-page-sablier></c-page-sablier>`
        );
        next();
    };

    private routerVersPageContact = (contextPageJS: ContextPageJS, next: () => unknown) => {
        this.sauvegarderContexteApp(PAGES_PRINCIPALES.contact);
        this.templatePageCourante = importDynamique(
            import('./pages/contact/PageContact.js'),
            html`<c-page-contact></c-page-contact>`,
            html`<c-page-sablier></c-page-sablier>`
        );
        next();
    };
    private routerVersPageDemandeAjoutTerritoire = (contextPageJS: ContextPageJS, next: () => unknown) => {
        this.sauvegarderContexteApp(PAGES_PRINCIPALES.demandeAjoutTerritoire);
        this.templatePageCourante = importDynamique(
            import('./pages/contact/PageDemandeAjoutTerritoire.js'),
            html`<c-page-demande-ajout-territoire></c-page-demande-ajout-territoire>`,
            html`<c-page-sablier></c-page-sablier>`
        );
        next();
    };
    private routerVersPageErreurNotFound = (contextPageJS: ContextPageJS, next: () => unknown) => {
        this.sauvegarderContexteApp(PAGES_PRINCIPALES.erreur);
        this.contexteApp.mettreEnErreur('URL_INCONNUE', `La page demandée n'existe pas, l'adresse "${contextPageJS.canonicalPath}" est inconnue.`);
        this.templatePageCourante = importDynamique(
            import('./pages/erreur/PageErreur.js'),
            html`<c-page-erreur
                codeErreur=${ifDefined(this.contexteApp.erreur?.code)}
                messageErreur="${ifDefined(this.contexteApp.erreur?.message)}"
            ></c-page-erreur>`,
            html`<c-page-sablier></c-page-sablier>`
        );
        this.contexteApp.annulerErreur();
        next();
    };
    private sauvegarderContexteApp(modelePage: ModelePage<DonneesPage>, donneesInitiales: DonneesPage = {}) {
        const nouvellePage = new Page<DonneesPage>(modelePage, donneesInitiales);
        if (this.contexteApp.pageCourante.getId() !== nouvellePage.getId()) {
            this.resetScrollPage();
        }
        this.contexteApp.pageCourante = nouvellePage;
    }

    private resetScrollPage() {
        window.scrollTo({ top: 0, left: 0, behavior: 'auto' });
    }

    render() {
        return this.templatePageCourante;
    }

    firstUpdated() {
        document.addEventListener(EvenementNaviguer.ID, (event: Event) => {
            this.actionNaviguer((<EvenementNaviguer>event).detail.url, (<EvenementNaviguer>event).detail.cible);
        });
        document.addEventListener(EvenementErreur.ID, (event: Event) => {
            this.actionErreur(event);
        });
        document.addEventListener(EvenementMajDonneesPage.ID, (event: Event) => {
            this.actionMajDonneesPage(event);
        });

        // FIXME : faire marcher le back, il semblerait que pagejs voit l'evt, mais lit ne fait pas le requestupdate
        window.addEventListener('popstate', (e) => {
            console.log('Popstate', e.state);
        });
    }

    private actionErreur(event: Event) {
        const evenementErreur = <EvenementErreur>event;
        this.contexteApp.mettreEnErreur(evenementErreur.detail.codeErreur, evenementErreur.detail.messageErreur);
        pagejs(window.location.pathname + window.location.search + window.location.hash);
    }

    private actionMajDonneesPage(event: Event) {
        const evenementMajDonneesPage = <EvenementMajDonneesPage<Partial<DonneesPage>>>event;
        // TODO : probablement mieux de gerer ce concept d'ancienne page dans ContexteApp. A faire avec la conservation du contexte entre diag et carte
        const anciennePage = new Page(this.contexteApp.pageCourante.modelePage, this.contexteApp.pageCourante.donnees);
        this.contexteApp.pageCourante.majDonnees(evenementMajDonneesPage.detail.donneesPage);
        if (anciennePage.getUrl() !== this.contexteApp.pageCourante.getUrl()) {
            this.ajouterDansHistoriqueDeNavigation(this.contexteApp.pageCourante.getUrl());
        }
        this.positionnerInformationsPage(this.contexteApp.pageCourante);
    }

    private ajouterDansHistoriqueDeNavigation(urlRelative: string) {
        const url = new URL(urlRelative, location.origin);
        history.pushState({ path: urlRelative }, '', url);
    }

    actionNaviguer(url: string, cible = '_self') {
        console.log(`Naviger vers url=${url}, cible=${cible}`);
        if (url.startsWith('#')) {
            // lien intern vers hash
            const urlCible = new URL(location.href);
            urlCible.hash = url;
            pagejs(`${urlCible.pathname}${urlCible.search}${urlCible.hash}`);
        } else if (!url.startsWith('http') && cible === '_self') {
            // lien interne, url sans http://hostname
            const urlCible = new URL(`${location.protocol}\\${location.host}${url}`);
            // TODO : modifier ce fonctionnement => stocker l'état du mode beta dans un champ de CraterApp, et ne plus avoir besoin de le faire suivre dans l'url
            if (estActifModeBeta() && !urlCible.searchParams.has(PARAMETRE_URL_ACTIVER_FONCTIONS_BETA)) {
                urlCible.searchParams.set(PARAMETRE_URL_ACTIVER_FONCTIONS_BETA, '');
            }
            console.log(`Naviger vers url=${urlCible.href}, cible=${cible}`);
            pagejs(`${urlCible.pathname}${urlCible.search}${urlCible.hash}`);
        } else {
            // lien externe
            window.open(url, cible);
        }
    }
}

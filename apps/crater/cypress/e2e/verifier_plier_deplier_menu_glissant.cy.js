context('Test de vérification plier/deplier menu glissant', () => {
    it('Le bouton du burger menu doit etre masqué sur écran large', () => {
        // init
        cy.viewport(1500, 1000);
        cy.visit('/');

        cy.get('c-menu-glissant-mobile').should('be.hidden');
        cy.get('[data-cy=bouton-ouvrir-menu-glissant]').should('be.hidden');
    });

    it('Afficher le menu glissant', () => {
        // init
        cy.viewport(1000, 1000);
        cy.visit('/');

        cy.get('c-menu-glissant-mobile').should('be.hidden');
        cy.get('[data-cy=bouton-ouvrir-menu-glissant]').click({ force: true });

        cy.get('c-menu-glissant-mobile')
            .should('be.visible')
            // Click dans la zone du menu en dehors d'un lien ne doit pas le fermer
            .click(50, 900)
            .should('be.visible');
    });

    it('Masquer le menu glissant', () => {
        // init
        cy.viewport(1000, 1000);
        cy.visit('/');

        // click sur le bouton fermer
        cy.get('[data-cy=bouton-ouvrir-menu-glissant]').click({ force: true });
        cy.get('[data-cy=bouton-fermer-menu-glissant]').click({ force: true });
        // then
        cy.get('c-menu-glissant-mobile').should('be.hidden');

        // click en dehors du menu
        cy.get('[data-cy=bouton-ouvrir-menu-glissant]').click({ force: true });
        cy.get('body').click(0, 0, { force: true });
        // then
        cy.get('c-menu-glissant-mobile').should('be.hidden');
    });
});

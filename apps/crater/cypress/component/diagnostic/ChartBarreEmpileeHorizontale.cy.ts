import '@lga/design-system/public/theme-defaut/theme-defaut.css';
import '../../../src/pages/diagnostic/charts/ChartBarreEmpileeHorizontale.js';
import { html } from 'lit';

describe('Test BarreMenuTerritoire', () => {
    const items = [
        {
            valeur: 40,
            libelle: 'categorie a',
            couleur: 'red'
        },
        {
            valeur: 62,
            libelle: 'categorie b',
            couleur: 'green'
        },
        {
            valeur: 100,
            libelle: 'categorie c',
            couleur: 'blue'
        },
        {
            valeur: 100,
            libelle: 'categorie d',
            couleur: 'orange'
        }
    ];

    it('Test affichage mobile', () => {
        // given
        cy.viewport(500, 500);
        cy.mount<'c-chart-barre-empilee-horizontale'>(html`
            <c-chart-barre-empilee-horizontale .items=${items} unite="ha"></c-chart-barre-empilee-horizontale>
        `);
        // then
        cy.get('c-chart-barre-empilee-horizontale').invoke('width').should('be.approximately', 484, 10);
        cy.get('c-chart-barre-empilee-horizontale').get('.barre-item').should('have.length', 4);
        cy.get('c-chart-barre-empilee-horizontale').get('.legende-item').should('have.length', 4);
        cy.get('c-chart-barre-empilee-horizontale').get('.barre-item').eq(1).invoke('width').should('be.approximately', 99, 2);
        cy.get('c-chart-barre-empilee-horizontale').get('.barre-item').eq(1).should('have.css', 'background-color', 'rgb(0, 128, 0)');
    });
});

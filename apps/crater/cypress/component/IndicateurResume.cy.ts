import '@lga/design-system/public/theme-defaut/theme-defaut.css';
import '../../src/pages/diagnostic/chapitres/composants/IndicateurResume.js';

import chaiColors from 'chai-colors';
import { html } from 'lit';
import { VALEURS_COULEURS } from '../../../../modules/design-system/cypress/component/outils_tests';
import { QualificatifEvolution, Signe } from '@lga/commun/build/outils/base.js';
import { Indicateur } from '../../src/domaines/systeme-alimentaire/Indicateur.js';
import { SystemeAlimentaire } from '../../src/domaines/systeme-alimentaire/SystemeAlimentaire.js';

// permet de comparer des couleurs hex rgb, etc...
chai.use(chaiColors);

const ICONE = `<svg fill="#000000" width="800px" height="800px" viewBox="0 0 32 32" version="1.1" xmlns="http://www.w3.org/2000/svg">
<title>square</title>
<path d="M1.25 1.25v29.5h29.5v-29.5zM29.25 29.25h-26.5v-26.5h26.5z"></path>
</svg>`;

const indicateur = new Indicateur(
    {
        id: "id de l'indicateur",
        idMaillon: 'id maillon',
        libelle: "Titre de l'indicateur",
        description: 'description',
        icone: ICONE
    },
    new SystemeAlimentaire()
);

describe('Desktop', () => {
    it("2 chiffres clés, sans description d'évolution", () => {
        //Given
        cy.viewport(900, 500);
        cy.mount<'c-indicateur-resume'>(
            html` <c-indicateur-resume
                .options=${{
                    indicateur: indicateur,
                    chiffresCles: [
                        {
                            libelleAvantChiffre: 'libelle avant chiffre 1',
                            chiffre: '1%',
                            libelleApresChiffre: 'libelle apres chiffre 1'
                        },
                        {
                            libelleAvantChiffre: 'libelle avant chiffre 2',
                            chiffre: '1%',
                            libelleApresChiffre: 'libelle apres chiffre 2'
                        }
                    ],
                    motSeparateurChiffresCles: 'VS'
                }}
            >
            </c-indicateur-resume>`
        );
        //Then
        cy.get('c-indicateur-resume').find('.mot-separateur').contains('VS');
    });

    it('chiffre clé avec message évolution positif', () => {
        //Given
        cy.viewport(900, 500);
        cy.mount<'c-indicateur-resume'>(
            html` <c-indicateur-resume
                .options=${{
                    indicateur: indicateur,
                    chiffresCles: [
                        {
                            libelleAvantChiffre: 'libelle avant chiffre',
                            chiffre: '1%',
                            libelleApresChiffre: 'libelle apres chiffre',
                            evolutionSigne: Signe.POSITIF,
                            evolutionQualificatif: QualificatifEvolution.progres,
                            evolutionDescription: "ca monte et c'est bien"
                        }
                    ],
                    motSeparateurChiffresCles: 'VS'
                }}
            >
            </c-indicateur-resume>`
        );
        //Then
        cy.get('c-indicateur-resume').find('#evolution').should('have.css', 'color').and('be.colored', VALEURS_COULEURS.couleur_succes);
        cy.get('c-indicateur-resume').find('#evolution svg').should('have.css', 'stroke').and('be.colored', VALEURS_COULEURS.couleur_succes);
    });

    it('chiffre clé avec message évolution négative', () => {
        //Given
        cy.viewport(900, 500);
        cy.mount<'c-indicateur-resume'>(
            html` <c-indicateur-resume
                .options=${{
                    indicateur: indicateur,
                    chiffresCles: [
                        {
                            libelleAvantChiffre: 'libelle avant chiffre',
                            chiffre: '1%',
                            libelleApresChiffre: 'libelle apres chiffre',
                            evolutionSigne: Signe.NEGATIF,
                            evolutionQualificatif: QualificatifEvolution.declin,
                            evolutionDescription: "ca baisse et c'est mal"
                        }
                    ],
                    motSeparateurChiffresCles: 'VS'
                }}
            >
            </c-indicateur-resume>`
        );
        //Then
        cy.get('c-indicateur-resume').find('#evolution').should('have.css', 'color').and('be.colored', VALEURS_COULEURS.couleur_danger);
        cy.get('c-indicateur-resume').find('#evolution svg').should('have.css', 'stroke').and('be.colored', VALEURS_COULEURS.couleur_danger);
    });
});
describe('Mobile', () => {
    it("doit s'afficher sans icone a gauche", () => {
        //Given
        cy.viewport(320, 500);
        cy.mount<'c-indicateur-resume'>(
            html` <c-indicateur-resume
                .options=${{
                    indicateur: indicateur,
                    chiffresCles: [
                        {
                            libelleAvantChiffre: 'libelle avant chiffre',
                            chiffre: '1%',
                            libelleApresChiffre: 'libelle apres chiffre',
                            evolutionSigne: Signe.ZERO,
                            evolutionQualificatif: QualificatifEvolution.neutre,
                            evolutionDescription: "pas d'évolution et c'est neutre "
                        }
                    ],
                    motSeparateurChiffresCles: 'VS'
                }}
            >
            </c-indicateur-resume>`
        );
        //Then
        cy.get('c-indicateur-resume').find('#icone-indicateur').should('not.be.visible');
    });
});

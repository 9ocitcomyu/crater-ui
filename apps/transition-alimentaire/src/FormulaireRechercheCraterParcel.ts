import '@lga/commun/build/composants/champ-recherche-territoire/ChampRechercheTerritoire.js';

import { ChampRechercheTerritoire } from '@lga/commun/build/composants/champ-recherche-territoire/ChampRechercheTerritoire';
import { EvenementReinitialiserTerritoire } from '@lga/commun/build/composants/champ-recherche-territoire/EvenementReinitialiserTerritoire';
import { EvenementSelectionnerTerritoire } from '@lga/commun/build/composants/champ-recherche-territoire/EvenementSelectionnerTerritoire';
import { getApiBaseUrl } from '@lga/commun/build/env/config-baseurl';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system';
import { css, html, LitElement } from 'lit';
import { customElement, query, state } from 'lit/decorators.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-formulaire-recherche-crater-parcel': FormulaireRechercheCraterParcel;
    }
}

@customElement('c-formulaire-recherche-crater-parcel')
export class FormulaireRechercheCraterParcel extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        css`
            #boutons {
                display: flex;
                align-items: center;
                justify-content: space-around;
                padding: 1rem;
            }
            #boutons a {
                color: var(--couleur-blanc);
                padding: 0.3rem 1rem;
                margin-left: 0.1rem;
                margin-right: 0.1rem;
                cursor: pointer;
                border-radius: 5px;
                text-decoration: none;
            }
            #boutons a:hover {
                filter: brightness(150%);
            }
            .inactif {
                background-color: var(--gris) !important;
                cursor: default !important;
                filter: none !important;
            }
            #crater {
                background-color: var(--bleu-crater);
            }
            #parcel {
                background-color: var(--vert);
            }
        `
    ];

    @query('c-champ-recherche-territoire')
    private champRechercheTerritoire!: ChampRechercheTerritoire;

    @state()
    private idTerritoireCrater: string | null | undefined = undefined;

    @state()
    private idTerritoireParcel: string | null | undefined = undefined;

    render() {
        return html`
            <c-champ-recherche-territoire
                apiBaseUrl=${getApiBaseUrl()}
                lienFormulaireDemandeAjoutTerritoire="https://resiliencealimentaire.org/formulaire-ajout-territoire-crater/"
            >
            </c-champ-recherche-territoire>

            <div id="boutons" class="texte-moyen">
                ${this.idTerritoireCrater === undefined
                    ? html`<a id="crater" class="inactif">Voir dans CRATer →</a>`
                    : this.idTerritoireCrater === null
                    ? html`<a id="crater" href="https://crater.resiliencealimentaire.org">Voir dans CRATer →</a>`
                    : html`<a id="crater" href="https://crater.resiliencealimentaire.org/diagnostic?idTerritoire=${this.idTerritoireCrater}"
                          >Voir dans CRATer →</a
                      >`}
                ${this.idTerritoireParcel === undefined
                    ? html`<a id="parcel" class="inactif">Voir dans Parcel →</a>`
                    : this.idTerritoireParcel === null
                    ? html`<a id="parcel" href="https://parcel-app.org">Voir dans Parcel →</a>`
                    : html`<a id="parcel" href="https://parcel-app.org/resultats-de-votre-relocalisation?idterritoire=${this.idTerritoireParcel}"
                          >Voir dans Parcel →</a
                      >`}
            </div>
        `;
    }

    protected firstUpdated() {
        this.champRechercheTerritoire.addEventListener(EvenementSelectionnerTerritoire.ID, this.actionSelectionTerritoire);
        this.champRechercheTerritoire.addEventListener(EvenementReinitialiserTerritoire.ID, this.actionReinitaliserFormulaire);
    }

    private actionSelectionTerritoire = (evt: Event) => {
        this.idTerritoireCrater = (<EvenementSelectionnerTerritoire>evt).detail.idTerritoireCrater;
        this.idTerritoireParcel = (<EvenementSelectionnerTerritoire>evt).detail.idTerritoireParcel;
    };

    private actionReinitaliserFormulaire = () => {
        this.idTerritoireCrater = undefined;
        this.idTerritoireParcel = undefined;
    };
}
